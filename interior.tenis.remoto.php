<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.::Federacion Deportiva Peruana de Tenis::.</title>
<meta name="keywords" content="tenis">
	<meta name="description" content="tenis">
	<meta name="robots" content="index, follow">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="favicon.ico">
	<link href='http://fonts.googleapis.com/css?family=Cuprum|Cantarell' rel='stylesheet' type='text/css'>
	
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<script src="js/jquery.mCustomScrollbar.js"></script>
	<script type="text/javascript">$(document).ready(function() {$('#header .submenu .wrapper div').hide();	});function changeText(textObj,text){if(textObj.value==""){textObj.value=text;textObj.style.color="#bebebe";} else if(textObj.value==text){textObj.value="";textObj.style.color="#069";}}</script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>


	<link href="css/slides.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery.browser.min.js"></script>
	<script type="text/javascript" src="js/slides.min.jquery.js"></script>

</head>
<body class="fnd_deportes">
	<div class="dpt_body">
		<div class="dpt_container_uno">
		<?php include_once("includes/base/header.php");?>
		</div>
		<div class="dpt_container_dos">
			<div class="dpt_section panel_item_uno corner_div_4"
				style="margin-top: 10px; height: auto">
				<div id="_deporte_interior" class="dpt_panel corner_div_4">
				<?php switch($_REQUEST['pg']){case md5('contactenos.php'):$titulo='Contactenos';$content='contactenos.php'; break;case md5('quienes-somos.php'):$title='Quienes Somos';$content='quienes-somos.php'; break;case md5('servicios.php'):$title='Nuestros Servicios';$content='servicios.php'; break;case md5('mapa.php'):$title='Mapa de Sitio';$content='mapa.php'; break;case md5('resultados_inte_deportes.php'):$title='Resultados por Deportes';$content='resultados_inte_deportes.php'; break;case md5('noticia.fb.php'):$title='Noticia';$content='noticia.interior.fb.php';	break;}$arrayColor=array("magenta-out","black-out","green-out","blue-out","orange-out","orangellow-out");$idx=rand(0,5);$color=$arrayColor[$idx];if(!empty($content)&&file_exists('includes/interior/'.$content)){include_once("includes/interior/interior-contenido.php");}else{include_once("construccion.php");}?>
				</div>
			</div>
			<script type="text/javascript">
				var _gaq = _gaq || [];
				_gaq.push(['_setAccount', 'UA-21199236-4']);
				_gaq.push(['_trackPageview']);
				(function() {
					var ga = document.createElement('script'); 
					ga.type = 'text/javascript'; 
					ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; 
					s.parentNode.insertBefore(ga, s);
				})();
			</script>
		</div>
		<?php include_once("includes/base/pie_pag.php");?>
	</div>
</body>
</html>
