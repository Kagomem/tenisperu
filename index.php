<?php include_once("includes/constant.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
	<title>.::Federaci&oacute;n Deportiva Peruana de Tenis</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="apple-mobile-web-app-capable" content="yes">

	<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Terminal+Dosis:500' rel='stylesheet' type='text/css' />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
	<script src="js/jquery.mCustomScrollbar.js"></script>
	<link rel="stylesheet" type="text/css" href="css/skeleton.css">
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
    <link rel="stylesheet" type="text/css" href="css/deportes_interior.css">
    <link rel="stylesheet" type="text/css" href="css/slider.css">
   
    <link rel="stylesheet" type="text/css" href="css/flex.css">

	<link href="css/slides.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery.browser.min.js"></script>
	<script type="text/javascript" src="js/slides.min.jquery.js"></script>
	
	<script type="text/javascript">var _gaq = _gaq || [];_gaq.push(['_setAccount', 'UA-21199236-4']);_gaq.push(['_trackPageview']);(function() {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);})();</script>

	<script language="javascript" type="text/javascript" src="js/funciones.tenis.js"></script>
	<script type="text/javascript">$(document).ready(function(){var jash = window.location.hash;if(jash=="#noticias"){$("html, body").animate({scrollTop:"900px"});}});</script>
    <script type="text/javascript" src='js/scrollingcarousel.2.0.min.js'></script>


</head>
<body>

<script type="text/javascript">
        $(document).ready(function () {
            $('#carousel-demo1').scrollingCarousel({
                autoScroll: true,

            }
            );
        });
    </script>

<div id="header" style="background-image:url(img/top_back_r.png)"> 

            <div class="container">

                <div class="four columns">
                    <div class="ocultar" ><div style="height:50px"></div></div>

                    <div id="alinear_i">
                        <a href="index.php"><img style="padding-top: 16px; 
    z-index: 999;" src="img/png/logo_tenisperu2.png" align="middle" border="0" /></a>
                    </div>
                </div>

                <div class="nine columns">
                    <div class="thin-sep"></div>
                    <div id="alinear_d" style="margin-top: -5px; ">
                        Telf.: (+5913) 7195290 | 7195289 | 7195288
                        
                         </div> 

                </div>


                <div id="alinear">
                    <a href="https://www.facebook.com/Federaci%C3%B3n-Deportiva-Peruana-de-Tenis-473451966072809/?fref=ts" target="_blank"><img src="http://somosteamperu.pe/imagenes/redes/1-team-peru.png" alt="Facebook" border="0" /></a> 
                    <a href="https://twitter.com/GobSantaCruz" target="_blank"><img src="http://somosteamperu.pe/imagenes/redes/4-team-peru.png" alt="Twitter" border="0" /></a> 
                    <a href="https://www.youtube.com/user/Gobdeptalautonomo" target="_blank"><img src="http://somosteamperu.pe/imagenes/redes/3-team-peru.png" alt="Youtube" border="0" /></a> 
                   
                </div> 



                <div class="twelve columns">
                    <style>
                        .clearfix:after					{ visibility: hidden; display: block; font-size: 0; content: ' '; clear: both; height: 0; }
                        * html .clearfix 				{ zoom: 1; }
                        *:first-child+html .clearfix	{ zoom: 1; }
                        .cleaner 						{ height: 0; line-height: 0; clear: both; }
                        ul, ol					{ list-style: none; }

                        #nav
                        {
                            font-family: 'Oxygen';
                            font-style: normal;
                            font-weight: 400;
                            position: relative;
                            margin:0;
                            padding:0;
                            height:auto;
                            float:right;
                            text-align:center;
                            font-size: 16px; /* 24 */
                            margin-left: 50px;

                        }

                        #nav > a
                        {
                            display: none;
                        }

                        #nav li
                        {
                            position: relative;
                        }
                        #nav li a
                        {
                            text-decoration:none;
                            display: block;
                        }
                        #nav li a:active
                        {
                            /*background-color: #FFF !important;*/
                        }

                        #nav span:after
                        {
                            width: 0;
                            height: 0;
                            border: 1px solid transparent; /* 5 */
                            border-bottom: none;
                            /*border-top-color: #efa585;*/
                            content: '';
                            vertical-align: middle;
                            display: inline-block;
                            position: relative;
                            right: 5px; /* 5 */
                        }

                        /* first level */

                        #nav > ul
                        {
                            height: auto; /* 60 */
                            right:0;
                            padding:0;
                            margin:8px 0;
                        }
                        #nav > ul > li
                        {
                            /*width: 25%;*/
                            height: auto;
                            float: left;

                        }
                        #nav > ul > li > a
                        {
                            font-family: 'Oxygen';
                            font-style: normal;
                            font-weight: 400;
                            height: auto;
                            font-size: 16px; /* 24 */
                            line-height: 20px; /* 60 (24) */
                            text-align: center;
                            padding: 0 20px; 
                            margin:30px 0 10px 0; 
                            color:#474747;
                            border-right:#474747 thin solid;
                        }
                        #nav > ul > li:not( :last-child) > a
                        {
                        }
                        #nav > ul > li:hover > a,
                        #nav > ul:not( :hover) > li.active > a
                        {

                            color:black;
                        }


                        /* second level */

                        #nav li ul
                        {
                            background-color: #A41430;
                            display: none;
                            position: absolute;
                            top: 100%;
                            width:250px;
                            padding:5px;
                            z-index:99999999999999999;
                        }
                        #nav li:hover ul
                        {
                            display: block;
                            left: 0;
                            right: 0;
                        }
                        #nav li:not( :first-child):hover ul
                        {
                            left: -1px;
                        }
                        #nav li ul a
                        {
                            font-size: 16px; /* 20 */
                            border-bottom: 1px solid white;
                            padding: 5px; /* 15 (20) */
                            color:#FFFFFF;
                            text-align:left;
                        }
                        #nav li ul li a:hover,
                        #nav li ul:not( :hover) li.active a
                        {
                              background-color: #9E0000;
    color: white;


                        }


                        @media only screen and ( max-width: 960px ) /* 640 */
                        {
                            html
                            {
                                font-size: 75%; /* 12 */
                            }

                            #nav
                            {
                                position: fixed;
                                margin:10px auto;
                                top:-10px;
                                right:0;
                                left:0;
                                width:100%;
                                z-index:99999999999999999999;
                                background-color: #9E0000;

                            }
                            #nav > a
                            {
                                width: 40px; /* 50 */
                                height: 40px; /* 50 */
                                text-align: left;
                                text-indent: -9999px;
                                background-color: #9E0000;
                                position: relative;

                            }
                            #nav > a:before
                            {
                                content: "";
                                position: absolute;
                                left:25%;
                                top: 25%;
                                width: 1.5em;
                                padding:0.1em;
                                background: #FFF;
                                box-shadow: 
                                    0 0.45em 0 0 #FFF,
                                    0 0.9em 0 0 #FFF;

                            }
                            #nav > a:after
                            {
                                top: 60%;
                            }



                            #nav:not( :target) > a:first-of-type,
                            #nav:target > a:last-of-type
                            {
                                display: block;
                            }


                            /* first level */

                            #nav > ul
                            {
                                height: auto;
                                display: none;
                                position: absolute;
                                left: 0;
                                right: 0;
                                top:25px;
                                padding:0;
                                z-index:99999999999999999;
                                background-color:#9E0000;

                            }
                            #nav:target > ul
                            {
                                display: block;
                            }
                            #nav > ul > li
                            {
                                /*width: 100%;*/
                                padding:0;
                                float: none;
                            }
                            #nav > ul > li > a
                            {
                                height: auto;
                                text-align: left;
                                padding: 2px; /* 20 (24) */
                                margin:10px;
                                color:#FFFFFF;

                            }
                            #nav > ul > li:not(:last-child) > a
                            {
                                border-right: none;
                                border-bottom: 1px solid #FFFFFF;
                            }


                            /* second level */

                            #nav li ul
                            {
                                position: static;
                                padding: 20px 20px 0 20px; /* 20 */
                                padding-top: 0;
                                width:100%;
                                border-bottom: 0;
                            }
                        }

                    </style>


	<div id="nav">
		<?php include_once("includes/base/top_menu.php"); ?>
	</div>
</div></div></div>
<div class="linea-top"></div>
<div class="linea-sep1"></div>

<div id="bd_tenis">


       <div class="front" class="container_margin" >
                <?php include_once("includes/modulo/modulo.portada.php"); ?>
        </div>


    <div class="slidernoticia" style="height: 40px;">
        <div class="container">
         <div class="pimentel-container carrousel-container">

                <div  class="test" style="height: 40px; font-size: 18px; font: bold; float: left; margin-left: 10px; margin-top: 10px; position: absolute;">
                <a href="">Últimas noticias
                </a>
                </div>
                <div class="imgarrow"  style="margin-left: 130px; margin-top: 2px; position: absolute;">
                <img class="uu" src="img/arrownews.png" class="responsive">
                </div>

                <div id="carousel-demo1" class="carousel-demo">
                <?php include_once("includes/modulo/content.more.portada.php");?>
                </div>
        
        </div>
        </div>
    </div>

        <div id="informacion">
            <div class="container">
            <div class="informacion_f nopadding">
                <?php include_once("includes/modulo/modulo.informacion_deportiva.php"); ?>
            </div>
            <div class="ranking_f padding">
                <?php //include_once("includes/modulo/modulo.ranking.php"); ?>
                <?php //include_once("includes/modulo/modulo.torneo.php");?>
                <?php include_once("includes/modulo/modulo.inscripcion.php"); ?>
            </div>
            </div>
        </div>
                
        <div id="newnoticia">
            <?php include_once 'includes/modulo/modulo.notice.php'; ?>
        </div>
        <div class="linea-sep"></div>

        <div id="multimedia">
            <div class="container">
                <div class="one_half padding">
                <?php include_once 'includes/modulo/modulo.gallery.php'; ?>
                </div>
                <div class="one_half padding">
                <?php include_once 'includes/modulo/modulo.video.php'; ?>
                </div>
            </div>
        </div>
        
        <div class="container">
              <?php include_once("includes/modulo/modulo.enlaces.php"); ?>           
            
             <?php include_once("includes/modulo/modulo.auspiciador.php"); ?>
        </div>
        


        <?php include_once("includes/base/pie_pag.php"); ?>
        
        

</div>



<script type="text/javascript" src="js/owl.carousel.min.js"></script>
             
<!-- Frontpage Demo -->
<script>

            $(document).ready(function ($) {
                $("#owl-example").owlCarousel();
            });

            $(document).ready(function ($) {
                $("#owl-example2").owlCarousel();
            });

            $(document).ready(function ($) {
                $("#owl-example3").owlCarousel();
            });

            $(document).ready(function ($) {
                $("#owl-example4").owlCarousel();
            });

            $(document).ready(function ($) {
                $("#owl-example5").owlCarousel();
            });

            $("body").data("page", "frontpage");

</script>
<script type="text/javascript" src="js/bootstrap-collapse.js"></script>
<script type="text/javascript" src="js/bootstrap-transition.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>

<script type="text/javascript" src="js/google-code-prettify/prettify.js"></script>
<script type="text/javascript" src="js/application.js"></script>
<script stype="text/javascript" src="js/flex.js"></script>
<script type="text/javascript">
            $(function () {
                SyntaxHighlighter.all();
            });
            $(window).load(function () {
                $('.flexslider').flexslider({
                    animation: "slide",
                    animationLoop: false,
                    itemWidth: 210,
                    itemMargin: 5,
                    minItems: 2,
                    maxItems: 4,
                    start: function (slider) {
                        $('body').removeClass('loading');
                    }
                });
            });
</script>


<script type="text/javascript">
    jQuery(function ($) {
        var disqus_loaded = false;
        var top = $("#faq").offset().top;
        var owldomain = window.location.hostname.indexOf("owlgraphic");
        var comments = window.location.href.indexOf("comment");

        if (owldomain !== -1) {
            function check() {
                if ((!disqus_loaded && $(window).scrollTop() + $(window).height() > top) || (comments !== -1)) {
                    $(window).off("scroll")
                    disqus_loaded = true;
                    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                    var disqus_shortname = 'owlcarousel'; // required: replace example with your forum shortname
                    var disqus_identifier = 'OWL Carousel';
                    //var disqus_url = 'http://owlgraphic.com/owlcarousel/';
                    /* * * DON'T EDIT BELOW THIS LINE * * */
                    (function () {
                        var dsq = document.createElement('script');
                        dsq.type = 'text/javascript';
                        dsq.async = true;
                        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    })();
                }
            }
            $(window).on("scroll", check)
            check();
        } else {
            $('.disqus').hide();
        }
    });
</script>


<!-- InstanceEndEditable -->
</body>
</html>