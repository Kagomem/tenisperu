function pagina_datos(url, id, pag) {
	$
			.ajax({
				url : 'pg.tenis.php?mc=' + url,
				cache : false,
				type : "GET",
				data : 'p=' + pag,
				beforeSend : function() {
					$("#" + id).empty();
					$("#" + id).append('<div class="loading_anterior"><img src="img/gif/loading.gif" width="50" height="50" /></div>');
				},
				success : function(datos) {
					$("#" + id).html('' + datos);
				}
			})
}
$(document).ready(
		function() {
			$("#panel_notice_container_ultimo").mCustomScrollbar("vertical",
					400, "easeOutCirc", 1.05, "auto", "yes", "yes", 10);
			$("#panel_notice_container_destacado").mCustomScrollbar("vertical",
					400, "easeOutCirc", 1.05, "auto", "yes", "yes", 10);
			$("#panel_notice_container_anterior").mCustomScrollbar("vertical",
					400, "easeOutCirc", 1.05, "auto", "yes", "yes", 10);
			$(".tab_content").hide();
			$("ul.tabs li:first").addClass("active").show();
			$(".tab_content:first").show();
			$("ul.tabs li").click(function() {
				$("ul.tabs li").removeClass("active");
				$(this).addClass("active");
				$(".tab_content").hide();
				var activeTab = $(this).find("a").attr("href");
				$(activeTab).fadeIn();
				return false
			})
		});
$(document).ready(function() {
	slide_front();
	slide_front_start();
	$(".tab_contentSI").hide();
	$("ul.tabsSI li:first").addClass("active").show();
	$(".tab_contentSI:first").show();
	$("ul.tabsSI li").click(function(event) {
		event.preventDefault()
	});
	$("ul.tabsSI li").click(function() {
		$("ul.tabsSI li").removeClass("active");
		$(this).addClass("active");
		$(".tab_contentSI").hide();
		var activeTab = $(this).find("a").attr("href");
		$(activeTab).fadeIn();
		return false
	})
});
$(document).ready(function() {
	$(".tab_contentGlle").hide();
	$(".optionGallery .opti:first a").addClass("a3g4t5d").show();
	$(".tab_contentGlle:first").show();
	$(".optionGallery .opti a").click(function(event) {
		event.preventDefault()
	});
	$(".optionGallery .opti a").click(function() {
		$(".optionGallery .opti a").removeClass("a3g4t5d");
		$(this).addClass("a3g4t5d");
		$(".tab_contentGlle").hide();
		var activeTab = $(this).attr("href");
		console.info("a3g4t5d" + activeTab);
		$(activeTab).fadeIn();
		return false
	})
});
$(document).ready(function() {
	;
	$(document).on('click',".uopa1ufg5p",function(event) {
		event.preventDefault();
		return false
	});
	$(document).on('click',".uopa1ufg5p",function() {
		var href = $(this).attr("href");
		visor_index_(href);
		$('html').addClass('theaterMode');
		$('body').css({
			overflow : "hidden"
		});
		$('.container_popup').css({
			overflow : "scroll",
			height : $('html').height()
		});
		$('.back_popup').css({
			height : $('html').height()
		});
	});
	$('.tn-image').on('click',function(){
		var gall_id = $('input[name="id_gallery_re"]',this).val();
		$.ajax({
			type: 'POST',
			url : 'includes/modulo/content.imagen.php',
			data: 'id='+gall_id,
			beforeSend : function() {
				$('.tab_image_container').html(
								'<div class="loading"><img src="img/gif/loading.gif" width="50" height="50" /></div>')
			},
			success : function(datos) {
				$(".tab_image_container").html('' + datos);
			}
		})
	});
	$('.tn-video').on('click',function(){
		var gall_id = $('input[name="id_video_re"]',this).val();
		$.ajax({
			type: 'POST',
			url : 'includes/modulo/content.video.php',
			data: 'id='+gall_id,
			beforeSend : function() {
				$('.tab_video_container').html(
								'<div class="loading"><img src="img/gif/loading.gif" width="50" height="50" /></div>')
			},
			success : function(datos) {
				$(".tab_video_container").html('' + datos);
			}
		})
	});

});
function slide_front(){
	$('.item_front_more').on('click',function(){
		$('.item_front_more').removeClass('');
		$(this).addClass('');
		var thisIndex = $(this).index();
		$('.item_front').css({'opacity':'0','z-index':'0'});
		$('.item_front:eq('+thisIndex+')').css({'opacity':'1','z-index':'2'});
		slide_stop();
		slide_front_start();
	});
}
function slide_front_start(){
	var activeIndex = $('.item_front_more').index();
	var cont = 0;
	var eqNext = (activeIndex==3)? 0: activeIndex+1;
	slide_interval = setInterval(function() {
		if (cont <= 3) {
			$('.item_front').css({'opacity':'0','z-index':'0'});
	    	$('.item_front:eq('+eqNext+')').css({'opacity':'1','z-index':'2'});
	    	$('.item_front_more').removeClass('');
	    	$('.item_front_more:eq('+eqNext+')').addClass('');
	    	if (cont==3) {cont=0;}else{cont++;}
	    	if (eqNext==3) {eqNext=0;}else{eqNext++;}
		}
	}, 5000);
}
function slide_stop(){
	clearInterval(slide_interval);
}
function visor_index_(url) {
	$('#container_popup').fadeIn('normal');
	$
			.ajax({
				url : url,
				cache : false,
				beforeSend : function() {
					$(".panel_popup").empty();
					$(".panel_popup")
							.append(
									'<div class="loading"><img src="img/gif/loading.gif" width="95" height="95" /></div>')
				},
				type : "GET",
				success : function(datos) {
					$(".panel_popup").html(datos)
				}
			})
}
function rankingChange(id, cat) {
	$
			.ajax({
				url : 'includes/modulo/actions/pg.ranking.php',
				cache : false,
				beforeSend : function() {
					$("#" + id).empty();
					$("#" + id)
							.append(
									'<div class="loading"><img src="img/gif/loading.gif" width="50" height="50" /></div>')
				},
				type : "GET",
				data : 'cat=' + cat,
				success : function(datos) {
					$("#" + id).html('' + datos)
				}
			})
}