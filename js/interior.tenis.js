$(document).ready(function(){
	validateAfiliacion();
	validateData();
	inscription();
	$('.btn_prev').click(function(){
		viewModInscription($(this));
	});
	$('#date_text').datepicker({
		format: 'dd/mm/yyyy'
	});

	$(document).on('change','[name="photo"],[name="voucher"]',function(){
		readURL(this);
	});

	$("input").keypress(function(event) {
	    if (event.which == 13) {
	        event.preventDefault();
	    }
	});
});
function readURL(input){
		var reader = new FileReader();
		if(	input.files[0].type=='image/jpeg'||input.files[0].type=='image/png'||input.files[0].type=='image/jpg'||
			input.files[0].type=='image/JPEG'||input.files[0].type=='image/PNG'||input.files[0].type=='image/JPG'){
			if(input.files[0].size<=1048576){
				if (input.files && input.files[0]) {
					reader.onload = function (e) {
						if ($('.athlete_details').is(':visible')) {
							$('#img-spotman').attr('src', e.target.result);
						}
						else{
							$('#img-voucher').attr('src', e.target.result);
						}
					}
				    reader.readAsDataURL(input.files[0]);
				}
			}else{
				var msg='El peso m\xE1ximo por Foto es de 1 Mb';
				if ($('.athlete_details').is(':visible')) {
					$('[name="photo"]').popover({
						content: msg
					}).popover('show');
					$('[name="photo"]').val('');
				}
				else{
					$('[name="voucher"]').popover({
						content: msg
					}).popover('show');
					$('[name="voucher"]').val('');
				}
				
			}
		}else{
			var msg='Archivo no permitido';
			if ($('.athlete_details').is(':visible')) {
				$('[name="photo"]').popover({
						content: msg
				}).popover('show');
				$('[name="photo"]').val('');
			}
			else{
				$('[name="voucher"]').popover({
						content: msg
				}).popover('show');
				$('[name="voucher"]').val('');
			}
			
		}
	}
function viewModInscription(btn){
	var btnClass = $(btn).attr('class').split(' ');
	var divIndex = $(btn).parents('div.mod_inscription').index();
	var divHidden = $(btn).parents('div.mod_inscription');
	var formMod = divHidden.parent();
	var divActive = '';
	if (btnClass[1]=="btn_next") {
		divActive = formMod.children().eq(divIndex+1);
	}else{
		divActive = formMod.children().eq(divIndex-1);
	}
	divHidden.removeClass('active');
	divActive.addClass('active');
}
function validateAfiliacion(){
	$('#submit_afiliacion').click(function(e){
		e.preventDefault();
		//debugger;
			var $btn = $(this);
			var codAfiliacion = $('#codAfiliacion');
			var valCod = $('#codAfiliacion').val();
			var valDNI = $('#DNI').val();
			var tyAcc = $('#type_acc').val();
			var mod = $(codAfiliacion).parents('div.mod_inscription');
			var btn_next = $(mod[0]).children('.sec_inscription').eq(1).children('.content_sec_inscription').children('p.sec_buttons').children('.btn_next');
			if (tyAcc == 'C') {
				if (valCod == '' || valCod == null) {
					$('#codAfiliacion').popover('show');
				}else{
					var	id_event = $('#id_event').val();
					$.ajax({
						  url: "includes/interior/inscription_data.php",
						  type: 'post',
						  data: "codAfiliacion="+valCod+'&DNI='+valDNI+'&id_event='+id_event+'&type_acc='+tyAcc,
						  beforeSend: function( xhr ) {
						    xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
						  }
						}).done(function( data ) {						
						   	if ( parseInt(data) != 0 ) {
						      alert( 'Ud. ya esta registrado en el evento.\nCualquier consulta ponerse en contacto al correo:\n\ninscripciones.menores@tenisperu.com.pe' );
						   	}
						   	else{	
								viewModInscription($btn);
								$('[name="photo"]').val('');
						   	}
					});
				};
			}
			else{
				var	id_event = $('#id_event').val();
					$.ajax({
						  url: "includes/interior/inscription_data.php",
						  type: 'post',
						  data: "codAfiliacion="+valCod+'&DNI='+valDNI+'&id_event='+id_event+'&type_acc='+tyAcc,
						  beforeSend: function( xhr ) {
						    xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
						  }
						}).done(function( data ) {
						   	if ( parseInt(data) != 0 ) {
						      alert( 'Ud. ya esta registrado en el evento.\nCualquier consulta ponerse en contacto al correo:\n\ninscripciones.menores@tenisperu.com.pe' );
						   	}
						   	else{	
								viewModInscription($btn);
								$('[name="photo"]').val('');
						   	}
					});
			}
	});
}
function validateData(){
	$('#submit_person').click(function(e){
		e.preventDefault();
			var flPhoto = $('[name="photo"]').val();
			var name_inscription = $('#name_inscription').val();
			var lastname_patern = $('#lastname_patern').val();
			var date_text = $('#date_text').val();
			var dni = $('#document_number_form_inscription').val();
			var gender = $('input[name="gender"]:checked').val();
			var mail_sportman = $('#mail_sportman').val();
			var errors = 0;
			if (name_inscription == '' || name_inscription == null || name_inscription == undefined) {
				$('#name_inscription').popover('show');
				errors = errors+1;
			};
			if (lastname_patern == '' || lastname_patern == null || lastname_patern == undefined) {
				$('#lastname_patern').popover('show');
				errors = errors+1;
			};
			if (date_text == '' || date_text == null || date_text == undefined) {
				$('#date_text').popover('show');
				errors = errors+1;
			};
			if (dni == '' || dni == null || dni == undefined) {
				$('#document_number_form_inscription').popover('show');
				errors = errors+1;
			};
			if (gender == '' || gender == null || gender == undefined) {
				$('#gender_popover').popover('show');
				errors = errors+1;
			};
			if (mail_sportman == '' || mail_sportman == null || mail_sportman == undefined) {
				$('#mail_sportman').popover({
					content: "Este campo es requerido."
				}).popover('show');
				errors = errors+1;
			}
			else if(/^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+.)+[A-Z]{2,4}$/i.test(mail_sportman)){
				errors = errors;
			}
			else{
				$('#mail_sportman').popover({
					content: "Por favor escriba un correo electronico valido."
				}).popover('show');
				errors = errors+1;
			}
			if(errors == 0){
				viewModInscription($(this));
				$('[name="voucher"]').val('');	
			}

	});
}
function inscription(){
	$('input[type="submit"]').click(function(e){debugger;
		e.preventDefault();
		var type_paying = $('#type_paying').val();
		var flvoucher = $('[name="voucher"]').val();
		var num_operation = $('#num_operation').val();
		var errors = 0;
		if (type_paying == 'B') {
			if($('[name="voucher"]').html()!=undefined){
				if (flvoucher == '' || flvoucher == null || flvoucher == undefined) {
					$('[name="voucher"]').popover({
							content: "Este campo es requerido."
					}).popover('show');
					errors = errors+1;
				};
			}
			if (num_operation == '' || num_operation == null || num_operation == undefined) {
				$('#form_inscription').submit(function(ev){
					ev.preventDefault();
				});
				$('#num_operation').popover('show');
				errors = errors+1;
			};
		}
		if(errors == 0){
			$('#form_inscription').submit();	
		};
	});
}
function deletePaste(e){ 
    return !(e.keyCode==86 && e.ctrlKey);
}

function isLetter(text){
 
	if (navigator.appName == "Netscape"){
		charCode = text.which;
	}else{
		charCode = text.keyCode;
	} 
	var inp = String.fromCharCode(charCode);
	if(/[a-zA-ZáéíóúÁÉÍÓÚñÑüÜ ]/.test(inp) || text.keyCode==8 || (text.keyCode>=37&&text.keyCode<=40)){
		return true;
	}else{
		return false;
	}
}
function isNumeric(number){
	var charCode;
	if (navigator.appName == "Netscape"){
		charCode = number.which;
	}else{
		charCode = number.keyCode;
	} 
	if((charCode<48||charCode>57)&&charCode!=13&&charCode!=8&&charCode!=0){
		
		return false;
	}else{
		return true;
	}
}
function isNumericLengthTwenty(number){
	var charCode;
	if (navigator.appName == "Netscape"){
		charCode = number.which;
	}else{
		charCode = number.keyCode;
	} 
	if((charCode<48||charCode>57)&&charCode!=13&&charCode!=8&&charCode!=0){
		
		return false;
	}else{
		var document_inscription = document.getElementById('document_number_form_inscription');
		if(document_inscription.value.length>19){
			document_inscription.value=document_inscription.value.substring(0,19);
		}
		return true;
	}
}
function isLetterAndNumber(text){
  var charCode;
	if (navigator.appName == "Netscape"){
		charCode = text.which;
	}else{
		charCode = text.keyCode;
	} 
	var inp = String.fromCharCode(charCode);
	if(/[a-zA-Z0-9 ]/.test(inp)){
		return true;
	}else{
		//alert("Por favor teclee solo texto en este campo!");
		return false;
	}
}
function isLetterAndNumberAndDash(text){
  var charCode;
	if (navigator.appName == "Netscape"){
		charCode = text.which;
	}else{
		charCode = text.keyCode;
	} 
	var inp = String.fromCharCode(charCode);
	if(/[a-zA-Z0-9 -]/.test(inp)){
		return true;
	}else{
		//alert("Por favor teclee solo texto en este campo!");
		return false;
	}
}
function isNumberAndSlash(text){
  var charCode;
	if (navigator.appName == "Netscape"){
		charCode = text.which;
	}else{
		charCode = text.keyCode;
	} 
	var inp = String.fromCharCode(charCode);
	if(/[0-9\/]/.test(inp)){
		return true;
	}else{
		//alert("Por favor teclee solo texto en este campo!");
		return false;
	}
}
function isMail(email){
    var charCode;
	if (navigator.appName == "Netscape"){
		charCode = email.which;
	}else{
		charCode = email.keyCode;
	} 
	var inp = String.fromCharCode(charCode);
	if(/^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+.)+[A-Z]{2,4}$/i.test(inp)){
		return true;
	}else{
		//alert("Por favor teclee solo texto en este campo!");
		return false;
	}
}