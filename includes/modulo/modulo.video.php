<div class="headline-section">
	<span>Videos
	<strong class="theme-secondary "></strong>
	</span>	
</div>
<script>
	$(function(){
		$('#slide_videos').slides({
			preload: true,
			preloadImage: 'img/gif/loading.gif',
			play: 4000,
			pause: 1000,
			hoverPause: true,
			effect:"fade",
			next:"GVnext",
			prev:"GVprev",
			generateNextPrev: false,
			generatePagination: false
		});
	});
</script>
<div id="panel_video_container" class="tab_containerGlle">
	<div class="tab_video_related display_inline" id="contentVideo">
		<div id="slide_videos">
			<div class="slides_container">
				<?php include_once("content.video.related.php");?>
			</div>
			<a href="#" class="GVprev">&laquo;</a><a href="#" class="GVnext">&raquo;</a>
		</div>
	</div>
</div>
