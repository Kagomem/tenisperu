
<div class="container">
<div class="headline-section">
	<span>
	Noticias
	<strong class="theme-secondary "></strong>
	</span>	
</div>
<div id="panel_option_modulo_news">
	<div class="option_container">
		<ul class="content_option tabs">
			<li class=""><a href="#NtUltimo">&Uacute;ltimas</a></li>
			<li><a href="#NtDestacado">Destacadas</a></li>
			<li><a href="#NtAnterior">Anteriores</a></li>
		</ul>
	</div>
</div>
<div class="tab_container nopadding">
	<div id="NtUltimo" class="tab_content">
	<?php include_once("list.noticia.ultimo.php");?>
	</div>
	<div id="NtDestacado" class="tab_content">
	<?php include_once("list.noticia.destacado.php");?>
	</div>
	<div id="NtAnterior" class="tab_content">
		<?php include_once("list.noticia.anterior.php");?>
	</div>
</div>
</div>