<style>
.tn-nt-title {
	padding: 0px 0px 0px 0px;
	font-size: 12px;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('#first_line_separate').css({'height':'34px','margin-bottom':'-10px'});
		$('#second_line_separate,#third_line_separate').css({'height':'18px','margin-bottom':'-3px'});
		$('#option_TnsAviso').click(function(e){
			$('#first_line_separate').css({'height':'34px','margin-bottom':'-10px'});
			$('#second_line_separate,#third_line_separate').css({'height':'18px','margin-bottom':'-3px'});
		});
		$('#option_TnsFicha').click(function(e){
			$('#first_line_separate').css({'height':'34px','margin-bottom':'-10px'});
			$('#second_line_separate').css({'height':'34px','margin-bottom':'-10px'});
			$('#third_line_separate').css({'height':'18px','margin-bottom':'-3px'});
		});
		$('#option_TnsCalendar').click(function(e){
			$('#third_line_separate').css({'height':'34px','margin-bottom':'-10px'});
			$('#second_line_separate').css({'height':'34px','margin-bottom':'-10px'});
			$('#first_line_separate').css({'height':'18px','margin-bottom':'-3px'});
		});
		$('#option_TnsTorneo').click(function(e){
			$('#third_line_separate').css({'height':'34px','margin-bottom':'-10px'});
			$('#second_line_separate,#first_line_separate').css({'height':'18px','margin-bottom':'-3px'});
		});
	});
</script>
<!--<div
	class="dpt_titulo_modulo tn_backgroung_titulo_modulo tn_backgroung_titulo_modulo_azul corner_div_noBottom_6">
	<div class="tn-titulo-container">
		<span class="style_font">Informaci&oacute;n Deportiva</span>
	</div>
</div>-->


<div class="three_fifth padding">
<div id="panel_option_modulo" class="border-bottom">
	<div class="option_container_s">
		<ul class="content_option tabsSI">
			<li id="option_TnsAviso" class="active"><a href="#TnsAviso">Avisos</a></li><!--
		 --><li id="first_line_separate" class="line_separate"></li><!--
		 --><li id="option_TnsFicha"><a href="#TnsFicha">Descargar Fichas</a></li><!--
		 --><li id="second_line_separate"class="line_separate"></li><!--
		 --><li id="option_TnsCalendar"><a href="#TnsCalendar">Calendarios</a></li><!--
		 --><li id="third_line_separate" class="line_separate"></li><!--
		 --><li id="option_TnsTorneo"><a href="#TnsTorneo">Torneos</a></li>
		</ul>
	</div>
</div>
	<div class="tab_containerSI">
		<div id="TnsAviso" class="tab_contentSI">
		<?php include_once("list.informacion_deportiva.aviso.php");?>
		</div>
		<div id="TnsFicha" class="tab_contentSI">
		<?php include_once("list.informacion_deportiva.ficha.php");?>
		</div>
		<div id="TnsCalendar" class="tab_contentSI">
		<?php include_once("list.informacion_deportiva.calendario.php");?>
		</div>
		<div id="TnsTorneo" class="tab_contentSI">
		<?php include_once("list.informacion_deportiva.torneo.php");?>
		</div>
	</div>
</div>
