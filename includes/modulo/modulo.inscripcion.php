<?php 
	$rankeados=$objPortada->lista_ranking("MENORES","nada","nada");
	$categorias=$objPortada->getListCategorie();
?>
<style>
.panel_option_combo {
	z-index: 10;
	padding: 2px 3px 2px 100px;
	text-align: right;
	position: absolute;
	right: 0px;
	top: 0px;
}

.panel_option_combo .option_combo_modulo {
	display: inline-block;
	margin: 0px 2px;
	position: relative;
	z-index: 1;
	height: 24px;
	background-color: #D5DFE5;
	float: right;
}

.panel_option_combo .option_combo_modulo .combo_label {
	padding: 3px 5px;
	margin-right: 25px;
}

.panel_option_combo .option_combo_modulo .combo_label span {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #666;
}

.panel_option_combo .option_combo_modulo .combo_icon {
	border-left-width: 1px;
	border-left-style: solid;
	border-left-color: #D5DFE5;
	position: absolute;
	top: 0px;
	right: 0px;
	display: inline-block;
	height: 100%;
	width: 25px;
}

.panel_option_combo .option_combo_modulo .combo_icon a {
	display: inline-block;
	padding: 12px 13px;
	background-image: url(img/png/icon_combobox.png);
	background-repeat: no-repeat;
	background-position: 7px 7px;
	position: absolute;
	z-index: 5;
	top: 0;
	right: 0
}

.panel_option_combo .option_combo_modulo .list_option_combo {
	display: none;
	position: absolute;
	background-color: hsl(0, 0%, 97%);
	border: 1px solid hsl(0, 0%, 73%);
	padding: 3px;
	width: 100px;
	left: 0px;
	top: 24px;
	visibility: visible;
	-webkit-border-radius: 0px 4px 4px 4px;
	-moz-border-radius: 0px 4px 4px 4px;
	border-radius: 0px 4px 4px 4px;
}

.panel_option_combo .option_combo_modulo .list_option_combo ul {
	display: block;
}

.panel_option_combo .option_combo_modulo .list_option_combo li {
	display: block
}

.panel_option_combo .option_combo_modulo .list_option_combo li a {
	padding: 3px 5px;
	display: block;
	text-align: left;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #868686;
	text-decoration: none;
}

.panel_option_combo .option_combo_modulo .list_option_combo li a:hover {
	background-color: #DDF9F0;
	color: #2D5920
}
</style>
<div class="border-bottom" style="height: 45px; position:relative; background: #fff;padding-bottom: 5px;">
	<div class="tn-titulo-container">
		<span class="title_section">INSCRIPCI&Oacute;N A EVENTOS</span>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() { 
		$("#combo_sexo .combo_icon a").click(function(event) {
			event.preventDefault();
			$("#combo_sexo .list_option_combo").toggle();
			return false;
		});
		$("#combo_sexo .list_option_combo a").click(function(event){
			var str = $(this).text();
			var val = $(this).attr('dir');
			$("#combo_sexo .combo_label span").html(str);
			$("#combo_sexo .combo_label input").attr('value',val);
			$(".list_option_combo").hide();
		});$("#combo_cate .combo_icon a").click(function(event) {
			event.preventDefault();
			$("#combo_cate .list_option_combo").toggle();
			return false;
		});
		$("#combo_cate .list_option_combo a").click(function(event){S
			var str = $(this).text();
			var val = $(this).attr('dir');
			$("#combo_cate .combo_label span").html(str);
			$("#combo_cate .combo_label input").attr('value',val);
			$(".list_option_combo").hide();
		});
		$("#combo_sexo .combo_icon a").click(function(event) {
			event.preventDefault();
		});
		$("#combo_cate .combo_icon a").click(function(event) {
			event.preventDefault();
		});
		$(".list_option_combo a").click(function(event){
			event.preventDefault();
		});
		$("body").click(function(){
			$(".list_option_combo").hide();
		});
	});
	</script>
<div class="panel_modulo_container panel_ranking corner_div_6">
	<!-- <img src="img/png/pronto.png" style="height: 100%;" alt="muy pronto"> -->
	<?php include_once('list.inscripcion.php'); ?>
    
    <div style="">
        <a href="interior.tenis_deportistas.php">
            <img src="img/png/ficha.png" style="width: 270px;
    margin-top: 35px;  margin-botton:10px ">    
        </a>
        
    </div>
</div>
