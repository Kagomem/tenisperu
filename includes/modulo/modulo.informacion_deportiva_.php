
<div
	class="dpt_titulo_modulo tn_backgroung_titulo_modulo tn_backgroung_titulo_modulo_azul corner_div_noBottom_6">
	<div class="tn-titulo-container">
		<span class="style_font">Informaci&oacute;n Deportiva</span>
	</div>
</div>
<div id="panel_option_modulo">
	<div class="option_container">
		<ul class="content_option tabsSI">
			<li class="active"><a href="#TnsAviso">Avisos</a></li>
			<li><a href="#TnsFicha">Ficha de Inscripci&oacute;n</a></li>
			<li><a href="#TnsCalendar">Calendario</a></li>
		</ul>
	</div>
</div>
<div class="tab_containerSI">
	<div id="TnsAviso" class="tab_contentSI">
	<?php include_once("list.informacion_deportiva.aviso.php");?>
	</div>
	<div id="TnsFicha" class="tab_contentSI">
	<?php include_once("list.informacion_deportiva.ficha.php");?>
	</div>
	<div id="TnsCalendar" class="tab_contentSI">
	<?php include_once("list.informacion_deportiva.calendario.php");?>
	</div>
</div>
