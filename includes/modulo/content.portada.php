<?php 
	include_once("includes/query/tenis.class.php");
	$objPortada=new Tenis;
	$result=$objPortada->getListFront(" WHERE f.state_front=1 ORDER BY f.date_register DESC LIMIT 4");
	$registros=$objPortada->numRegistro;
	if ($registros==0){
		echo '<div class="nofound">En breve se publicar&aacute;n NOTICIAS DESTACADAS</div>';
	}
	for($i=1;$i<=$registros;$i++){
		$fecha = $result[$i-1]['3'];
?>
		
		<div class="item_front <?php echo ($i==1) ? 'active' : '' ; ?>">
			<div class="tn-image">
				<a
					href="interior.popup.php?p=<?php echo md5("portada.individual"); ?>&i=<?php echo $result[$i-1]['0']; ?>"
					title="<?php echo utf8_encode($result[$i-1]['1']); ?>" 
					target="_blank">
					<img class="img_noborder" src="<?php echo $path_image_portada.$result[$i-1]['3']; ?>"width="535"  alt="Slide 1">
				</a>
			</div>
			<div class="tn-caption">
				<div class="tn-title">
					<a
						href="interior.popup.php?p=<?php echo md5("portada.individual"); ?>&i=<?php echo $result[$i-1]['0']; ?>"
						title="<?php echo $result[$i-1]['1']; ?>" 
						target="_blank">
						<?php echo $result[$i-1]['1']; ?>
					</a>
				</div>
				<div class="tn-nt-content">
				<?php 
					$conte=strip_tags($result[$i-1]['2']); 
					echo substr($conte,0,1),'...'; 
				?>
				</div>
			</div>
		</div>
<?php } ?>