<script language="javascript">
    $(document).ready(function() {
        $(".close_popup").click(function() {
            $('#container_popup').hide();
            $('.panel_popup').empty();
            $('html').removeClass('theaterMode');
            $('body').css({overflow: "auto"});
            $('#container_popup').css({overflow: "auto"});
        });
    });
</script>
<?php
$id_tournament = $_REQUEST["i"];
include_once("includes/query/tenis.class.php");
$objData = new Tenis;
$result = $objData->getListTournament('WHERE state_tournament=1 AND id_tournament=' . $id_tournament);
$registros = $objData->numRegistro;
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
for ($i = 0; $i < $registros; $i++) {
    $title = $result[$i]['1'];
    $fecha = $result[$i]['3'];
    $contenido = $result[$i]['2'];
    ($imagen == '') ? $imagen = 'noticia-default_1305222383.png' : '';
}
list($año, $mes, $dia) = split('[/.-]', $fecha);
$mes = intval($mes);
$fecha_ = $dia . " de " . $meses[$mes - 1] . " del " . $año;
?>


<div style = "background-color: #FFF;" class = "redondeo-div">
    <div class = "font-family-body body-content"
         style = "padding-bottom: 20px; padding-top: 5px">
        <table width = "96%" border = "0" align = "center" style = "margin: auto">
            <tr>
                <td valign = "top"><div align = "center" class = "noti-inte-titulo">
                        <h3>
                            <?php echo $title;
                            ?>
                        </h3>
                        <div align="left"
                             style="color: #666; font-size: 11px; padding: 5px 5px 0px 15px">
<?php echo 'Fecha: ' . $fecha_; ?>
                        </div>
                    </div>
                </td>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td width="69%" valign="top"><div class="noti-inte-contenido"
                                                  style="overflow: auto">
<?php echo $contenido; ?>
                    </div>
                </td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="30%" valign="top"><div class="cont_anex">
                        <h4>Otros Torneos</h4>
<?php include_once("anex_torneo.php"); ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row">&nbsp;</th>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
</div>
