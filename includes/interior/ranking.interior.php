<div class="contd">
<?php 
	include_once("includes/query/tenis.class.php");
	$objPortada=new Tenis;
	$rankeados=$objPortada->lista_ranking("MENORES","nada","nada");
	$categorias=$objPortada->getListCategorie();
?>
	<style>
#_deporte_interior .body-content a {
	color: #666;
}

.panel_option_combo {
	z-index: 10;
	padding: 0px 3px 2px 200px;
	text-align: right;
	position: absolute;
	left: 0px;
	top: 0px;
}

.panel_option_combo .option_combo_modulo {
	display: inline-block;
	margin: 0px 2px;
	position: relative;
	z-index: 1;
	height: 24px;
	background-color: #F8F8F8;
	border: 1px solid #D5DFE5;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	float: right;
}

.panel_option_combo .option_combo_modulo .combo_label {
	padding: 3px 5px;
	margin-right: 25px;
}

.panel_option_combo .option_combo_modulo .combo_label span {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #666;
}

.panel_option_combo .option_combo_modulo .combo_icon {
	border-left-width: 1px;
	border-left-style: solid;
	border-left-color: #D5DFE5;
	position: absolute;
	top: 0px;
	right: 0px;
	display: inline-block;
	height: 100%;
	width: 25px;
}

.panel_option_combo .option_combo_modulo .combo_icon a {
	display: inline-block;
	padding: 12px 13px;
	background-image: url(img/png/icon_combobox.png);
	background-repeat: no-repeat;
	background-position: 7px 7px;
	position: absolute;
	z-index: 5;
	top: 0;
	right: 0
}

.panel_option_combo .option_combo_modulo .list_option_combo {
	display: none;
	position: absolute;
	background-color: hsl(0, 0%, 97%);
	border: 1px solid hsl(0, 0%, 73%);
	padding: 3px;
	width: 100px;
	left: 0px;
	top: 24px;
	visibility: visible;
	-webkit-border-radius: 0px 4px 4px 4px;
	-moz-border-radius: 0px 4px 4px 4px;
	border-radius: 0px 4px 4px 4px;
}

.panel_option_combo .option_combo_modulo .list_option_combo ul {
	display: block;
}

.panel_option_combo .option_combo_modulo .list_option_combo li {
	display: block
}

.panel_option_combo .option_combo_modulo .list_option_combo li a {
	padding: 3px 5px;
	display: block;
	text-align: left;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #868686;
	text-decoration: none;
}

.panel_option_combo .option_combo_modulo .list_option_combo li a:hover {
	background-color: #DDF9F0;
	color: #2D5920
}

#_deporte_interior .body-content ul {
	padding: 0px;
}
</style>
	<div class="dpt_titulo_modulo" style="height: 30px">
		<span style="padding-left: 100px; font-size: 16px; font-weight: bold;">Categor&iacute;a:</span>
		<div class="panel_option_combo" style="left: 0px;">
			<div class="option_combo_modulo" id="combo_cate">
				<div class="combo_label">
					<span>Todos</span><input name="CTvalue" type="hidden" value="nada" />
				</div>
				<div class="combo_icon">
					<a href=""></a>
				</div>
				<div class="list_option_combo">
					<ul>
						<li><a href="" dir="nada"
							onclick="rankingChange('panel_list_rankig','nada');">Todos</a>
						</li>
						<?php foreach($categorias as $categoria){ ?>
						<li><a href="" dir="<?php echo $categoria[0]; ?>" onclick="rankingChange('panel_list_rankig','<?php echo $categoria[0]; ?>');">
							<?php echo (($categoria[2]=='1')?'Damas ':'Varones ').$categoria[1]; ?>
						</a>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="panel_list_rankig" style="width: 80%; margin: auto;">
		<div class="tn_ranking_list_header corner_div_noBottom_6">
			<div class="tn_ranking_list_rank" style="width: 10%;">
				<strong>Rank</strong>
			</div>
			<div class="tn_ranking_list_deport" style="width: 40%;">
				<strong>Deportistas</strong>
			</div>
			<div class="tn_ranking_list_point" style="width: 25%;">
				<strong>Ciudad</strong>
			</div>
			<div class="tn_ranking_list_point" style="width: 15%;">
				<strong>Puntos</strong>
			</div>
		</div>
		<div id="panel_list_rankig">
		<?php include_once('includes/interior/list.ranking.interior.php'); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	function rankingChange(id,cat){
		$.ajax({
			url:'includes/modulo/actions/pg.ranking.interior.php',
			cache: false,
			beforeSend: function() {
				$("#"+id).empty();
				$("#"+id).append('<div class="loading"><img src="img/gif/loading.gif" width="50" height="50" /></div>');
			},
			type: "GET",data:'cat='+cat,
			success: function(datos){
				$("#"+id).html(''+datos);
			}
		});
	}
	$(document).ready(function() { 
		$("#combo_sexo .combo_icon a").click(function(event) {
			event.preventDefault();
			$("#combo_sexo .list_option_combo").toggle();
			return false;
		});
		$("#combo_sexo .list_option_combo a").click(function(event){
			var str = $(this).text();
			;var val = $(this).attr('dir');
			$("#combo_sexo .combo_label span").html(str);
			$("#combo_sexo .combo_label input").attr('value',val);
			$(".list_option_combo").hide();
		});
		$("#combo_cate .combo_icon a").click(function(event) {
			event.preventDefault();
			$("#combo_cate .list_option_combo").toggle();
			return false;
		});
		$("#combo_cate .list_option_combo a").click(function(event){
			var str = $(this).text();
			var val = $(this).attr('dir');
			$("#combo_cate .combo_label span").html(str);
			$("#combo_cate .combo_label input").attr('value',val);
			$(".list_option_combo").hide();
		});
		$("#combo_sexo .combo_icon a").click(function(event) {
			event.preventDefault();
		});
		$("#combo_cate .combo_icon a").click(function(event) {
			event.preventDefault();
		});
		$(".list_option_combo a").click(function(event){
			event.preventDefault();
		});
		$("body").click(function(){
			$(".list_option_combo").hide();
		});
	});
</script>
