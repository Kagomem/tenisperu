<?php echo get_date_spanish(time(), true, 'day').' <strong>'.date("d",time()).'</strong> de <strong>'.get_date_spanish(time(), true, 'month').' </strong>del <strong> '.date("Y", time()).'</strong>';
function get_date_spanish( $time, $part = false, $formatDate = '' ){
	$month = array("","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiempre", "Diciembre");
	$month_execute = "n";
	 $month_mini = array("","ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "DIC");
	$month_mini_execute = "n";
	 $day = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
	 $day_execute = "w";
	$day_mini = array("DOM","LUN","MAR","MIE","JUE","VIE","SAB");
	 $day_mini_execute = "w";
	$print_hoy = array("month"=>"month", "month_mini"=>"month_mini");
	if( $part === false ){
		return date("d", $time) . " de " . $month[date("n",$time)] . " del ".date("Y", $time)." , ". date("H:i",$time) ." hs";
	}elseif( $part === true ){
		if( ! empty( ${$formatDate} ) && !empty( ${$formatDate}[date(${$formatDate.'_execute'},$time)] ) ) return ${$formatDate}[date(${$formatDate.'_execute'},$time)];
	else return date($formatDate, $time);
	}else{
		return date("d-m-Y H:i", $time);
	}
}?>
