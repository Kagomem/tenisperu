<?php include_once("includes/constant.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
	<title>.::Federaci&oacute;n Deportiva Peruana de Tenis</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico"/>
	<link rel="stylesheet" href="css/base.css" />	
	<link href='css/style_o.css' rel='stylesheet' type='text/css'>
	<link href="css/deportes_styles.css" rel="stylesheet" type="text/css" />
	<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
	
	<link href='http://fonts.googleapis.com/css?family=Terminal+Dosis:500' rel='stylesheet' type='text/css' />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
	<script src="js/jquery.mCustomScrollbar.js"></script>
	
	<link href="css/slides.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery.browser.min.js"></script>
	<script type="text/javascript" src="js/slides.min.jquery.js"></script>
	
	<link href="css/deportes.interior.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">var _gaq = _gaq || [];_gaq.push(['_setAccount', 'UA-21199236-4']);_gaq.push(['_trackPageview']);(function() {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);})();</script>

	<script language="javascript" type="text/javascript" src="js/funciones.tenis.js"></script>

</head>
<body>
	<div id="header" style="margin-top: 0px;">
		<div id="title">
			<figure>
				<a href="index.php"><img src="img/png/logo_tenisperu.png"></a>
			</figure>
			<div>
				<h1>Federaci&oacute;n Deportiva Peruana de Tenis</h1>
			</div>
		</div>
		<!--div id="nav">
				
		</div-->
		<div  id="sponsors">
			<div class="views">
				<a class="visitas" href="javascript:;">
				<?php 
					include_once ('includes/modulo/contador.php');
				?>
				</a>
			</div>
			<?php include_once("includes/modulo/modulo.auspiciador.php"); ?>
		</div>
	</div>
	<div id="nav" style="position:relative;top:3px;">
		<?php include_once("includes/base/top_menu.php"); ?>
	</div>
	<div id="bd_tenis">

		<div id="front" class="container_margin">
				<?php include_once("includes/modulo/modulo.portada.php"); ?>
		</div>
		<div id="information">
			<div id="information_sport">
				<?php include_once("includes/modulo/modulo.informacion_deportiva.php"); ?>
			</div>
			<div id="ranking">
				<?php //include_once("includes/modulo/modulo.ranking.php"); ?>
				<?php //include_once("includes/modulo/modulo.torneo.php");?>
				<?php include_once("includes/modulo/modulo.inscripcion.php"); ?>
			</div>
		</div>
		<div id="new" class="container_margin">
			<?php include_once 'includes/modulo/modulo.notice.php'; ?>
		</div>
		<div id="multimedia">
			<div id="imagen">
				<?php include_once 'includes/modulo/modulo.gallery.php'; ?>
			</div>
			<div id="video">
				<?php include_once 'includes/modulo/modulo.video.php'; ?>
			</div>
		</div>
		<div id="related_link" class="container_margin">
			<span class="title_section">Enlaces Relacionados</span>
			<?php include_once("includes/modulo/modulo.enlaces.php"); ?>
		</div>
	</div>
	<div id="footer">
		<?php include_once("includes/base/pie_pag.php"); ?>
	</div>
	<?php include_once("includes/modulo/modulo.popup.php"); ?>
	<script type="text/javascript">
	$(document).ready(function() {
		$(window).scroll(function () {    
			if($(window).scrollTop()>187){
				$('#nav').css({'position':'fixed','top':'0px'});
			}else{
				$('#nav').css({'position':'relative','top':'3px'});
			}               
		 });
		});
	</script>
</body>
</html>