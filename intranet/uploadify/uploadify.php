<?php

/*
  Uploadify
  Copyright (c) 2012 Reactive Apps, Ronnie Garcia
  Released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
 */

// Define a destination
//$targetFolder = '/adoperu/img/'; // Relative to the root

if (!empty($_FILES)) {



    $rand = rand(1000000, 9999999);
    $tempFile = $_FILES['Filedata']['tmp_name'];
    $image = $_FILES['Filedata']['name'];
    $tipo_original = $_FILES['Filedata']['type'];
    //$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
   // $targetPath = '../../resource/image/body/foto/';
     $targetPath = '../../img/galeria/';




    list($ancho_original, $alto_original, $tipo_original, $alternativo) = getimagesize($tempFile);
    if ($tipo_original == IMAGETYPE_GIF) {
        $img = imagecreatefromgif($tempFile);
        $n_foto1_type_return = "gif";
    }
    if ($tipo_original == IMAGETYPE_JPEG) {
        $img = imagecreatefromjpeg($tempFile);
        $n_foto1_type_return = "jpg";
    }
    if ($tipo_original == IMAGETYPE_PNG) {
        $img = imagecreatefrompng($tempFile);
        $n_foto1_type_return = "png";
    }

    $thumb = imagecreatetruecolor(790, 445);
    imagecopyresampled($thumb, $img, 0, 0, 0, 0, 790, 445, $ancho_original, $alto_original);

    if ($tipo_original == IMAGETYPE_GIF) {
        imagegif($thumb, $tempFile);
    }
    if ($tipo_original == IMAGETYPE_JPEG) {
        imagejpeg($thumb, $tempFile, 100);
    }
    if ($tipo_original == IMAGETYPE_PNG) {
        imagepng($thumb, $tempFile);
    }

    $filedata = $rand . '.' . $n_foto1_type_return;
    $targetFile = rtrim($targetPath, '/') . '/' . $filedata;

    // Validate the file type
    $fileTypes = array('jpg', 'jpeg', 'JPG', 'png'); // File extensions
    $fileParts = pathinfo($_FILES['Filedata']['name']);
    if (in_array($fileParts['extension'], $fileTypes)) {
      move_uploaded_file($tempFile, $targetFile);
      
      echo '' . $filedata;
    } else {
        echo 'false';
    }
}
?>


