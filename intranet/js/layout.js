(function($){
	var initLayout = function() {
		$('#colorSelector').ColorPicker({
			color: '#0000ff',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$('#colorSelected').css('backgroundColor', '#' + hex);
				$('#colorpickerField').val('#' + hex);
			}
		});
	};
	EYE.register(initLayout, 'init');
})(jQuery)