function loader_formulario(id, url) {
	$.ajax({
		url : url,
		type : "GET",
		cache : false,
		data : accion = 'c',
		beforeSend : function() {
			$(".loading").fadeIn("normal")
		},
		success : function(datos) {
			$('#' + id).empty().append(datos).show();
			$('.loading').fadeOut('slow')
		}
	})
};
function NuevoDatos(url) {
	$.ajax({
		type : "POST",
		url : url,
		beforeSend : function() {
			$(".loading").fadeIn("normal")
		},
		cache : false,
		data : 'accion=c',
		success : function(datos) {
			$("#formulario").empty().html(datos).slideDown();
			$('.loading').fadeOut('slow');
			$("#lista").slideUp()
		}
	});
	return false
}
function EditDatos(url) {
	$.ajax({
		type : "GET",
		url : url,
		beforeSend : function() {
			$(".loading").fadeIn("normal")
		},
		cache : false,
		success : function(datos) {
			$("#formulario").empty().html(datos).slideDown();
			$('.loading').fadeOut('slow');
			$("#lista").slideUp()
		}
	});
	return false
}
function ConsultaDatos(url) {
	$.ajax({
		url : url + 'lista.php',
		cache : false,
		type : "GET",
		success : function(datos) {
			$("#lista").html(datos)
		}
	})
}
function EliminarDato(id, url) {
	var msg = confirm("Desea eliminar este Registro")
	if (msg) {
		$.ajax({
			url : url,
			type : "POST",
			data : 'accion=D&id=' + id,
			success : function(datos) {
				$("#fila-" + id).fadeOut('slow', function() {
					$(this).remove()
				})
			}
		})
	}
	return false
}
function Cancelar() {
	$("#formulario").slideUp();
	$("#lista").slideDown();
	return false
}
function guardarDatos_avisos() {
	var id_textarea = $('.conteni').attr('id');
	var contenido = $('#' + id_textarea).attr('value');
	var idaviso = $('#idaviso').attr('value');
	var accion = $('#accion').attr('value');
	var nombre = $('#nombre').attr('value');
	var estado = $("#estado").attr("value");
	var url = $("#url").attr("value");
	var idusuario = $("#idusuario").attr("value");
	switch (accion) {
	case 'u':
		$
				.ajax({
					url : 'includes/modulos/avisos/aviso_acciones.php?accion='
							+ accion,
					type : "POST",
					cache : false,
					data : 'idaviso=' + idaviso + '&nombre=' + nombre
							+ '&contenido=' + escape(contenido) + '&estado='
							+ estado + '&url=' + url + '&idusuario='
							+ idusuario,
					success : function(datos) {
						ConsultaDatos('includes/modulos/avisos/');
						alert('Se Actualizo con exito');
						$("#formulario").hide();
						$("#lista").show()
					}
				});
		break;
	case 'c':
		$
				.ajax({
					url : 'includes/modulos/avisos/aviso_acciones.php?accion='
							+ accion,
					type : "POST",
					cache : false,
					data : 'nombre=' + nombre + '&contenido='
							+ escape(contenido) + '&estado=' + estado + '&url='
							+ url + '&idusuario=' + idusuario,
					success : function(datos) {
						ConsultaDatos('includes/modulos/avisos/');
						$("#formulario").hide();
						$("#lista").show()
					}
				});
		break
	}
	return false
}
function guardarDatos_Noticia() {
	var id_textarea = $('.conteni').attr('id');
	var contenido = $('#' + id_textarea).attr('value');
	var accion = $('#accion').attr('value');
	var idnoticia = $('#idnoticia').attr('value');
	var titulo = $('#titulo').attr('value');
	var idcategoria = $('#idcategoria').attr('value');
	var imagen = $('#imagen').attr('value');
	var fecha = $('#fecha').attr('value');
	var estado = $("#estado").attr("value");
	var url = $("#url").attr("value");
	var idusuario = $("#idusuario").attr("value");
	if (imagen != 'null' && imagen != 'undefined') {
		switch (accion) {
		case 'u':
			$.ajax({
				url : 'includes/modulos/noticias/noticia_acciones.php?accion='
						+ accion,
				type : "POST",
				cache : false,
				data : 'idnoticia=' + idnoticia + '&titulo=' + titulo
						+ '&idcategoria=' + idcategoria + '&contenido='
						+ escape(contenido) + '&imagen=' + imagen + '&fecha='
						+ fecha + '&estado=' + estado + '&url=' + url
						+ '&idusuario=' + idusuario,
				success : function(datos) {
					ConsultaDatos('includes/modulos/noticias/');
					alert('Se Actualizo con exito');
					$("#formulario").hide().empty();
					$("#lista").show()
				}
			});
			break;
		case 'c':
			$.ajax({
				url : 'includes/modulos/noticias/noticia_acciones.php?accion='
						+ accion,
				type : "POST",
				cache : false,
				data : 'titulo=' + titulo + '&idcategoria=' + idcategoria
						+ '&contenido=' + escape(contenido) + '&imagen='
						+ imagen + '&fecha=' + fecha + '&estado=' + estado
						+ '&url=' + url + '&idusuario=' + idusuario,
				success : function(datos) {
					ConsultaDatos('includes/modulos/noticias/');
					$("#formulario").hide().empty();
					$("#lista").show()
				}
			});
			break
		}
		return false
	} else {
		alert('Debe seleccionar una imagen.')
	}
}
function guardarDatos_portada() {
	var accion = $('#accion').attr('value');
	var id_textarea = $('.conteni').attr('id');
	var idportada = $('#idportada').attr('value');
	var titulo = $('#titulo').attr('value');
	var contenido = $('#' + id_textarea).attr('value');
	var imagen = $('#imagen').attr('value');
	var fecha = $('#fecha').attr('value');
	var estado = $("#estado").attr("value");
	var idusuario = $("#idusuario").attr("value");
	if (imagen != 'null' && imagen != 'undefined') {
		switch (accion) {
		case 'u':
			$.ajax({
				url : 'includes/modulos/portada/portada_acciones.php?accion='
						+ accion,
				type : "POST",
				cache : false,
				data : 'idportada=' + idportada + '&titulo=' + titulo
						+ '&contenido=' + escape(contenido) + '&imagen='
						+ imagen + '&fecha=' + fecha + '&estado=' + estado
						+ '&idusuario=' + idusuario,
				success : function(datos) {
					ConsultaDatos('includes/modulos/portada/');
					alert('Se Actualizo con exito');
					$("#formulario").hide().empty();
					$("#lista").show()
				}
			});
			break;
		case 'c':
			$.ajax({
				url : 'includes/modulos/portada/portada_acciones.php?accion='
						+ accion,
				type : "POST",
				cache : false,
				data : 'titulo=' + titulo + '&contenido=' + escape(contenido)
						+ '&imagen=' + imagen + '&fecha=' + fecha + '&estado='
						+ estado + '&idusuario=' + idusuario,
				success : function(datos) {
					ConsultaDatos('includes/modulos/portada/');
					$("#formulario").hide().empty();
					$("#lista").show()
				}
			});
			break
		}
		return false
	} else {
		alert('Debe seleccionar una imagen.')
	}
}
function guardarDatos_imagen(nombre) {
	var galeria = $("#select-galeria").attr("value");
	var idusuario = $("#idusuario").attr("value");
	$.ajax({
		url : 'includes/modulos/imagenes/image_acciones.php',
		type : "POST",
		cache : false,
		data : 'image=' + escape(nombre) + '&idgaleria=' + galeria
				+ '&idusuario=' + idusuario,
		success : function(datos) {
		}
	})
}
function guardarDatos_galeria() {
	var id = $('#id_gal').attr('value');
	var accion = $('#accion_gal').attr('value');
	var nombre = $("#nombre_gal").attr("value");
	var estado = $("#estado_gal").attr("value");
	var idusuario = $("#idusuario_gal").attr("value");
	switch (accion) {
	case 'c':
		$.ajax({
			url : 'includes/modulos/imagenes/galeria_acciones.php?accion='
					+ accion,
			type : "POST",
			cache : false,
			data : 'nombre=' + escape(nombre) + '&estado=' + estado
					+ '&idusuario=' + idusuario,
			success : function(datos) {
				ConsultaDatos('includes/modulos/imagenes/galeria-');
				galeria_combo();
				$("#formulario").hide().empty();
				$("#lista").show()
			}
		});
		break;
	case 'u':
		$.ajax({
			url : 'includes/modulos/imagenes/galeria_acciones.php?accion='
					+ accion,
			type : "POST",
			cache : false,
			data : 'id=' + id + '&nombre=' + escape(nombre) + '&estado='
					+ estado + '&idusuario=' + idusuario,
			success : function(datos) {
				ConsultaDatos('includes/modulos/imagenes/galeria-');
				galeria_combo();
				$("#formulario").hide().empty();
				$("#lista").show()
			}
		});
		break
	}
}
function galeria_combo() {
	$.ajax({
		url : 'includes/modulos/imagenes/galeria-combo.php',
		cache : false,
		type : "GET",
		success : function(datos) {
			$("#combo-gal").html(datos)
		}
	})
}
function guardarDatos_contenido() {
	var accion = $('#accion').attr('value');
	var id_textarea = $('.conteni').attr('id');
	var idmenu = $('#idmenu').attr('value');
	var idsubmenu = $('#idsubmenu').attr('value');
	var menu = $('#menu').attr('value');
	var contenido = $('#' + id_textarea).attr('value');
	var target = $('#target').attr('value');
	var nivel = $('#nivel').attr('value');
	var estado = $("#estado").attr("value");
	var idusuario = $("#idusuario").attr("value");
	if (menu != '') {
		switch (accion) {
		case 'u':
			$.ajax({
				url : 'includes/modulos/menu/menu_acciones.php?accion='
						+ accion,
				type : "POST",
				cache : false,
				data : 'idmenu=' + idmenu + '&idsubmenu=' + idsubmenu
						+ '&menu=' + escape(menu) + '&contenido='
						+ escape(contenido) + '&target=' + target + '&nivel='
						+ nivel + '&estado=' + estado + '&idusuario='
						+ idusuario,
				success : function(datos) {
					ConsultaDatos('includes/modulos/menu/');
					alert('Se Actualizo con exito');
					$("#formulario").hide().empty();
					$("#lista").show()
				}
			});
			break;
		case 'c':
			$.ajax({
				url : 'includes/modulos/menu/menu_acciones.php?accion='
						+ accion,
				type : "POST",
				cache : false,
				data : 'idsubmenu=' + idsubmenu + '&menu=' + escape(menu)
						+ '&contenido=' + escape(contenido) + '&target='
						+ target + '&nivel=' + nivel + '&estado=' + estado
						+ '&idusuario=' + idusuario,
				success : function(datos) {
					ConsultaDatos('includes/modulos/menu/');
					$("#formulario").hide().empty();
					$("#lista").show()
				}
			});
			break
		}
		return false
	} else {
		alert('Debe Ingresar un Menu para guardar.')
	}
}
function ver_menu(id) {
	$('.nivel').css({
		"background-color" : "#fff"
	});
	$('.hide').fadeOut(100);
	$('#fila-' + id).css({
		"background-color" : "#E4E4E4"
	});
	$('.submenu-' + id).fadeIn("slow")
}