<?php

include 'conexion.class.php';
$direc_modulos = 'includes/modulos/';

class intranet {

    function getUrl($texto_url) {
        $find = array(" - ", " ", "ñ", "Ñ", "á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "&", ",", '"', "(", ")", "'", ";", ".");
        $replace = array("-", "-", "nh", "NH", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "y", "-", "", "", "", "", "", "");

        $url = str_replace($find, $replace, trim($texto_url));
        return strtolower($url);
        //  $url_new=substr($url_new,0,-1);
    }

    function setInsertFile($name_table, $id_relation, $path_file, $describe_file, $type_file, $id_user_creator) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("INSERT INTO intr_file (id_relation,name_table,path_file,describe_file,type_file,date_register,id_user_creator) VALUES ('$id_relation','$name_table','$path_file','$describe_file','$type_file',now(),'$id_user_creator');", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListFile($table = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $notice = $con->conect;
            $result = mysql_query("SELECT f.id_relation as relacion,f.path_file as lugar,f.describe_file as descripcion,f.type_file as tipo  FROM intr_file f WHERE f.name_table='" . $table . "'", $notice);
            $numero = ($result == NULL) ? 0 : mysql_num_rows($result);
            $this->numRegistro = $numero;
            if (!$result) {
                return false;
            } else {
                while ($dataRow = mysql_fetch_row($result)) {
                    $data[] = $dataRow;
                }
                return $data;
            }
        }
    }

    function getListVideo($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $front = $con->conect;
            $result = mysql_query("SELECT  iv.id_video,iv.id_gallery,iv.image_video,iv.title_video,iv.url_video,iv.date_video,iv.date_register,iv.date_modified, iv.publication_star, iv.publication_end, iv.state_video,iv.id_user_creator,iv.id_user_modified,ig.title_gallery   FROM intr_video iv left join intr_gallery ig on iv.id_gallery=ig.id_gallery $condicion", $front);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertVideo_(
    $title_video, $image_video, $url_video, $id_gallery, $estate_video, $date_video, $id_user_creator) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $fecha = date("Y/m/d");
            $front = $con->conect;
            $result = mysql_query(
                    "INSERT INTO intr_video(
                        id_gallery,
                        image_video,
                        title_video,
                        url_video,
                        date_video,
                        date_register,
                        state_video,                        
                        id_user_creator
                        ) VALUES (
                        '$id_gallery',
                        '$image_video',
                         '$title_video',
                         '$url_video',
                          '$date_video',
                              '$fecha',
                          '$estate_video',                              
                              '$id_user_creator'
                                  );
			", $front);
            $id = mysql_insert_id();
            if (!$result) {
                return false;
            } else {
                return true;
            }
        }
    }

    function deleteVideo($id_video) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $front = $con->conect;
            $result = mysql_query("delete from intr_video where id_video=$id_video;
			", $front);
            $id = mysql_insert_id();
            if (!$result)
                return false;
            else
                return $id;
        }
    }

    function getListFront($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $front = $con->conect;
            $result = mysql_query("SELECT  id_front,title_front,content_front,image_front,date_front,date_register,date_modified, publication_star, publication_end, state_front,id_user_creator,id_user_modified    FROM intr_front $condicion", $front);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertFront($title_front, $content_front, $date_front, $publication_star, $publication_end, $state_front, $id_user_creator) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $front = $con->conect;
            $result = mysql_query("INSERT INTO intr_front(title_front,content_front,date_front,publication_star,publication_end,state_front,date_register,id_user_creator) VALUES ('$title_front','$content_front','$date_front','$publication_star','$publication_end','$state_front',now(),'$id_user_creator');
			", $front);
            $id = mysql_insert_id();
            if (!$result)
                return false;
            else
                return $id;
        }
    }

    function setInsertImageFront($id_front, $image_front, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $front = $con->conect;
            $result = mysql_query("update  intr_front SET image_front = '$image_front',id_user_modified='$id_user_modified',date_modified=now() WHERE  id_front=$id_front;", $front);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setUpdateFront($id_front, $title_front, $content_front, $date_front, $publication_star, $publication_end, $state_front, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $front = $con->conect;
            $result = mysql_query("update  intr_front SET title_front = '$title_front' ,content_front = '$content_front',date_front = '$date_front', publication_star = '$publication_star', publication_end = '$publication_end',id_user_modified='$id_user_modified',date_modified=now(),state_front = '$state_front' WHERE  id_front=$id_front;", $front);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteFront($id_front) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $front = $con->conect;
            $result = mysql_query("DELETE  FROM intr_front WHERE id_front=$id_front", $front);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListNotice($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $notice = $con->conect;
            $result = mysql_query("SELECT *  FROM intr_notice $condicion", $notice);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertNotice($title_notice, $content_notice, $section_notice, $publication_front, $date_notice, $publication_star, $publication_end, $state_notice, $id_user_creator, $highlight_notice) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $notice = $con->conect;
            $result = mysql_query("INSERT INTO intr_notice(title_notice,content_notice,section_notice,publication_front,date_notice,publication_star,publication_end,state_notice,date_register,id_user_creator,highlight_notice) VALUES ('$title_notice','$content_notice','$section_notice','$publication_front','$date_notice','$publication_star','$publication_end','$state_notice',now(),'$id_user_creator','$highlight_notice');", $notice);
            $id = mysql_insert_id();
            if (!$result)
                return false;
            else
                return $id;
        }
    }

    function setInsertImageNotice($id_notice, $image_notice, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $notice = $con->conect;
            $result = mysql_query("update  intr_notice SET image_notice = '$image_notice',id_user_modified='$id_user_modified',date_modified=now() WHERE  id_notice=$id_notice;
			", $notice);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertVideo($id_notice, $video_notice, $imageVideo_notice, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $notice = $con->conect;
            $result = mysql_query("update  intr_notice SET imageVideo_notice='$imageVideo_notice',video_notice = '$video_notice',id_user_modified='$id_user_modified',date_modified=now() WHERE  id_notice=$id_notice;
			", $notice);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setUpdateNotice($id_notice, $title_notice, $content_notice, $section_notice, $publication_front, $date_notice, $publication_star, $publication_end, $state_notice, $id_user_modified, $highlight_notice) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $notice = $con->conect;
            $result = mysql_query("update  intr_notice SET title_notice = '$title_notice' ,content_notice = '$content_notice',section_notice='$section_notice',publication_front='$publication_front',date_notice = '$date_notice',publication_star = '$publication_star', publication_end = '$publication_end',id_user_modified='$id_user_modified',date_modified=now(),state_notice = '$state_notice',highlight_notice='$highlight_notice' WHERE  id_notice=$id_notice;", $notice);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteNotice($id_notice) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $notice = $con->conect;
            $result = mysql_query("DELETE  FROM intr_notice WHERE id_notice=$id_notice", $notice);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListDiary($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT *  FROM intr_diary $condicion", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertDiary($title_diary, $content_diary, $date_diary, $publication_star, $publication_end, $state_diary, $id_user_creator) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("INSERT INTO intr_diary (title_diary,content_diary,date_diary,publication_star,publication_end,state_diary,date_register,id_user_creator) VALUES ('$title_diary','$content_diary','$date_diary','$publication_star','$publication_end','$state_diary',now(),'$id_user_creator');", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setUpdateDiary($id_diary, $title_diary, $content_diary, $date_diary, $publication_star, $publication_end, $state_diary, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  intr_diary SET title_diary = '$title_diary' , content_diary = '$content_diary', date_diary = '$date_diary',publication_star = '$publication_star', publication_end = '$publication_end',id_user_modified='$id_user_modified',date_modified=now(),state_diary = '$state_diary' WHERE  id_diary=$id_diary;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteDiary($id_diary) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM intr_diary WHERE id_diary=$id_diary", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListTournament($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT *  FROM intr_tournament $condicion", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numRegistro = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListTournamentList($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT it.*,c.categoria,case when c.sexo='1' then concat('Damas ',c.categoria) else concat('Varones ',c.categoria) end as nomcategoria  FROM intr_tournament it inner join categorias c on it.idcategoria=c.idCategoria $condicion", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numRegistro = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertTournament($title_tournament, $content_tournament, $genero_tournament, $publication_star, $publication_end, $state_tournament, $id_user_creator, $tipo, $categoria, $lugar, $init_tournament, $end_tournament) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("INSERT INTO intr_tournament(title_tournament,content_tournament,genero_tournament,publication_star,publication_end,state_tournament,date_register,id_user_creator,tipo,idcategoria,lugar,init_tournament,end_tournament) VALUES ('$title_tournament','$content_tournament',$genero_tournament,'$publication_star','$publication_end','$state_tournament',now(),'$id_user_creator','$tipo',$categoria,'$lugar','$init_tournament','$end_tournament');", $link);
            if (!$result)
                return false;
            else
                return mysql_insert_id();
        }
    }

    function setUpdateTournament($id_tournament, $title_tournament, $content_tournament, $genero_tournament, $publication_star, $publication_end, $state_tournament, $id_user_modified, $tipo, $categoria, $lugar, $init_tournament, $end_tournament) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  intr_tournament SET title_tournament = '$title_tournament' , content_tournament = '$content_tournament', genero_tournament = $genero_tournament,publication_star = '$publication_star', publication_end = '$publication_end',id_user_modified='$id_user_modified',date_modified=now(),state_tournament = '$state_tournament',tipo='$tipo',idcategoria=$categoria,lugar='$lugar',init_tournament='$init_tournament',end_tournament ='$end_tournament' WHERE  id_tournament=$id_tournament;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteTournament($id_tournament) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM intr_tournament WHERE id_tournament=$id_tournament", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListSports_Information($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $sports_information = $con->conect;
            $result = mysql_query("SELECT d.id_sports_information as id,d.title_sports_information as titulo,d.type_sports_information as tipo,d.url_sports_information as url,d.file_sports_information as archivo,d.date_sports_information as fecha,d.publication_star as publ_start,d.publication_end as publ_end,d.state_sports_information as estado,d.color as color  FROM  intr_sports_information d $condicion", $sports_information);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertSports_Information(
    $title_sports_information, $type_sports_information, $url_sports_information, $date_sports_information, $publication_star, $publication_end, $state_sports_information, $id_user_creator, $file_sports_information
    ) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $fecha = date("Y/m/d");
            $sports_information = $con->conect;
            $publiS_C = date_create($publication_star);
            $publiS_F = date_format($publiS_C, "Y/m/d");

            $publisE_C = date_create($publication_end);
            $publisE_F = date_format($publisE_C, "Y/m/d");

            $sql = "INSERT INTO intr_sports_information(
                title_sports_information,
                type_sports_information,
                url_sports_information,
                date_sports_information,
                publication_star,
                publication_end,
                state_sports_information,
                date_register,
                id_user_creator,
                file_sports_information
                ) VALUES (
                '$title_sports_information',
                    '$type_sports_information',
                        '$url_sports_information',
                            '$date_sports_information',
                                '$publiS_F',
                                    '$publisE_F',
                                        '$state_sports_information',
                                            '$fecha',
                                            '$id_user_creator',
                                                '$file_sports_information')";
            $result = mysql_query($sql, $sports_information);
            $id = mysql_insert_id();
            if (!$result) {
                return false;
            } else {
                return true;
            }
        }
    }

    function setInsertImageSports_Information($id_sports_information, $image_sports_information, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $sports_information = $con->conect;
            $result = mysql_query("update  intr_sports_information SET image_sports_information = '$image_sports_information',id_user_modified='$id_user_modified',date_modified=now() WHERE  id_sports_information=$id_sports_information;
	", $sports_information);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setUpdateSports_Information($id_sports_information, $title_sports_information, $type_sports_information, $url_sports_information, $date_sports_information, $publication_star, $publication_end, $state_sports_information, $id_user_modified, $file_sports_information, $color) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $sports_information = $con->conect;
            $result = mysql_query("update  intr_sports_information SET title_sports_information = '$title_sports_information' ,type_sports_information = '$type_sports_information',url_sports_information='$url_sports_information',date_sports_information = '$date_sports_information',publication_star = '$publication_star', publication_end = '$publication_end',id_user_modified='$id_user_modified',date_modified=now(),state_sports_information = '$state_sports_information' ,file_sports_information='$file_sports_information',color='$color' WHERE  id_sports_information=$id_sports_information;", $sports_information);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteSports_Information($id_sports_information) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $sports_information = $con->conect;
            $result = mysql_query("DELETE  FROM intr_sports_information WHERE id_sports_information=$id_sports_information", $sports_information);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListSponsor($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT s.id_sponsor AS id,s.title_sponsor as titulo,s.image_sponsor as imagen,s.url_sponsor as url,s.date_sponsor as fecha,s.publication_star as publ_start,s.publication_end as publ_end,s.state_sponsor as estado  FROM intr_sponsor s $condicion", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertSponsor($title_sponsor, $type_sponsor, $url_sponsor, $date_sponsor, $publication_star, $publication_end, $state_sponsor, $id_user_creator) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $query = "select id_sponsor,title_sponsor from intr_sponsor where title_sponsor='$title_sponsor'";
            $result_validar = mysql_query($query, $link);
            if (mysql_num_rows($result_validar) > 0) {
                return false;
            } else {
                $result = mysql_query("INSERT INTO intr_sponsor(title_sponsor,type_sponsor,url_sponsor,date_sponsor,publication_star,publication_end,state_sponsor,date_register,id_user_creator) VALUES ('$title_sponsor','$type_sponsor','$url_sponsor','$date_sponsor','$publication_star','$publication_end','$state_sponsor',now(),'$id_user_creator');", $link);
                $id = mysql_insert_id();
                if (!$result) {
                    return false;
                } else {
                    return $id;
                }
            }
        }
    }

    function setInsertImageSponsor($id_sponsor, $image_sponsor, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  intr_sponsor SET id_user_modified='$id_user_modified',date_modified=now(),image_sponsor='$image_sponsor' WHERE  id_sponsor=$id_sponsor;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setUpdateSponsor($id_sponsor, $title_sponsor, $type_sponsor, $url_sponsor, $date_sponsor, $publication_star, $publication_end, $state_sponsor, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;

            $query = "select id_sponsor,title_sponsor from
                intr_sponsor where title_sponsor='$title_sponsor' and id_sponsor!=$id_sponsor";
            $result_validar = mysql_query($query, $link);
            if (mysql_num_rows($result_validar) > 0) {
                return false;
            } else {
                $result = mysql_query("update  intr_sponsor SET title_sponsor = '$title_sponsor' ,type_sponsor = '$type_sponsor',url_sponsor='$url_sponsor',date_sponsor = '$date_sponsor',publication_star = '$publication_star', publication_end = '$publication_end',id_user_modified='$id_user_modified',date_modified=now(),state_sponsor = '$state_sponsor' WHERE  id_sponsor=$id_sponsor;", $link);
                if (!$result) {
                    return false;
                } else {
                    return $result;
                }
            }
        }
    }

    function deleteSponsor($id_sponsor) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM intr_sponsor WHERE id_sponsor=$id_sponsor", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListCalendar($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT a.id_calendar as id,a.title_calendar as titulo,a.pais_calendar as Cpais,p.title_country as pais,a.start_calendar as fecha,a.end_calendar as fechafin,a.publication_star as publ_start,a.publication_end as publ_end,a.state_calendar as estado  FROM intr_calendar a,intr_country p WHERE a.pais_calendar=p.id_country $condicion", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertCalendar($title_calendar, $pais_calendar, $start_calendar, $end_calendar, $publication_star, $publication_end, $state_calendar, $id_user_creator) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("INSERT INTO intr_calendar(title_calendar,pais_calendar,start_calendar,end_calendar,publication_star,publication_end,state_calendar,date_register,id_user_creator) VALUES ('$title_calendar','$pais_calendar','$start_calendar','$end_calendar''$publication_star','$publication_end','$state_calendar',now(),'$id_user_creator');", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setUpdateCalendar($id_calendar, $title_calendar, $pais_calendar, $start_calendar, $end_calendar, $publication_star, $publication_end, $state_calendar, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  intr_calendar SET title_calendar = '$title_calendar' , pais_calendar = '$pais_calendar', start_calendar = '$start_calendar',publication_star = '$publication_star', publication_end = '$publication_end',id_user_modified='$id_user_modified',date_modified=now(),state_calendar = '$state_calendar',end_calendar='$end_calendar' WHERE  id_calendar=$id_calendar;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteCalendar($id_calendar) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM intr_calendar WHERE id_calendar=$id_calendar", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListCountry($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT *  FROM intr_country $condicion", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertCountry($title_country, $id_user_creator) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $sql = "select title_country from intr_country where title_country='$title_country'";
            $resut_valida = mysql_query($sql, $link);
            if (mysql_num_rows($resut_valida) > 0) {
                return false;
            } else {
                $result = mysql_query("INSERT INTO intr_country(title_country,date_register,id_user_creator) VALUES ('$title_country',now(),'$id_user_creator');", $link);
                if (!$result) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    function setUpdateCountry($id_country, $title_country, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  intr_country SET title_country = '$title_country',id_user_modified='$id_user_modified',date_modified=now()  WHERE  id_country=$id_country;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteCountry($id_country) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM intr_country WHERE id_country=$id_country", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListImage($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $image = $con->conect;
            $result = mysql_query("SELECT *  FROM intr_image $condicion", $image);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertImage(
    $id_gallery, $image, $title_image, $date_image, $publication_star, $publication_end, $state_image, $id_user_creator) {
        $con = new conexion;
        $fecha = date("Y/m/d");
        if ($con->conectar() == true) {
            $image_ = $con->conect;
            $result = mysql_query("INSERT INTO intr_image
                (id_gallery,
                image,
                title_image,
                date_image,
                publication_star,
                publication_end,
                state_image,
                date_register,
                id_user_creator
                ) VALUES
                ('$id_gallery',
                    '$image',
                        'Sin Nombre',
                        '$fecha',
                        '$fecha',
                        '$fecha',
                        '$state_image',
                            '$fecha',
                            '$id_user_creator'
                                );", $image_);
            $id = mysql_insert_id();
            if (!$result) {
                return false;
            } else {
                return true;
            }
        }
    }

    function setInsertImageImage($id_image, $image_image, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $image = $con->conect;
            $result = mysql_query("update  intr_image SET image_image = '$image_image',id_user_modified='$id_user_modified',date_modified=now() WHERE  id_image=$id_image;", $image);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setUpdateImage($id_image, $title_image, $content_image, $section_image, $publication_image, $date_image, $publication_star, $publication_end, $state_image, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $image = $con->conect;
            $result = mysql_query("update  intr_image SET title_image = '$title_image' ,content_image = '$content_image',section_image='$section_image',publication_image='$publication_image',date_image = '$date_image',publication_star = '$publication_star', publication_end = '$publication_end',id_user_modified='$id_user_modified',date_modified=now(),state_image = '$state_image' WHERE  id_image=$id_image;", $image);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteImage($id_image) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $image = $con->conect;
            $result = mysql_query("DELETE  FROM intr_image WHERE id_image='$id_image'", $image);
            if (!$result)
                return false;
            else
                return true;
        }
    }

    function getListGallery($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT f.id_gallery AS id,f.title_gallery AS titulo,f.image_gallery AS image,f.state_gallery AS estado,f.describe_gallery AS descripcion,f.type_gallery AS tipo FROM intr_gallery f $condicion", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertGallery($title_gallery, $type_gallery, $describe_gallery, $state_gallery, $id_user_creator) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $fecha = date("Y/m/d");
            $result = mysql_query("
                INSERT INTO intr_gallery(
                title_gallery,
                type_gallery,
                describe_gallery,
                state_gallery,
                date_register,
                id_user_creator) VALUES ('$title_gallery','$type_gallery','$describe_gallery','$state_gallery','$fecha','$id_user_creator');", $link);
            if (!$result) {
                return false;
            } else {
                return true;
            }
        }
    }

    function setUpdateGallery($id_gallery, $title_gallery, $type_gallery, $describe_gallery, $state_gallery, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("UPDATE  intr_gallery SET title_gallery = '$title_gallery',type_gallery='$type_gallery',describe_gallery='$describe_gallery',state_gallery='$state_gallery',id_user_modified='$id_user_modified',date_modified=now() WHERE  id_gallery=$id_gallery;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setUpdateGalleryPhotoPrincipal($id_gallery, $image_gallery) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("UPDATE  intr_gallery SET image_gallery = '$image_gallery' WHERE  id_gallery=$id_gallery;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteGallery($id_gallery) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM intr_gallery WHERE id_gallery=$id_gallery", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListLink($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT *  FROM intr_link $condicion", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertLink($title_link, $type_link, $url_link, $date_link, $publication_star, $publication_end, $state_link, $id_user_creator) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("INSERT INTO intr_link(title_link,type_link,url_link,date_link,publication_star,publication_end,state_link,date_register,id_user_creator) VALUES ('$title_link','$type_link','$url_link','$date_link','$publication_star','$publication_end','$state_link',now(),'$id_user_creator');", $link);
            $id = mysql_insert_id();
            if (!$result)
                return false;
            else
                return $id;
        }
    }

    function setInsertImageLink($id_link, $image_link, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  intr_link SET image_link = '$image_link',id_user_modified='$id_user_modified',date_modified=now() WHERE  id_link=$id_link;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setUpdateLink($id_link, $title_link, $type_link, $url_link, $date_link, $publication_star, $publication_end, $state_link, $id_user_modified) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  intr_link SET title_link = '$title_link' ,type_link = '$type_link',url_link='$url_link',date_link = '$date_link',publication_star = '$publication_star', publication_end = '$publication_end',id_user_modified='$id_user_modified',date_modified=now(),state_link = '$state_link' WHERE  id_link=$id_link;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteLink($id_link) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM intr_link WHERE id_link=$id_link", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListCategorie($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT *  FROM categorias $condicion order by sexo,categoria", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function getListCity($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT *  FROM ubigeo $condicion group by ciudad;", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function getListSportsman($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT *  FROM jugadores $condicion order by Apellidos,FechaNac", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function setInsertSportman($apellidos, $nombres, $completo, $fechanac, $lugar, $sexo, $flag) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $query = "select * from jugadores where Completo='$completo'";
            $result_valida = mysql_query($query, $link);
            if (mysql_num_rows($result_valida) > 0) {
                return False;
            } else {
                $result = mysql_query("INSERT INTO jugadores(apellidos,nombres,completo,fechanac,lugar,sexo,flag) VALUES ('$apellidos','$nombres','$completo','$fechanac','$lugar',$sexo,$flag);", $link);
                $id = mysql_insert_id();
                if (!$result) {
                    return false;
                } else {
                    return $id;
                }
            }
        }
    }

    function setUpdateSportman($idjugador, $apellidos, $nombres, $completo, $fechanac, $lugar, $sexo, $flag) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  jugadores SET apellidos = '$apellidos' ,nombres = '$nombres',completo='$completo',fechanac = '$fechanac',lugar = '$lugar', sexo = $sexo,flag=$flag WHERE  idjugador=$idjugador;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteSportsman($idjugador) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM jugadores WHERE idjugador=$idjugador", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertCategorie($categoria, $sexo, $flag) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("INSERT INTO categorias(categoria,sexo,flag) VALUES ('$categoria',$sexo,$flag);", $link);
            $id = mysql_insert_id();
            if (!$result)
                return false;
            else
                return $id;
        }
    }

    function setUpdateCategorie($idcategoria, $categoria, $sexo, $flag) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  categorias SET categoria = '$categoria',sexo = $sexo,flag=$flag WHERE  idcategoria=$idcategoria;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deleteCategorie($idcategoria) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM categorias WHERE idcategoria=$idcategoria", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListSportsmanInTournament($idtorneo = '', $condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT j.*,case when j.idjugador in (select p.idjugador  FROM participantes p where p.idtorneo=$idtorneo) then 1 else 0 end as participa  FROM jugadores j $condicion order by participa,j.nombres", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function getListSportmanInTournament($idtorneo = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("select p.idparticipantes,j.apellidos,j.nombres,p.psingles,p.pdobles  FROM participantes p inner join jugadores j on p.idjugador=j.idjugador where p.idtorneo=$idtorneo order by j.nombres", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function setUpdatePuntajeAParticipante($idParticipante, $Singles, $Dobles) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update  participantes set psingles=$Singles, pdobles=$Dobles where idparticipantes=$idParticipante;", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function setInsertParticipantSQL($sql) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query($sql, $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function lista_ranking($tipo, $data = "nada", $torneo = "nada") {
        if ($tipo == 'MENORES') {
            if ($data == "nada") {
                $where1 = "";
                $where2 = "";
            } else {
                $where1 = " and c.idcategoria=" . $data . " and c.categoria=(" . date('Y') . "-substr(j.FechaNac,1,4)) and c.sexo=j.Sexo ";
                $where2 = ", categorias c";
            }
            $where3 = " and t.tipo='" . $tipo . "' and t.id_tournament=p1.idTorneo and t.id_tournament=p2.idTorneo and t.id_tournament=p3.idTorneo";
            $where4 = ", intr_tournament t";
            $sql = 'select j.nombres,j.apellidos,j.Completo,j.Lugar,j.FechaNac, p1.suma, p2.Single, p3.Doble, j.idJugador  FROM jugadores j,( select sum(psingles)+sum(pdobles*0.25) suma , idparticipantes, idJugador , idTorneo FROM participantes group by idJugador order by suma DESC limit 0,6) p1,( select sum(psingles) single, idparticipantes, idJugador , idTorneo FROM participantes group by idJugador  order by single DESClimit 0,6) p2,( select sum(pdobles) doble, idparticipantes, idJugador , idTorneo FROM participantes group by idJugador order by doble DESC limit 0,6) p3 ' . $where2 . ' ' . $where4 . ' where j.idJugador=p3.idJugador and p1.idparticipantes=p2.idparticipantes and p2.idparticipantes=p3.idparticipantes ' . $where1 . ' ' . $where3 . ' order by (p1.suma) DESC';
        } elseif ($tipo == 'GENERAL') {
            $sql = $this->ranking_general($data);
        } elseif ($tipo == 'SENIORS') {
            $sql = $this->ranking_senior($data);
        }
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query($sql, $link);
            $numero = ($result == NULL) ? 0 : mysql_num_rows($result);
            $this->numRanking = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function ranking_general($genero) {
        $query = "select j.nombres,j.apellidos,j.Completo,j.Lugar,j.FechaNac, p1.suma, p2.Single, p3.Doble, j.idJugador  FROM jugadores j, intr_tournament t , participantes p,( select sum(psingles)+sum(pdobles*0.25) suma , idparticipantes, idJugador , idTorneo  FROM participantes group by idJugador order by suma DESC limit 0,6) p1, ( select sum(psingles) single, idparticipantes, idJugador , idTorneo  FROM participantes group by idJugador order by single DESC limit 0,6) p2, ( select sum(pdobles) doble, idparticipantes, idJugador , idTorneo  FROM participantes group by idJugador order by doble DESC limit 0,6) p3 where t.tipo='GENERAL' and p.idJugador=j.idJugador and p.idTorneo=t.id_tournament and j.idJugador=p3.idJugador and p1.idparticipantes=p2.idparticipantes and p2.idparticipantes=p3.idparticipantesorder and j.sexo=$genero by (p1.suma) DESC";
        return $query;
    }

    function ranking_senior($genero) {
        $query = "select j.nombres,j.apellidos,j.Completo,j.Lugar,j.FechaNac, p1.suma, p2.Single, p3.Doble, j.idJugador  FROM jugadores j, intr_tournament t , participantes p,( select sum(psingles)+sum(pdobles*0.25) suma , idparticipantes, idJugador , idTorneo  FROM participantes group by idJugador order by suma DESC limit 0,6) p1, ( select sum(psingles) single, idparticipantes, idJugador , idTorneo  FROM participantes group by idJugador order by single DESC limit 0,6) p2, ( select sum(pdobles) doble, idparticipantes, idJugador , idTorneo  FROM participantes group by idJugador order by doble DESC limit 0,6) p3 where t.tipo='SENIORS' and p.idJugador=j.idJugador and p.idTorneo=t.id_tournament and j.idJugador=p3.idJugador and p1.idparticipantes=p2.idparticipantes and p2.idparticipantes=p3.idparticipantesorder and j.sexo=$genero by (p1.suma) DESC";
        return $query;
    }

    function detallepuntos($data) {
        $sql = '(select t.title_tournament, t.init_tournament, t.Lugar, t.Tipo, j.Completo, p.psingles ,0 dobles FROM participantes p, jugadores j, intr_tournament t where j.idJugador=p.idJugador and t.id_tournament=p.idTorneo and j.idjugador=' . $data . 'order by psingles DESC limit 0,6)union all(select t.title_tournament, t.init_tournament, t.Lugar, t.Tipo, j.Completo, 0, p.pdobles dobles FROM participantes p, jugadores j, intr_tournament t where j.idJugador=p.idJugador and t.id_tournament=p.idTorneo and j.idjugador=' . $data . 'order by pdobles DESC limit 0,6)';
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query($sql, $link);
            $numero = ($result == NULL) ? 0 : mysql_num_rows($result);
            $this->numRanking = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function getListGeneralPage($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT ip.id_page,ip.idpater,ip.title_page,ip.orden,ip.state_page,case when ip.idpater=0 then '<b>(Sin Superior)</b>' else (select p.title_page  FROM intr_page p where p.id_page=ip.idpater) end as superior,ip.state_page,ip.publisher_in_menu  FROM intr_page ip  $condicion ", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numPage = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function getListPage($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("SELECT *  FROM intr_page  $condicion ", $link);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numPage = $numero;
            if (!$result) {
                return false;
            } else {
                while ($row = mysql_fetch_row($result)) {
                    $results[] = $row;
                }
                return $results;
            }
        }
    }

    function setInsertPage(
            $title_page, $content_page, $link,
            $idpater, $orden, $id_user_creator, 
            $publisher_in_menu, $state_page) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link_ = $con->conect;
            $fecha=date("Y/m/d");
            $result = mysql_query("
                INSERT INTO intr_page(
                idpater,title_page,content_page,link,
                orden,date_register,
                id_user_creator,publisher_in_menu,state_page
                ) VALUES (
                 $idpater,'$title_page','$content_page','$link',
                   $orden,'$fecha',
                        '$id_user_creator',$publisher_in_menu,$state_page
                            );", $link_);
            $id = mysql_insert_id();
            if (!$result)
                return false;
            else
                return true;
        }
    }

    function setUpdatePage($id_page, $title_page, $content_page, $link, $idpater, $orden, $id_user_creator, $publisher_in_menu, $state_page) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link_ = $con->conect;
            $result = mysql_query("
                update 
                intr_page SET
                title_page = '$title_page' ,
                    content_page = '$content_page',
                        id_user_modified='$id_user_creator',
                            date_modified=now(),
                            idpater=$idpater,
                                orden=$orden,
                                    publisher_in_menu=$publisher_in_menu,
                                        state_page=$state_page 
                                            WHERE  id_page=$id_page ;", $link_);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function deletePage($id_page) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("DELETE  FROM intr_page WHERE id_page=$id_page", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListEvent($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $notice = $con->conect;
            $result = mysql_query("SELECT *  FROM ins_event $condicion", $notice);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getListEvent_Information($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $event_information = $con->conect;
            $result = mysql_query("SELECT d.id_event as id,d.name_event as nombre_evento,d.date_start as fecha_inicio,d.date_end as fecha_fin,d.active as activo,d.deleted as eliminado,d.file_base as archivo_base, d.amount , d.description,d.place,d.currency,d.phone,d.email,d.type_pay,d.name_pay,d.type_acc FROM  ins_event d $condicion", $event_information);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function setInsertEvent(
    $name_event, $url, $date_start, $date_end, $active, $file, $user, $amount, $description, $place, $currency, $phone, $email, $type_pay, $name_pay, $type_acc) {
        $url = $this->getUrl($url);
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $fecha = date("Y/m/d");
            $fecha_C_E = date_create($date_end);
            $fecha_F_E = date_format($fecha_C_E, "Y/m/d");
            $fecha_C_S = date_create($date_start);
            $fecha_F_S = date_format($fecha_C_S, "Y/m/d");

            $query = "select id_event, name_event,date_start from ins_event where 
                name_event='$name_event' and date_start='$fecha_F_S'";
            $result_buscar = mysql_query($query, $link);
            if (mysql_num_rows($result_buscar) > 0) {
                return false;
            } else {
                $result = mysql_query("INSERT INTO ins_event(
                name_event, url,  date_start,
                date_end,  active,  file_base,
                deleted, dateregister, userregistration,
                amount,  description, place,
                currency,phone,email,
                type_pay,name_pay,type_acc
                )
                VALUES 
                ('$name_event','$url','$fecha_F_S',
                    '$fecha_F_E','$active','$file',
                        '0','$fecha','$user',
                    '$amount','$description','$place',
                        '$currency','$phone','$email',
                            '$type_pay','$name_pay','$type_acc'
                                )", $link);
                if (!$result) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    function setUpdateEvent($id_event, $name_event, $url, $date_start, $date_end, $active, $user, $amount, $description, $place, $currency, $phone, $email, $type_pay, $name_pay, $type_acc) {
        // var_dump($type_pay, $name_pay);
        $url = $this->getUrl($url);
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            // $fecha = date("Y/m/d");
            //fecha de finalizacion
            $query = "select id_event, name_event,dateregister from ins_event 
                where name_event='$name_event' and date_start='$date_start' and id_event!='$id_event'";
            $result_buscar = mysql_query($query, $link);
            if (mysql_num_rows($result_buscar) > 0) {
                return false;
            } else {
                $parse_date_end = date_parse($date_end . " 23:59:59");
                $date_end = $parse_date_end[year] . "/" . $parse_date_end[month] . "/" . $parse_date_end[day] . " " . $parse_date_end[hour] . ":" . $parse_date_end[minute] . ":" . $parse_date_end[second];
                $result = mysql_query("update ins_event  
                SET
                name_event= '$name_event',
                    url='$url',
                        date_start='$date_start',
                            date_end='$date_end',
                                active='$active',
                                    datemodified=now(),  usermodification='$user',
                                        amount=$amount, description='$description',
                                          place='$place',currency='$currency',phone='$phone',
                                                        email='$email',type_pay='$type_pay',
                                                            name_pay='$name_pay',type_acc='$type_acc'  WHERE  id_event=$id_event;", $link);
                if (!$result) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    function deleteEvent($id_event, $user) {
        $con = new conexion;
        if ($con->conectar() == true) {
            $link = $con->conect;
            $result = mysql_query("update ins_event  SET deleted='1',datemodified=now(),usermodification='$user'  WHERE  id_event=$id_event;", $link);
            if (!$result)
                return false;
            else
                return $result;
        }
    }

    function getAthlete($condicion = '') {
        $con = new conexion;
        if ($con->conectar() == true) {
            $event_information = $con->conect;
            $result = mysql_query("SELECT 	d.id_person as id,
											d.id_event as id_evento,
											d.affiliate_code as codigo_afiliacion,
											d.name_person as nombres,
											d.surname_paternal as apellido_paterno,
											d.surname_maternal as apellido_materno,
											d.gender as genero,
											d.date_birth as fecha_nacimiento,
											d.document_number as numero_documento,
											d.email as email,
											d.number_operation as numero_operacion,
											d.voucher as voucher,
											d.currency as moneda,
											d.amount as cantidad,
											d.photo as foto,
											d.landline as telefono_fijo,
											d.mobile as celular,
											d.name_father as nombre_padre,
											d.email_father as email_padre,
											d.phone_father as telefono_padre,
											d.active as activo,
											d.deleted as eliminado FROM  ins_person d $condicion", $event_information);
            $numero = $result == NULL ? 0 : mysql_num_rows($result);
            $this->numAuspiciador = $numero;
            if (!$result)
                return false;
            else
                return $result;
        }
    }

}

?>