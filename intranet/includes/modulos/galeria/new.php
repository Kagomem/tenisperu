<?php
ob_start();
session_start();
$accion = $_REQUEST['acc'];
($accion == '') ? $accion = 'I' : $accion = 'U';
$stadoForm = $_REQUEST['st'];
if ($accion == 'U') {
    $id_gallery = $_REQUEST['cd'];
    include_once('includes/commons/intranet.class.php');
    $objIntranet = new intranet;
    $dataDiary = $objIntranet->getListGallery('WHERE f.id_gallery=' . $id_gallery);
    while ($data = mysql_fetch_array($dataDiary)) {
        $id_gallery = $data['0'];
        $title_gallery = $data['1'];
        $image_gallery = $data['2'];
        $state_gallery = $data['3'];
        $describe_gallery = $data['4'];
        $type_gallery = $data['5'];
    }
}
?>
<div id="record" class="formNew">
    <?php if ($stadoForm != NULL && $stadoForm == 1) { ?>
        <script>
            $(document).ready(function() {
                $('#message').fadeIn('normal', function() {
                    $(this).delay(1500).fadeOut('normal');
                });
            });
        </script>
        <div class="form-state" id="message" style="display:none">
            <div class="message exito">Registro Guardado</div>
        </div>
    <?php } ?>
    <?php if ($stadoForm != NULL && $stadoForm == 2) { ?>
        <script>
            $(document).ready(function() {
                $('#message').fadeIn('normal', function() {
                    $(this).delay(1500).fadeOut('normal');
                });
            });
        </script>
        <div class="form-state" id="message" style="display:none">
            <div class="message exito">Error!!!. No se pudo guardar el nuevo registro.</div>
        </div>
    <?php } ?>
    <form action="includes/modulos/galeria/galeria_acciones.php"
          method="post" enctype="multipart/form-data" name="formDiary" id="formDiary">
        <div class="top-item-form">
            <div class="item">
                <div class="label-input">Publicar:</div>
                <div class="input">
                    <div class="intr-checkbox">
                        <input name="estado" type="radio" id="activo" value="1" <?php echo ($state_gallery == 1) ? 'checked' : ''; ?>>
                        <label for="activo">S&iacute;</label>
                    </div>
                    <div class="intr-checkbox">
                        <input name="estado" type="radio" id="inactivo" value="0"  <?php echo ($state_gallery == '' || $state_gallery == 0) ? 'checked' : ''; ?>>
                        <label for="inactivo">No</label>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="label-input">Tipo Galeria: </div>
                <div class="input" id="vA">
                    <?php
                    if ($accion == 'U') {
                        echo '<strong style="color:#333">-  ' . strtoupper($type_gallery) . '  -</strong>';
                        echo '<input name="tipo" type="hidden" value="' . $type_gallery . '" />';
                    } else {
                        ?>
                        <select name="tipo">
                            <option value="imagen"  <?php echo ($type_gallery == 'imagen') ? 'selected="selected"' : ''; ?>>Imagen</option>
                            <option value="video"  <?php echo ($type_gallery == 'video') ? 'selected="selected"' : ''; ?>>Video</option>
                        </select>
                    <?php } ?>
                </div>
            </div>
        </div>
        <input type="hidden" id="accion" name="accion" value="<?php echo $accion ?>" />
        <input type="hidden" id="idusuario" name="idusuario" value="<?php echo $_SESSION['idusuario']; ?>" />
        <input type="hidden" id="id_gallery" name="id_gallery" value="<?php echo $id_gallery; ?>" />
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="optionForm">
            <tr>
                <td width="14%">&nbsp;</td>
                <td width="50%"><div class="intr-label">Nombre de Galer&iacute;a:</div></td>
                <td width="18%">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><div align="left">
                        <input type="text" name="titulo" id="titulo" class="text-input corner-input-intr small-input" value="<?php echo $title_gallery; ?>" />
                    </div></td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td><div class="intr-label">Descripci&oacute;n:</div></td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td><div align="left">
                        <textarea name="descripcion" class="text-input corner-input-intr small-input"
                                  id="descripcion" style="max-height:90px;max-width:560px"><?php echo ($describe_gallery == '') ? '' : $describe_gallery; ?></textarea>
                        <div style="color:#999;text-align:center"><?php echo ($describe_gallery == '') ? 'Sin Descripci&oacute;n' : ''; ?></div>
                    </div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle">
                    <div class="buttons" style="text-align:center">
                        <input name="Restablecer" type="reset" class="bootom cancel" value=" Cancelar " onclick="window.history.back();">
                        <input class="bootom save" type="submit" value=" Guardar ">
                    </div></td>
                <td align="center" valign="middle">&nbsp;</td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
        $(document).ready(function()
        {
            $(".save").click(function(e) {
                e.preventDefault();
                var titulo = $("#titulo").val();
                var descripcion = $("#descripcion").val();
                //formDiary
                if (titulo.trim().length < 1)
                {
                    alert("Porvor complete los campos");
                }
                else if (titulo.trim().length < 3)
                {
                    alert("Escriba un titulo con un minimo de 3 caracteres");
                }
                else if (titulo.trim().length > 150)
                {
                    alert("Escriba un titulo con un maximo de 150 caracteres");
                }
                else if (descripcion.trim().length < 1)
                {
                    alert("Por escriba una descripcion");
                }
                else if (descripcion.trim().length < 5)
                {
                    alert("El campo contenido debe tener un minimo de 5 caracteres");
                }
                else if (descripcion.trim().length > 300)
                {
                    alert("El campo contenido debe tener como maximo 300 caracteres");
                }
                else if (isNaN(titulo.trim()) == false)
                {
                    alert("EL titulo no puede ser un valor numerico");
                }
                else if (isNaN(descripcion.trim()) == false)
                {
                    alert("El campo descripcion no puede ser un valor numerico");
                }
                else
                {
                    document.formDiary.submit();
                }

            });


            $("#fecha").glDatePicker({
                onChange: function(target, newDate)
                {
                    target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                }
            });
            $("#publicacion_ini").glDatePicker(
                    {
                        onChange: function(target, newDate)
                        {
                            target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                        },
                        endDate: 10,
                        startDate: new Date(),
                        allowOld: false
                    });
            $("#publicacion_fin").glDatePicker(
                    {
                        onChange: function(target, newDate)
                        {
                            target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                        },
                        endDate: 20,
                        showPrevNext: true,
                        allowOld: false,
                        startDate: new Date()
                    });
        });
</script> 