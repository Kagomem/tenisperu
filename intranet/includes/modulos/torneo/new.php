<?php
ob_start();
session_start();
$accion = $_REQUEST['acc'];
$stadoForm = $_REQUEST['st'];
($accion == '') ? $accion = 'I' : $accion = 'U';
$id_achievement = $_REQUEST['cd'];
include_once('includes/commons/intranet.class.php');
$objIntranet = new intranet;
$dataTorneo = $objIntranet->getListTournament('where id_tournament=' . $id_achievement);
$categoria = $objIntranet->getListCategorie('where flag=1');
$ciudad = $objIntranet->getListCity();
if ($accion == 'U') {
    $dataTorneo = $objIntranet->getListTournament('where id_tournament=' . $id_achievement);
    while ($data = mysql_fetch_array($dataTorneo)) {
        $title_achievement = $data['1'];
        $content_achievement = $data['2'];
        $genero = $data['3'];
        $idcategoria = $data['4'];
        $lugar = $data['5'];
        $tipo = $data['6'];
        $init_t = $data['7'];
        $end_t = $data['8'];
        $publication_star = $data['11'];
        $publication_end = $data['12'];
        $state_tournament = $data['13'];
    }
}
?>
<script type="text/javascript">
    /*tinyMCE.init({
     // General options
     mode : "textareas",
     theme : "advanced",
     skin : "o2k7",
     language :"en",
     skin_variant : "silver",
     forced_root_block : false, 
     force_br_newlines : true, 
     force_p_newlines : false,
     // plugins:"save,jfilebrowser",
     theme_advanced_buttons1 : "justifyleft,justifycenter,justifyright,justifyfull,|,bold,italic,underline,|,fontselect,notice,unnotice,|,fontselect,fontsizeselect,forecolor,backcolor",
     theme_advanced_buttons2 : "bullist,numlist,separator,outdent,indent,separator,undo,redo,link,unlink",
     theme_advanced_buttons3 : "hr,removeformat,visualaid,separator,sub,sup,separator,charmap,upload",
     
     });*/
    tinymce.init({
        selector: "textarea",
        plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons template textcolor paste fullpage textcolor"
        ],
        toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect",
        toolbar2: "cut copy paste | bullist numlist | outdent indent | undo redo | link unlink | forecolor backcolor",
        toolbar3: "table | hr | subscript superscript | charmap upload",
        toolbar_items_size: 'small',
        menubar: false,
        image_advtab: true,
        external_filemanager_path: "recursos/filemanager/",
        filemanager_title: "Subir archivos",
        external_plugins: {"filemanager": "../filemanager/plugin.min.js"}
    });
    $(document).ready(function() {
        $("#VImage").change(function() {
            var src = "Adjimage";
            var val = $(this).val();
            var cont = $(this).attr('checked');
            if (cont == "checked") {
                console.info('activado');
                active_file(src);
            }
            else {
                console.info('Desactivado');
                desactive_file(src);
            }
            //console.info('k'+cont);
        });
        $("#VArchivo").change(function() {
            var src = "Adjarchivo";
            var val = $(this).val();
            var cont = $(this).attr('checked');
            if (cont == "checked") {
                console.info('activado');
                active_file(src);
            }
            else {
                console.info('Desactivado');
                desactive_file(src);
            }
            //console.info('k'+cont);
        });
        $("#tipo").change(function() {
            var src = "Adjarchivo";
            var val = $(this).val();
            if (val != 'MENORES') {
                $('#categoria').hide();
                $('#categoria_sex').fadeIn();
            } else {
                $('#categoria_sex').hide();
                $('#categoria').fadeIn();
            }
        });
    });

    function active_file(src) {
        $('#option-form #' + src).removeClass('item-desactive');
        //$('#option-form #'+src).addClass('item-active');
        //alert(src);
    }
    function desactive_file(src) {
        //$('#option-form #'+src).removeClass('item-active');
        $('#option-form #' + src).addClass('item-desactive');
        //alert(src);
    }
</script>

<?php if ($stadoForm != NULL && $stadoForm == 1) { ?>
    <script>
        $(document).ready(function() {
            $('#message').fadeIn('normal', function() {
                $(this).delay(1500).fadeOut('normal');
            });

        });
    </script>
    <div class="form-state" id="message" style="display:none">
        <div class="message exito">Registro Guardado</div>
    </div>
<?php } ?>
<div id="record" class="formNew">
    <div id="option-form"> 
        <div class="item item-active"><span>REGISTRO DE TORNEO</span></div>
        <!--<div class="item item-desactive" id="Adjimage"><span>ADJUNTAR : Imagen</span></div>
        <div class="item " id="Adjarchivo"><span>ADJUNTAR : Archivo</span></div>-->
    </div>
    <form action="includes/modulos/torneo/torneo_acciones.php" method="post" enctype="multipart/form-data" name="formTorneo" id="formTorneo">
        <div class="top-item-form">
            <div class="item">
                <div class="label-input">Publicar:</div>
                <div class="input">
                    <div class="intr-checkbox">
                        <input name="estado" type="radio" id="activo" value="1" <?php echo ($state_tournament == 1) ? 'checked' : ''; ?>>
                        <label for="activo">S&iacute;</label>
                    </div>
                    <div class="intr-checkbox">
                        <input name="estado" type="radio" id="inactivo" value="0"  <?php echo ($state_tournament == '' || $state_tournament == 0) ? 'checked' : ''; ?>>
                        <label for="inactivo">No</label>
                    </div>
                </div>
            </div>
            <!--
        <div class="item">
            <div class="label-input">Adjuntar: <span>|</span></div>
            <div class="input" id="vA">
                <div class="intr-checkbox validA" style="display:none">
                <input name="VImage" id="VImage" type="checkbox" class="" checked="CHECKED">
                <label for="VImage" >Imagen</label>
                </div>
                <div class="intr-checkbox">
                <input name="VArchivo" id="VArchivo"  type="checkbox" class="validA" checked="checked">
                <label for="VArchivo">Archivo</label>
                </div>
            </div>
        </div>-->
            <div class="item">
                <div class="label-input">Intervalo de Torneo</div>
                <div class="input">
                    <div class="text interTour"><input name="init" type="text" class="text-input corner-input-intr small-input validate[required,custom[date]] _gldp" id="init" value="<?php echo $init_t; ?>" readonly="readonly"></div>
                    <div class="text interTour">
                        <input name="end" type="text" class="text-input corner-input-intr small-input validate[required,custom[date]] _gldp" id="end" value="<?php echo $end_t; ?>" readonly="readonly">
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="accion" name="accion" value="<?php echo $accion ?>" />
        <input type="hidden" id="idusuario" name="idusuario" value="<?php echo $_SESSION['idusuario']; ?>" />
        <input type="hidden" id="id_achievement" name="id_achievement" value="<?php echo $id_achievement; ?>" />
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="optionForm">
            <tr>
                <td width="11%">&nbsp;</td>
                <td colspan="2"><div class="intr-label">Titulo</div></td>
                <td><div class="intr-label"></div></td>
                <td width="12%">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3"><div align="left">
                        <textarea name="titulo" cols="40" rows="3" class="text-input corner-input-intr small-input" style="width: 100%" id="titulo"><?php echo $title_achievement; ?></textarea>
                    </div></td>
                <td><div align="left">
                    </div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td width="11%">&nbsp;</td>
                <td><div class="intr-label">Tipo</div></td>
                <td ><div class="intr-label">Categor&iacute;a:</div></td>
                <td><div class="intr-label">Lugar</div></td>
                <td width="12%">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <select name="tipo" id="tipo">
                        <option value="0" >Elegir...</option>
<?php //foreach($tipo as $row2){  ?>
                        <!--<option value="<?php //echo $row2[0]; ?>"><?php //echo $row2[1] ; ?></option>-->
<?php //}  ?>
                        <option value="SENIORS" <?php echo ($tipo == 'SENIORS') ? 'selected' : ''; ?>>SENIORS</option>
                        <option value="PRIMERA CATEGORIA" <?php echo ($tipo == 'PRIMERA CATEGORIA') ? 'selected' : ''; ?>>PRIMERA CATEGORIA</option>
                        <option value="MENORES" <?php echo ($tipo == 'MENORES') ? 'selected' : ''; ?>>MENORES</option>
                    </select></td>
                <td>
                    <select name="categoria" id="categoria" style="<?php echo $idcategoria == 0 ? 'display:none;' : ''; ?>">
                        <option value="0" >Elegir...</option>
<?php foreach ($categoria as $row2) { ?>
                            <option value="<?php echo $row2[0]; ?>" <?php echo ($idcategoria == $row2[0]) ? 'selected' : ''; ?>><?php echo (($row2[2] == '1') ? "Damas" : "Varones"), ' ', $row2[1]; ?></option>
<?php } ?>
                    </select>
                    <select name="categoria_sex" id="categoria_sex" style="<?php echo $idcategoria == 0 ? '' : 'display:none;'; ?>">
                        <option value="0" <?php echo ($genero == 0) ? 'selected' : ''; ?> >Varones</option>
                        <option value="1" <?php echo ($genero == 1) ? 'selected' : ''; ?>>Damas</option>
                    </select></td>
                <td><div align="left">
                        <select name="ciudad">
<?php foreach ($ciudad as $row2) { ?>
                                <option value="<?php echo $row2[5]; ?>" <?php echo ($lugar == $row2[5]) ? 'selected' : ''; ?>><?php echo $row2[5]; ?></option>
<?php } ?>
                        </select>
                    </div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3"><div class="intr-label">Descripci&oacute;n</div></td>
                <td></td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td colspan="3" valign="top"><input type="text" name="contenido" rows="7"  class="text-input corner-input-intr small-input" class="conteni" style="width: 100%" id="contenido" value="<?php echo nl2br($content_achievement); ?>"/></td>
                <td valign="top">&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td width="28%"><div class="intr-label">Inicio de Publicaci&oacute;n</div></td>
                <td width="26%"><div class="intr-label">Fin de Publicaci&oacute;n</div></td>
                <td width="23%">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><div align="left">
                        <input name="publicacion_ini" type="text" class="text-input corner-input-intr small-input" id="publicacion_ini" value="<?php echo $publication_star; ?>" readonly="readonly" />
                    </div></td>
                <td><div align="left">
                        <input name="publicacion_fin" type="text" class="text-input corner-input-intr small-input" id="publicacion_fin" value="<?php echo $publication_end; ?>" readonly="readonly" />
                    </div></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="middle">&nbsp;</td>
                <td colspan="3" align="center" valign="middle">
                    <div class="buttons" style="text-align:center">
                        <input name="Restablecer" type="reset" class="bootom cancel" value=" Cancelar " onclick="window.history.back();">
                        <input class="bootom save" type="submit" value=" Guardar ">
                    </div></td>
                <td align="center" valign="middle">&nbsp;</td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#init").glDatePicker({
            onChange: function(target, newDate)
            {
                target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
            }
        });
        $("#init").click(function() {
            $('#init-gldp').css({'top': '22px'});
        });
        $("#end").glDatePicker({
            onChange: function(target, newDate)
            {
                target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
            }
        });
        $("#end").click(function() {
            $('#end-gldp').css({'top': '22px'});
        });
        $("#fecha").glDatePicker({
            onChange: function(target, newDate)
            {
                target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
            }
        });
        $("#publicacion_ini").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 10,
                    startDate: new Date(),
                    allowOld: false
                });
        $("#publicacion_fin").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 20,
                    showPrevNext: true,
                    allowOld: false,
                    startDate: new Date()
                });
    });
</script> 

