<script>
$(document).ready(function(){
	$('.content-box tbody tr:even').addClass('alt-row');
 });
function EliminarDato(id,url){
	var msg = confirm("Desea eliminar este Registro")
	if ( msg ) {
	$.ajax({
		url: url,
		type: "POST",
		data: 'accion=D&id_achievement='+id,
		success: function(datos){
		//alert('registro -'+id+'- eliminado');
		//ConsultaDatos('includes/modulos/avisos/');
			$("#fila-"+id).fadeOut('slow',function(){$(this).remove();});
			}
	});
	}
	return false;
}
</script>
<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
        <h3>Todos los Torneos</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab current">Listado General</a></li>
            <li><a href="intranet.php?md=<?php echo md5('new_torneo')?>&fl=<?php echo $_REQUEST['fl'];?>">Escribir Nuevo</a></li>
        </ul>
        <div class="clear"></div>
        </div> <!-- End .content-box-header -->
        <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this div's tab -->
            <form action="" method="post">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th width="4%" align="left"><input class="check-all" type="checkbox"></th>
                    <th width="20%" align="left">NOMBRE</th>
                    <th width="9%" align="center" valign="middle">TIPO</th>
                    <th width="15%" align="center">CATEGORIA</th>
                    <th width="11%" align="center">FECHA</th>
                    <th width="12%" align="center" valign="middle">LUGAR</th>
                    <th width="8%" align="center" valign="middle">ESTADO</th>
                    <th width="12%" align="center" valign="middle">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
             <?php 
				include_once('includes/commons/intranet.class.php');
				$objData=new intranet;
				$result=$objData->getListTournamentList(' ORDER BY date_register DESC '); ?>
               <?php while($dataRow=mysql_fetch_row($result)){?>
               <tr id="fila-<?php echo $dataRow[0]; ?>">
               		<td align="left"><input type="checkbox" name="urls[]" value="0"></td>
                        <td align="left">
                    <?php echo $dataRow['1']; ?>
                    </td>
               		<td align="center" valign="middle"><?php echo $dataRow['6']; ?></td>
               		<td align="center" valign="middle"><?php echo ($dataRow['4']==0)?(($dataRow['3']==0)?'Varones':'Damas'):$dataRow['17']; ?></td>
               		<td align="center" valign="middle"><?php echo $dataRow['7']; ?></td>
               		<td align="center" valign="middle"><?php echo $dataRow['5']; ?></td>
                    <td align="center" valign="middle"><?php echo ($dataRow['13']==1)?'<span style="color:green">Activo</span>':'<span style="color:red">Inactivo</span>'; ?></td>
                    <td align="center" valign="middle"><!-- Icons -->
                        <a href="intranet.php?md=<?php echo md5('new_torneo').'&fl='.$_REQUEST['fl'].'&acc=U&cd='.$dataRow['0']; ?>" title="Edit"><img src="img/png/pencil.png" alt="Edit" border="0"></a>
                        <a href="javascript:EliminarDato(<?php echo $dataRow['0']; ?>,'includes/modulos/torneo/torneo_acciones.php')" onclick="<?php echo $dataRow['0']; ?>" title="Delete"><img src="img/png/cross.png" alt="Delete" border="0"></a>
                    </td>
               
               </tr>
               <?php }?>
				
                
                
            </tbody>
            <tfoot>
                <tr align="left">
                    <td colspan="8">
                    <div class="bulk-actions align-left">
                        <select name="option" class="first">
                            <option value="">Seleccionar acci&oacute;n...</option>
                            <option value="delete">Eliminar</option>
                        </select>
                    <input type="submit" class="button" name="apply" value="Aplicar">
                    </div>
                    <div class="clear"></div>
                    </td>
              </tr>
            </tfoot>										
            </table>
            </form>
        </div> <!-- End #tab1 -->
	</div> <!-- End .content-box-content -->			
</div>