<?php
ob_start();
session_start();
$accion = $_REQUEST['acc'];
$stadoForm = $_REQUEST['st'];
($accion == '') ? $accion = 'I' : $accion = 'U';
$idjugador = $_REQUEST['cd'];
include_once('includes/commons/intranet.class.php');
$objIntranet = new intranet;
$ciudad = $objIntranet->getListCity();
if ($accion == 'U') {
    $dataJugador = $objIntranet->getListSportsman('where idjugador=' . $idjugador);
    foreach ($dataJugador as $data) {
        $apellidos = $data['1'];
        $nombres = $data['2'];
        $completo = $data['3'];
        $fechanac = $data['4'];
        $lugar = $data['5'];
        $sexo = $data['6'];
        $flag = $data['7'];
    }
}
?>
<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode: "textareas",
        theme: "advanced",
        skin: "o2k7",
        language: "en",
        skin_variant: "silver",
        forced_root_block: false,
        force_br_newlines: true,
        force_p_newlines: false,
        //plugins:"save,jfilebrowser",
        theme_advanced_buttons1: "justifyleft,justifycenter,justifyright,justifyfull,|,bold,italic,underline,|,fontselect,notice,unnotice,|,fontselect,fontsizeselect,forecolor,backcolor",
        theme_advanced_buttons2: "bullist,numlist,separator,outdent,indent,separator,undo,redo,link,unlink",
        theme_advanced_buttons3: "hr,removeformat,visualaid,separator,sub,sup,separator,charmap,upload",
    });
    /*tinymce.init({
     selector: "textarea",
     plugins: [
     "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
     "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
     "table contextmenu directionality emoticons template textcolor paste fullpage textcolor"
     ],
     
     toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify |formatselect fontselect fontsizeselect",
     toolbar2: "cut copy paste | bullist numlist | outdent indent | undo redo | link unlink image | forecolor backcolor",
     toolbar3: "table | hr | subscript superscript | charmap | spellchecker",
     
     menubar: false,
     toolbar_items_size: 'small',
     
     style_formats: [
     {title: 'Bold text', inline: 'b'},
     {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
     {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
     {title: 'Example 1', inline: 'span', classes: 'example1'},
     {title: 'Example 2', inline: 'span', classes: 'example2'},
     {title: 'Table styles'},
     {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
     ],
     
     templates: [
     {title: 'Test template 1', content: 'Test 1'},
     {title: 'Test template 2', content: 'Test 2'}
     ]
     });*/
</script>
<?php if ($stadoForm != NULL && $stadoForm == 1) { ?>
    <script>
        $(document).ready(function() {
            $('#message').fadeIn('normal', function() {
                $(this).delay(1500).fadeOut('normal');
            });
        });
    </script>
    <style>

    </style>
    <div class="form-state" id="message" style="display:none">
        <div class="message exito">Registro Guardado</div>
    </div>
<?php } ?>
<?php if ($stadoForm != NULL && $stadoForm == 2) { ?>
    <script>
        $(document).ready(function() {
            $('#message').fadeIn('normal', function() {
                $(this).delay(1500).fadeOut('normal');
            });
        });
    </script>
    <style>

    </style>
    <div class="form-state" id="message" style="display:none">
        <div class="message exito">Error!!!. No se pudo guardar el nuevo registro!!!</div>
    </div>
<?php } ?>
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Nuevo Jugador</h3>
        <ul class="content-box-tabs">
            <li><a href="intranet.php?md=<?php echo md5('admi_jugador') ?>&fl=<?php echo $_REQUEST['fl']; ?>">Listado General</a></li>
            <li><a href="#tab2" class="default-tab current">Nuevo Jugador</a></li>
        </ul>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this div's tab -->
            <div id="record" class="formNew">
                <form action="includes/modulos/jugador/jugador_acciones.php"
                      method="post"
                      enctype="multipart/form-data"
                      name="formTorneo" id="formTorneo">
                    <input type="hidden" id="accion" name="accion" value="<?php echo $accion ?>" />
                    <input type="hidden" id="idusuario" name="idusuario" value="<?php echo $_SESSION['idusuario']; ?>" />
                    <input type="hidden" id="idjugador" name="idjugador" value="<?php echo $idjugador; ?>" />
                    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="optionForm">
                        <tr>
                            <td width="11%">&nbsp;</td>
                            <td colspan="2"><div class="intr-label">Nombres</div></td>
                            <td><div class="intr-label">Fecha Nacimiento</div></td>
                            <td width="12%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2"><div align="left">
                                    <input type="text" name="nombres" id="nombres" class="text-input corner-input-intr small-input" value="<?php echo $nombres; ?>" />
                                </div></td>
                            <td><div align="left">
                                    <input name="fechanac" type="text" class="text-input corner-input-intr small-input" id="fechanac" value="<?php echo $fechanac; ?>" />
                                </div></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="11%">&nbsp;</td>
                            <td colspan="2"><div class="intr-label">Apellidos</div></td>
                            <td><div class="intr-label">Sexo</div></td>
                            <td width="12%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2"><div align="left">
                                    <input type="text" name="apellidos" id="apellidos" class="text-input corner-input-intr small-input" value="<?php echo $apellidos; ?>" />
                                </div></td>
                            <td><div align="left">
                                    <select name="sexo">
                                        <option value="0" <?php echo ($sexo == '0') ? 'selected' : ''; ?>>Masculino</option>
                                        <option value="1" <?php echo ($sexo == '1') ? 'selected' : ''; ?>>Femenino</option>
                                    </select>
                                </div></td>
                            <td>&nbsp;</td>
                        </tr>  
                        <tr>
                            <td width="11%">&nbsp;</td>
                            <td colspan="2"><div class="intr-label">Lugar</div></td>
                            <td><div class="intr-label"></div></td>
                            <td width="12%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2"><div align="left">
                                    <select name="lugar">
                                        <?php foreach ($ciudad as $row2) { ?>
                                            <option value="<?php echo $row2[5]; ?>"  <?php echo ($lugar == $row2[5]) ? 'selected' : ''; ?>><?php echo $row2[5]; ?></option>
                                        <?php } ?>
                                    </select>
                                </div></td>
                            <td><div align="left">
                                </div></td>
                            <td>&nbsp;</td>
                        </tr> 

                        <tr>
                            <td align="center" valign="middle">&nbsp;</td>
                            <td colspan="3" align="center" valign="middle">
                                <div class="buttons" style="text-align:center">
                                    <input name="Restablecer" type="reset" class="bootom cancel" value=" Cancelar ">
                                    <input class="bootom save" type="submit" value=" Guardar ">
                                </div></td>
                            <td align="center" valign="middle">&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
        </div> <!-- End #tab1 -->
    </div> <!-- End .content-box-content -->			
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        $(".save").click(function(e) {
            e.preventDefault();
            var nombre = $("input[name=nombres]").val();
            var apellidos = $("input[name=apellidos]").val();
            var nacimiento = $("input[name=fechanac]").val();

            if (nombre.trim().length < 1)
            {
                alert("Porfavor complete su nombre!!!");
            }
            else if (nombre.trim().length < 5)
            {
                alert("Porfavor escriba sus nombre completos!!!");
            }
            else if (isNaN(nombre.trim()) == false)
            {
                alert("Su nombre no puede ser un valor numerico");
            }
            else if (nombre.trim().length > 300)
            {
                alert("Campo Nombre: No puede exceder los 300 caracteres");
            }
            else if (apellidos.trim().length < 1)
            {
                alert("Porfavor complete su apellido");
            }
            else if (apellidos.trim() < 5)
            {
                alert("Porfavor escriba sus 2 apellidos correctamente");
            }
            else if (apellidos.trim().length > 320)
            {
                alert("Campo Apellidos: No puede exceder los 320 caracteres!!!");
            }
            else if (isNaN(apellidos.trim()) == false)
            {
                alert("El campo apellido no soporta valores numericos");
            }
            else if (nacimiento < '1900/1/1')
            {
                alert("La fecha ingresada no es correcta");
            }
            else
            {
                document.formTorneo.submit();
            }


        });
        $("#fechanac").glDatePicker({
            onChange: function(target, newDate)
            {
                target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
            }
        });
        $("#publicacion_ini").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 10,
                    startDate: new Date(),
                    selectedDate: 3,
                    allowOld: false
                });
        $("#publicacion_fin").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 20,
                    showPrevNext: true,
                    allowOld: false,
                    startDate: new Date()
                });
    });
</script>