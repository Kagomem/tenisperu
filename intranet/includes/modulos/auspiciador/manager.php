<?php
$Img = '<img src="img/png/image.png" alt="" border="0" height="24" width="24" align="right">';
$stadoForm = $_REQUEST['st'];
?>
<script>
    $(document).ready(function() {
        $('.content-box tbody tr:even').addClass('alt-row');
    });
    function EliminarDato(id, url) {
        var msg = confirm("Desea eliminar este Registro")
        if (msg) {
            $.ajax({
                url: url,
                type: "POST",
                data: 'accion=D&id_sponsor=' + id,
                success: function(datos) {
                    //alert('registro -'+id+'- eliminado');
                    //ConsultaDatos('includes/modulos/avisos/');
                    $("#fila-" + id).fadeOut('slow', function() {
                        $(this).remove();
                    });
                }
            });
        }
        return false;
    }
</script>
<div class="content-box"><!-- Start Content Box -->
    <?php if ($stadoForm != NULL && $stadoForm == 1) { ?>
        <script>
            $(document).ready(function() {
                $('#message').fadeIn('normal', function() {
                    $(this).delay(1500).fadeOut('normal');
                });
            });
        </script>
        <div class="form-state" id="message" style="display:none">
            <div class="message exito">Registro Guardado</div>
        </div>
    <?php } ?>
    <?php if ($stadoForm != NULL && $stadoForm == 2) { ?>
        <script>
            $(document).ready(function() {
                $('#message').fadeIn('normal', function() {
                    $(this).delay(1500).fadeOut('normal');
                });
            });
        </script>
        <div class="form-state" id="message" style="display:none">
            <div class="message exito">Error!!!. No se pudo agregar el nuevo registro.:Auspiciador Duplicado</div>
        </div>
    <?php } ?>
    <div class="content-box-header">
        <h3>Lista de Auspiciadores</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab current">Lista de Auspiciadores</a></li>
            <li><a href="intranet.php?md=<?php echo md5('new_auspiciador') ?>&amp;fl=<?php echo $_REQUEST['fl']; ?>">Escribir Nuevo</a></li>
        </ul>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this div's tab -->
            <form action="" method="post">
                <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="4%" align="left"><input class="check-all" type="checkbox"></th>
                            <th width="40%" align="left">TITULO</th>
                            <th width="14%" align="center" valign="middle">IMAGEN</th>
                            <th width="13%" align="center" valign="middle">FECHA</th>
                            <th width="11%" align="center" valign="middle">ESTADO</th>
                            <th width="18%" align="center" valign="middle">ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include_once('includes/commons/intranet.class.php');
                        $objData = new intranet;
                        $result = $objData->getListSponsor(' ORDER BY date_register DESC ');
                        ?>
                        <?php
                        while ($dataRow = mysql_fetch_row($result)) {
                            $editImg = '<a href="intranet.php?md=' . md5('new-img-auspiciador.php') . '&fl=7&cd=' . $dataRow[0] . '"><img src="img/png/insert-image.png" alt="" border="0"  height="24" width="24" align="right"></a>';
                            ?>
                            <tr id="fila-<?php echo $dataRow[0]; ?>">
                                <td align="left"><input type="checkbox" name="urls[]" value="0"></td>
                                <td align="left">
    <?php echo $dataRow['1']; ?>
                                </td>
                                <td align="center" valign="middle"><?php echo ($dataRow['2'] == '') ? '<span style="color:red">NO</span>' : '<span style="color:green">SI</span>';
    echo '  ' . $editImg; ?></td>
                                <td align="center" valign="middle"><?php echo $dataRow['4']; ?></td>
                                <td align="center" valign="middle"><?php echo ($dataRow['7'] == 1) ? '<span style="color:green">Activo</span>' : '<span style="color:red">Inactivo</span>'; ?></td>
                                <td align="center" valign="middle"><!-- Icons -->
                                    <a href="intranet.php?md=<?php echo md5('new_auspiciador') . '&fl=' . $_REQUEST['fl'] . '&acc=U&cd=' . $dataRow['0']; ?>" title="Edit"><img  src="img/png/pencil.png" alt="Edit" border="0" ></a>
                                    <a href="javascript:EliminarDato(<?php echo $dataRow['0']; ?>,'includes/modulos/auspiciador/auspiciador_acciones.php')" onclick="<?php echo $dataRow['0']; ?>" title="Delete"><img src="img/png/cross.png" alt="Delete" border="0"></a>
                                </td>

                            </tr>
<?php } ?>   
                    </tbody>
                    <tfoot>
                        <tr align="left">
        <!--                    <td colspan="8">
                            <div class="bulk-actions align-left">
                                <select name="option" class="first">
                                    <option value="">Select action...</option>
                                    <option value="delete">Delete</option>
                                    <option value="move" class="nextSelect">Move to URL</option>
                                    <option value="copy" class="nextSelect">Copy to URL</option>
                                </select>
                                <select name="page" class="second">
                                    <option value="0">Default</option>
                                    <option value="">home page</option><option value="">--Pagina nueva</option>
                                    <option value="^http://dev.ikfsystems.sk/ikfa/prueba$">Prueba</option>											
                                </select>
                            <input type="submit" class="button" name="apply" value="Apply on selected">
                            </div>
                            <div class="clear"></div>
                            </td>-->
                        </tr>
                    </tfoot>										
                </table>
            </form>
        </div> <!-- End #tab1 -->
    </div> <!-- End .content-box-content -->			
</div>