<?php ob_start(); session_start(); 
$accion=$_REQUEST['acc'];
$stadoForm=$_REQUEST['st'];
($accion=='')?$accion='I':$accion='U';
if($accion=='U'){
	$idjugador=$_REQUEST['cd'];
	include_once('includes/commons/intranet.class.php');
	$objIntranet=new intranet;
	$records=$objIntranet->detallepuntos($idjugador);
}?>
<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
<h3><?php echo $records[0][4]; ?></h3>
        <ul class="content-box-tabs">
            <li><a href="intranet.php?md=<?php echo md5('admi_reporte')?>&fl=<?php echo $_REQUEST['fl'].'&tipo='.$_REQUEST['tipo'].'&categoria='.$_REQUEST['categoria'];?>">Regresar a Principal</a></li>
            <li><a href="#tab1" class="default-tab current">Historial de Puntajes</a></li>
        </ul>
        <div class="clear"></div>
        </div> <!-- End .content-box-header -->
        <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this div's tab -->
<div id="record" class="formNew">
 <?php
$ssingle=0;
$sdoble=0;?>
<table class="reporte" width="100%">
<columns>
<col width="30%">
<col width="10%">
<col width="20%">
<col width="20%">
<col width="10%">
<col width="10%">
</columns>
<thead>
<tr><th rowspan="2">Torneo</th>
<th rowspan="2">Fecha</th><th rowspan="2">Lugar</th>
<th rowspan="2">Tipo</th><th colspan="2">Puntos</th>
</tr>
<tr><th>Singles</th><th>Dobles</th></tr>
</thead>
<tbody>
<?php if(isset($records)) : 
	foreach($records as $row) : 
	$ssingle=$ssingle+$row[5];
	$sdoble=$sdoble+$row[6];
	?>
	<tr>
	<td><?php echo $row[0];?></td>
	<td><?php echo $row[1];?></td>
	<td><?php echo $row[2];?></td>
	<td><?php echo $row[3];?></td>
	<td align="center"><?php echo $row[5];?></td>
	<td align="center"><?php echo $row[6];?></td>
	</tr>
<?php 
	endforeach; ?>
<?php    
else:
echo "<tr><td colspan='3'>No hay registro de participacion en torneos</td></tr>";
endif;?>
<tr style="font-size:15px;"><th colspan="4"></th><th><?=$ssingle?></th><th><?=$sdoble?></th></tr>
</tbody>
</table>

<table class="reporte" width="100%">
<columns>
<col width="80%">
<col width="10%">
<col width="10%">
</columns>
<thead>
<tr style="font-size:15px;border-top:1px solid #CCC;"><th colspan="6" align="right">Total:&nbsp;&nbsp;<?=$ssingle?> + (<?=$sdoble?>) 25% = <span style="font-size:20px;"><?=$ssingle+($sdoble*25/100)?></span></td></tr>
</thead>
</table>
</div>