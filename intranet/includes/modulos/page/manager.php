<script>
$(document).ready(function(){
	$('.content-box tbody tr:even').addClass('alt-row');
 });
function EliminarDato(id,url){
	var msg = confirm("Desea eliminar este Registro")
	if ( msg ) {
	$.ajax({
		url: url,
		type: "POST",
		data: 'accion=D&idpage='+id,
		success: function(datos){
		//alert('registro -'+id+'- eliminado');
		//ConsultaDatos('includes/modulos/avisos/');
			$("#fila-"+id).fadeOut('slow',function(){$(this).remove();});
			}
	});
	}
	return false;
}
</script>
<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
        <h3>Interiores</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab current">Listado General</a></li>
            <li><a href="intranet.php?md=<?php echo md5('new_page')?>&fl=<?php echo $_REQUEST['fl'];?>">Nueva Interior</a></li>
        </ul>
        <div class="clear"></div>
        </div> <!-- End .content-box-header -->
        <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this div's tab -->
            <form action="" method="post">
            <table border="0" align="center" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th width="5%" align="left">
<!--                        <input class="check-all" type="checkbox">-->
                    </th>
                    <th width="40%" align="left">T&iacute;tulo</th>
                    <th width="15%" align="center" valign="middle">Superior</th>
                    <th width="10%" align="center" valign="middle">Orden</th>
                    <th width="10%" align="center" valign="middle">Men&uacute;?</th>
                    <th width="10%" align="center" valign="middle">Publicado</th>
                    <th width="10%" align="center" valign="middle">Acciones</th>
              </tr>
            </thead>
            <tbody>
             <?php 
				include_once('includes/commons/intranet.class.php');
				$objData=new intranet;
				$result=$objData->getListGeneralPage('');
			if($objData->numPage>0){
				?>
               <?php foreach($result as $dataRow){ ?>
               <tr id="fila-<?php echo $dataRow[0]; ?>">
               		<td align="left">
<!--                            <input type="checkbox" name="urls[]" value="0">-->
                        </td>
               		<td valign="middle"><b><?php echo ($dataRow['1']==0)?'<u>'.$dataRow['2'].'</u>':$dataRow['2']; ?></b></td>
               		<td valign="middle"><?php echo $dataRow['5']; ?></td>
               		<td valign="middle" align="center"><?php echo $dataRow['3']; ?></td>
                    <td align="center" valign="middle"><?php echo ($dataRow['7']==1)?'<span style="color:green">S&iacute;</span>':'<span style="color:red">No</span>'; ?></td>
                    <td align="center" valign="middle"><?php echo ($dataRow['6']==1)?'<span style="color:green">S&iacute;</span>':'<span style="color:red">No</span>'; ?></td>
                    <td align="center" valign="middle">
                        <a href="intranet.php?md=<?php echo md5('new_page').'&fl='.$_REQUEST['fl'].'&acc=U&cd='.$dataRow['0']; ?>" title="Edit"><img src="http://dev.ikfsystems.sk/ikfa/media/admin/images/icons/pencil.png" alt="Edit" border="0"></a>
                        <a href="javascript:EliminarDato(<?php echo $dataRow['0']; ?>,'includes/modulos/page/page_acciones.php')" onclick="<?php echo $dataRow['0']; ?>" title="Delete"><img src="http://dev.ikfsystems.sk/ikfa/media/admin/images/icons/cross.png" alt="Delete" border="0"></a>
                    </td>
               
               </tr>
               <?php }} ?>
				
                
                
            </tbody>
            <tfoot>
<!--                <tr align="left">
                    <td colspan="7">
                    <div class="bulk-actions align-left">
                        <select name="option" class="first">
                            <option value="">Seleccionar accion...</option>
                            <option value="delete">Eliminar</option>
                        </select>
                    <input type="submit" class="button" name="apply" value="Aplicar">
                    </div>
                    <div class="clear"></div>
                    </td>
              </tr>-->
            </tfoot>										
            </table>
            </form>
        </div> <!-- End #tab1 -->
	</div> <!-- End .content-box-content -->			
</div>