<?php
ob_start();
session_start();
$accion = $_REQUEST['acc'];
$stadoForm = $_REQUEST['st'];
($accion == '') ? $accion = 'I' : $accion = 'U';
if ($accion == 'U') {
    $id_front = $_REQUEST['cd'];
    include_once('includes/commons/intranet.class.php');
    $objIntranet = new intranet;
    $dataFront = $objIntranet->getListFront('where id_front=' . $id_front);
    while ($data = mysql_fetch_array($dataFront)) {
        $title_front = $data['1'];
        $content_front = $data['2'];
        $image_front = $data['3'];
        $date_front = $data['4'];
        $publication_star = $data['7'];
        $publication_end = $data['8'];
        $state_front = $data['9'];
    }
}
?>
<script type="text/javascript">
    /*tinyMCE.init({
     // General options
     mode : "textareas",
     theme : "advanced",
     skin : "o2k7",
     language :"en",
     skin_variant : "silver",
     forced_root_block : false, 
     force_br_newlines : true, 
     force_p_newlines : false,
     //plugins:"save,jfilebrowser",
     theme_advanced_buttons1 : "justifyleft,justifycenter,justifyright,justifyfull,|,bold,italic,underline,|,fontselect,notice,unnotice,|,fontselect,fontsizeselect,forecolor,backcolor",
     theme_advanced_buttons2 : "bullist,numlist,separator,outdent,indent,separator,undo,redo,link,unlink",
     theme_advanced_buttons3 : "hr,removeformat,visualaid,separator,sub,sup,separator,charmap,upload",
     
     });*/
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking fullscreen",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager template textcolor fullpage"
        ],
        toolbar1: "bold italic underline | alignleft aligncenter alignright alignjustify |formatselect fontselect fontsizeselect | styleselect ",
        toolbar2: "bullist numlist | outdent indent | undo redo | link unlink | print preview code | forecolor backcolor",
        toolbar3: "copy | table | hr | subscript superscript | charmap | removeformat ",
        toolbar_items_size: 'small',
        menubar: false,
        image_advtab: true,
        external_filemanager_path: "recursos/filemanager/",
        filemanager_title: "Subir archivos",
        external_plugins: {"filemanager": "../filemanager/plugin.min.js"}
    });
</script>
<?php if ($stadoForm != NULL && $stadoForm == 1) { ?>
    <script>
        $(document).ready(function() {
            $('#message').fadeIn('normal', function() {
                $(this).delay(1500).fadeOut('normal');
            });
        });
    </script>
    <div class="form-state" id="message" style="display:none">
        <div class="message exito">Registro Guardado</div>
    </div>
<?php } ?>
<div id="record" class="formNew">
    <form action="includes/modulos/portada/portada_acciones.php" method="post"
          enctype="multipart/form-data" name="formFront" id="formFront">
        <input type="hidden" id="accion" name="accion" value="<?php echo $accion ?>" />
        <input type="hidden" id="idusuario" name="idusuario" value="<?php echo $_SESSION['idusuario']; ?>" />
        <input type="hidden" id="id_front" name="id_front" value="<?php echo $id_front; ?>" />
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="optionForm">
            <tr>
                <td width="7%">&nbsp;</td>
                <td colspan="3"><div class="intr-label">Titulo</div></td>
                <td width="8%">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3"><div align="left">
                        <input name="titulo" type="text" class="text-input corner-input-intr small-input" id="titulo" value="<?php echo $title_front; ?>" maxlength="280" />
                    </div></td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td><div class="intr-label">Inicio de Publicaci&oacute;n</div></td>
                <td><div class="intr-label">Fin de Publicaci&oacute;n</div></td>
                <td width="28%"><div class="intr-label">Fecha</div></td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td><div align="left">
                        <input name="publicacion_ini" type="text" class="text-input corner-input-intr small-input" id="publicacion_ini" value="<?php echo $publication_star; ?>" maxlength="12" readonly="readonly"/>
                    </div></td>
                <td><div align="left">
                        <input name="publicacion_fin" type="text" class="text-input corner-input-intr small-input" id="publicacion_fin" value="<?php echo $publication_end; ?>" maxlength="12" readonly="readonly" />
                    </div></td>
                <td width="28%"><div align="left">
                        <input name="fecha" type="text" class="text-input corner-input-intr small-input" id="fecha" value="<?php echo $date_front; ?>" maxlength="12" readonly="readonly" />
                    </div></td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td width="29%"><div class="intr-label">Estado</div></td>
                <td width="28%"><div class="intr-label">
                        <?php if ($accion == 'I') { ?>
                            Imagen
                        <?php } ?>
                    </div></td>
                <td width="28%">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><div align="center">
                        <div class="intr-checkbox">
                            <input name="estado" type="radio" id="activo" value="1" <?php echo $state_front == 1 ? 'checked="checked"' : ''; ?> />
                            <label for="activo">Publicar</label>
                        </div>
                        <div class="intr-checkbox">
                            <input name="estado" type="radio" id="inactivo" value="0" <?php echo $state_front == 0 ? 'checked="checked"' : ''; ?> />
                            <label for="inactivo">No Publicar</label>
                        </div>
                    </div></td>
                <td><div align="center">
                        <?php if ($accion == 'I') { ?>
                            <div class="intr-checkbox">
                                <input name="image" type="radio" id="imageSI" value="1" <?php echo $image_front != '' ? 'checked="checked"' : ''; ?> />
                                <label for="imageSI">Con Imagen</label>
                            </div>
                            <div class="intr-checkbox">
                                <input name="image" type="radio" id="imageNO" value="0" <?php echo $image_front == '' ? 'checked="checked"' : ''; ?> />
                                <label for="imageNO">Sin Imagen</label>
                            </div>
                        <?php } ?>
                    </div></td>
                <td width="28%">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="middle">&nbsp;</td>
                <td colspan="3"><div class="intr-label">Contenido</div></td>
                <td align="center" valign="middle">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="middle">&nbsp;</td>
                <td colspan="3" align="center" valign="middle"><textarea name="contenido" rows="7" class="conteni" style="width: 100%" id="contenido"><?php echo $content_front; ?></textarea></td>
                <td align="center" valign="middle">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="middle">&nbsp;</td>
                <td colspan="3" align="center" valign="middle">
                    <div class="buttons" style="text-align:center">
                        <input name="Restablecer" type="reset" class="bootom cancel" value=" Cancelar ">
                        <input class="bootom save" name="save" id="save" type="submit" value=" Guardar ">
                    </div>
                    <p></p></td>
                <td align="center" valign="middle">&nbsp;</td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#save").click(function(e) {
            e.preventDefault();
            var titulo = $("#titulo").val();
            var inicioPublicacion = $("input[name=publicacion_ini]").val();
            var finPublicacion = $("input[name=publicacion_fin]").val();
            var contenido = tinyMCE.get('contenido').getContent();

            if (titulo.trim().length < 1)
            {
                alert("Porfavor complete todos los campos");
            }
            else if (titulo.trim().length < 5)
            {
                alert("Escriba un titulo con un minimo de 5 caracteres");
            }
            else if (titulo.trim().length > 400)
            {
                alert("Escriba un titulo con un maximo de 400 caracteres");
            }
            else if (isNaN(titulo.trim()) == false)
            {
                alert("El titulo no puede ser un valor numerico!!!");
            }
            else if (finPublicacion < inicioPublicacion)
            {
                alert("Erro:La fecha de fin de la publicacion no puede ser una fecha menor que la fecha de inicio");
            }
            else if (contenido.trim().length < 1)
            {
                alert("Porfavor escriba un contenido");
            }
            else
            {
                document.formFront.submit();
            }
        })

        $("#fecha").glDatePicker({
            onChange: function(target, newDate)
            {
                target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
            }
        });
        $("#publicacion_ini").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 10,
                    startDate: new Date(),
                    allowMonthSelect: false
                });
        $("#publicacion_fin").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 20,
                    showPrevNext: true,
                    allowMonthSelect: false,
                    startDate: new Date()
                });
    });
</script>