<?php

ob_start();
session_start();
include_once('../../commons/intranet.class.php');
$accion = $_REQUEST['accion'];
/* echo $accion.'<br>'; */
$objIntranet = new intranet;
switch ($accion) {
    case 'I':
        $title_front = $_REQUEST['titulo'];
        $content_front = $_REQUEST['contenido'];
        $image_front = $_REQUEST['image'];
        $publication_front = $_REQUEST['portada'];
        $date_front = $_REQUEST['fecha'];
        $publication_star = $_REQUEST['publicacion_ini'];
        $publication_end = $_REQUEST['publicacion_fin'];
        $state_front = $_REQUEST['estado'];
        $id_user_creator = $_REQUEST['idusuario'];

        if ($image_front == 1) {
            $id_front = $objIntranet->setInsertFront($title_front, $content_front, $date_front, $publication_star, $publication_end, $state_front, $id_user_creator);

            header('Location:../../../intranet.php?md=' . md5('new-img-portada.php') . '&fl=1&cd=' . $id_front . '&st=1');
        } else {
            $id_front = $objIntranet->setInsertFront($title_front, $content_front, $date_front, $publication_star, $publication_end, $state_front, $id_user_creator);

            header('Location:../../../intranet.php?md=' . md5('new_portada') . '&fl=1&st=1');
        }
        break;
    case 'U':
        $id_front = $_REQUEST['id_front'];
        $title_front = $_REQUEST['titulo'];
        $content_front = $_REQUEST['contenido'];
        $image_front = $_REQUEST['image'];
        $date_front = $_REQUEST['fecha'];
        $publication_star = $_REQUEST['publicacion_ini'];
        $publication_end = $_REQUEST['publicacion_fin'];
        $state_front = $_REQUEST['estado'];
        $id_user_modified = $_REQUEST['idusuario'];
        $objIntranet->setUpdateFront($id_front, $title_front, $content_front, $date_front, $publication_star, $publication_end, $state_front, $id_user_modified);
        header('Location:../../../intranet.php?md=' . md5('admi_portada') . '&fl=1');
        break;
    case 'R': break;

    case 'D':
        $id_front = $_REQUEST['id_front'];
        $objIntranet->deleteFront($id_front);
        //header('Location:index.php');
        break;
}
?>