<?php
$Img = '<img src="img/png/image.png" alt="" border="0" height="24" width="24" align="right">';
?>
<style>
    .dxe3sfaex{
        background-image: url(img/png/1325882758_star.png);
        background-repeat: no-repeat;
        background-position: center top;
        display: block;
        width: 16px;
        height: 16px;
        cursor:pointer;
    }
    .dxe3sfaexX{
        background-image: url(img/png/1325882758_star.png);
        background-repeat: no-repeat;
        background-position: center -16px;
        display: block;
        width: 16px;
        height: 16px;
        cursor:pointer;
    }
</style>
<script>
    $(document).ready(function() {
        $('.content-box tbody tr:even').addClass('alt-row');
    });
    function EliminarDato(id, url) {
        var msg = confirm("Desea eliminar este Registro")
        if (msg) {
            $.ajax({
                url: url,
                type: "POST",
                data: 'accion=D&id_notice=' + id,
                success: function(datos) {
                    //alert('registro -'+id+'- eliminado');
                    //ConsultaDatos('includes/modulos/avisos/');
                    $("#fila-" + id).fadeOut('slow', function() {
                        $(this).remove();
                    });
                }
            });
        }
        return false;
    }
</script>
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Manage layouts</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab current">Listado General</a></li>
            <li><a href="intranet.php?md=<?php echo md5('new_noticia') ?>&fl=<?php echo $_REQUEST['fl']; ?>">Escribir Nuevo</a></li>
        </ul>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this div's tab -->
            <form action="" method="post">
                <table border="0" align="center" cellpadding="0" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="4%" align="left"><input class="check-all" type="checkbox"></th>
                            <th width="46%" align="left">TITULO</th>
                            <th width="11%" align="center" valign="middle">Image/Video</th>
                            <th width="11%" align="center" valign="middle">FECHA</th>
                            <th width="10%" align="center" valign="middle">ESTADO</th>
                            <th width="18%" align="center" valign="middle">ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include_once('includes/commons/intranet.class.php');
                        $objData = new intranet;
                        $result = $objData->getListNotice(' ORDER BY date_register DESC ');
                        ?>
                        <?php
                        while ($dataRow = mysql_fetch_row($result)) {
                            $editImg = '<a href="intranet.php?md=' . md5('new-img-noticia.php') . '&fl=2&cd=' . $dataRow[0] . '">
                                <img src="img/png/insert-image.png" alt="" border="0"  height="24" width="24" align="right"></a>';
                            $editVid = '<a href="intranet.php?md=' . md5('new-video-noticia.php') . '&fl=2&cd=' . $dataRow[0] . '&acb=u">
                                <img src="img/png/insert-video.png" alt="" border="0"  height="24" width="24" align="right"></a>'
                            ?>
                            <tr id="fila-<?php echo $dataRow[0]; ?>">
                                     <td align="left"><!--<input type="checkbox" name="urls[]" value="0">-->
                                    <span class="dxe3sfaex<?php echo ($dataRow['14'] == 1) ? '' : 'X'; ?>" title="Noticia Destacada"></span>  
                                </td>
                                <td align="left">
                                    <?php
                                    echo substr($dataRow['1'], 0, 250);
                                    // echo $dataRow['1'];
                                    ?>
                                </td>
                                <td align="center" valign="middle">
                                    <?php
                                    if ($dataRow['3'] != '') {
                                        echo $editImg;
                                    } else if ($dataRow['15'] != '') {
                                        echo $editVid;
                                    } else {
                                        echo '<span style="color:red">Ninguno</span>';
                                    }
                                    ?>
                                </td>
                                <td align="center" valign="middle"><?php $fecha_C = date_create($dataRow['6']);
                                echo date_format($fecha_C, "d/m/Y"); ?>
                                </td>
                                <td align="center" valign="middle"><?php echo ($dataRow['11'] == 1) ? '<span style="color:green">Activo</span>' : '<span style="color:red">Inactivo</span>'; ?>
                                </td>
                                <td align="center" valign="middle"><!-- Icons -->
                                    <a href="intranet.php?md=<?php echo md5('new_noticia') . '&fl=' . $_REQUEST['fl'] . '&acc=U&cd=' . $dataRow['0']; ?>" title="Edit"><img  src="img/png/pencil.png" alt="Edit" border="0" ></a>
                                    <a href="javascript:EliminarDato(<?php echo $dataRow['0']; ?>,'includes/modulos/noticia/noticia_acciones.php')" onclick="<?php echo $dataRow['0']; ?>" title="Delete"><img src="img/png/cross.png" alt="Delete" border="0"></a>
                                </td>

                            </tr>
                        <?php } ?>



                    </tbody>
                    <tfoot>
                        <tr align="left">
                            <td colspan="8">
                                <div class="bulk-actions align-left">
                                    <select name="option" class="first">
                                        <option value="">Select action...</option>
                                        <option value="delete">Delete</option>
                                        <option value="move" class="nextSelect">Move to URL</option>
                                        <option value="copy" class="nextSelect">Copy to URL</option>
                                    </select>
                                    <select name="page" class="second">
                                        <option value="0">Default</option>
                                        <option value="">home page</option><option value="">--Pagina nueva</option>
                                        <option value="^http://dev.ikfsystems.sk/ikfa/prueba$">Prueba</option>											
                                    </select>
                                    <input type="submit" class="button" name="apply" value="Apply on selected">
                                </div>
                                <div class="clear"></div>
                            </td>
                        </tr>
                    </tfoot>										
                </table>
            </form>
        </div> <!-- End #tab1 -->
    </div> <!-- End .content-box-content -->			
</div>