<?php

ob_start();
session_start();
include_once('../../commons/intranet.class.php');
$accion = $_REQUEST['accion'];
/* echo $accion.'<br>'; */
$objIntranet = new intranet;
switch ($accion) {
    case 'I':
        $title_notice = $_REQUEST['titulo'];
        $content_notice = $_REQUEST['contenido'];
        $image_notice = $_REQUEST['imageVideo'];
        $section_notice = '1';
        $publication_front = $_REQUEST['portada'];
        $url_notice = $_REQUEST['dominio'];
        $date_notice = $_REQUEST['fecha'];
        $publication_star = $_REQUEST['publicacion_ini'];
        $publication_end = $_REQUEST['publicacion_fin'];
        $state_notice = $_REQUEST['estado'];
        $id_user_creator = $_REQUEST['idusuario'];
        $highlight_notice = $_REQUEST['destacada'];
        $id_notice = $objIntranet->setInsertNotice($title_notice, $content_notice, $section_notice, $publication_front, $date_notice, $publication_star, $publication_end, $state_notice, $id_user_creator, $highlight_notice);
        echo $highlight_notice;
        if ($image_notice == 1) {
            header('Location:../../../intranet.php?md=' . md5('new-img-noticia.php') . '&fl=2&cd=' . $id_notice . '&st=1');
        } else if ($image_notice == 2) {
            header('Location:../../../intranet.php?md=' . md5('new-video-noticia.php') . '&fl=2&cd=' . $id_notice . '&st=1');
        } else {
            header('Location:../../../intranet.php?md=' . md5('new_noticia') . '&fl=2&st=1');
        }
        break;
    case 'U':
        $id_notice = $_REQUEST['id_notice'];
        $title_notice = $_REQUEST['titulo'];
        $content_notice = $_REQUEST['contenido'];
        $image_notice = $_REQUEST['image'];
        $section_notice = '1';
        $publication_front = $_REQUEST['portada'];
        $url_notice = $_REQUEST['dominio'];
        $date_notice = $_REQUEST['fecha'];
        $publication_star = $_REQUEST['publicacion_ini'];
        $publication_end = $_REQUEST['publicacion_fin'];
        $state_notice = $_REQUEST['estado'];
        $id_user_modified = $_REQUEST['idusuario'];
        $highlight_notice = $_REQUEST['destacada'];
        echo $state_notice . '<br>';
        $objIntranet->setUpdateNotice($id_notice, $title_notice, $content_notice, 
                $section_notice, $publication_front, $date_notice, $publication_star, 
                $publication_end, $state_notice, $id_user_modified, $highlight_notice);
        header('Location:../../../intranet.php?md=' . md5('admi_noticia') . '&fl=2');
        break;
    case 'V':
        $id_notice = $_REQUEST['id_notice'];
        $id_user_modified = $_REQUEST['idusuario'];
        $video_notice = $_REQUEST['h_video'];
        $ivideo_notice = $_REQUEST['h_ivideo'];
        $objIntranet->setInsertVideo($id_notice, $video_notice, $ivideo_notice, $id_user_modified);
        header('Location:../../../intranet.php?md=' . md5('admi_noticia') . '&fl=2');
        break;

    case 'D':
        $id_notice = $_REQUEST['id_notice'];
        $objIntranet->deleteNotice($id_notice);
        //header('Location:index.php');
        break;
}
?>