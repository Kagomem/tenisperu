<?php

ob_start();
session_start();
include_once('../../commons/intranet.class.php');
$accion = $_REQUEST['accion'];
/* echo $accion.'<br>'; */
$objIntranet = new intranet;
switch ($accion) {
    case 'I':
        $upload_result_file = '../../../../pdf/fichas_inscripcion/';
        $urlSI = $_REQUEST['urlSI'];
        if ($urlSI != 'on') {
            $file_result_ = $_FILES['file_sports_information']['name'];
            if ($file_result_ != '') {
                $file_result_up = 'TRUE';
                if ($_FILES['file_sports_information']['name'] != '') {
                    $ext_file_adj = type_archivo('file_sports_information', 'file');
                }
                if ($ext_file_adj == true) {
                    if ($_FILES['file_sports_information']['name'] != '' && $ext_file_adj == true) {
                        $file_result_up = subirArchivo('file_sports_information', $upload_result_file, 30000000);
                        $file_sports_information = $file_result_up;
                        $url_sports_information = '';
                    } else {
                        $file_result_up = 'FALSE';
                    }
                } else if ($ext_file_adj == false) {
                    echo "{success:false,msg:'La extenci�n del archivo debe ser .doc o .pdf.'}";
                }
            }
        } else {
            $url_sports_information = $_REQUEST['url'];
        }
        if (($file_result_up != 'FALSE') || ($urlSI == 'on')) {
            $title_sports_information = $_REQUEST['titulo'];
            $type_sports_information = $_REQUEST['tipo'];
            $date_sports_information = $_REQUEST['fecha'];
            $publication_star = $_REQUEST['publicacion_ini'];
            $publication_end = $_REQUEST['publicacion_fin'];
            $state_sports_information = $_REQUEST['estado'];
            $id_user_creator = $_SESSION['idusuario'];
            $id_sports_information = $objIntranet->setInsertSports_Information($title_sports_information, $type_sports_information, $url_sports_information, $date_sports_information, $publication_star, $publication_end, $state_sports_information, $id_user_creator, $file_sports_information);

            if ($id_sports_information == True) {
                header('Location:../../../intranet.php?md=' . md5('new_ficha') . '&fl=4&st=1');
            } else {
                header('Location:../../../intranet.php?md=' . md5('new_ficha') . '&fl=4&st=2');
            }
        } else {
            header('Location:../../../intranet.php?md=' . md5('new_ficha') . '&fl=4&st=2');
        }
        break;
    case 'U':
        $upload_result_file = '../../../../pdf/fichas_inscripcion/';
        $urlSI = $_REQUEST['urlSI'];
        if ($urlSI != 'on') {
            $file_result_ = $_FILES['file_sports_information']['name'];
            if ($file_result_ != '') {
                $file_result_up = 'TRUE';
                if ($_FILES['file_sports_information']['name'] != '') {
                    $ext_file_adj = type_archivo('file_sports_information', 'file');
                }
                if ($ext_file_adj == true) {
                    if ($_FILES['file_sports_information']['name'] != '' && $ext_file_adj == true) {
                        $file_result_up = subirArchivo('file_sports_information', $upload_result_file, 30000000);
                        $file_sports_information = $file_result_up;
                        $url_sports_information = '';
                        $file_sports_information_temp = $_REQUEST['file_sports_information_temp'];
                        if ($file_sports_information_temp != '') {
                            if (file_exists($upload_result_file . $file_sports_information_temp)) {
                                unlink($upload_result_file . $file_sports_information_temp);
                            }
                        }
                    } else {
                        $file_result_up = 'FALSE';
                    }
                } else if ($ext_file_adj == false) {
                    $file_result_up = 'FALSE';
                }
            } else {
                $file_sports_information = $_REQUEST['file_sports_information_temp'];
                $url_sports_information = '';
            }
        } else {
            $url_sports_information = trim($_REQUEST['url']);
            if ($url_sports_information != '') {
                $file_sports_information_temp = $_REQUEST['file_sports_information_temp'];
                if ($file_sports_information_temp != '') {
                    if (file_exists($upload_result_file . $file_sports_information_temp)) {
                        unlink($upload_result_file . $file_sports_information_temp);
                    }
                }
            }
        }
        if (($file_result_up != 'FALSE') || ($urlSI == 'on')) {
            $id_sports_information = $_REQUEST['id_aviso_ficha'];
            $title_sports_information = $_REQUEST['titulo'];
            $type_sports_information = $_REQUEST['tipo'];
            $date_sports_information = $_REQUEST['fecha'];
            $publication_star = $_REQUEST['publicacion_ini'];
            $publication_end = $_REQUEST['publicacion_fin'];
            $state_sports_information = $_REQUEST['estado'];
            $id_user_modified = $_SESSION['idusuario'];
            $objIntranet->setUpdateSports_Information($id_sports_information, $title_sports_information, $type_sports_information, $url_sports_information, $date_sports_information, $publication_star, $publication_end, $state_sports_information, $id_user_modified, $file_sports_information);
            header('Location:../../../intranet.php?md=' . md5('admi_ficha') . '&fl=4');
        } else {
            header('Location:../../../intranet.php?md=' . md5('error_carga_file') . '&fl=4&st=1');
        }
        break;
    case 'R': break;

    case 'D':
        $id_sports_information = $_REQUEST['id_sports_information'];
        echo $objIntranet->deleteSports_Information($id_sports_information);
        //header('Location:index.php');
        break;
}

function subirArchivo($input_file, $upload_file_direc, $size_file) {
    $vowels = array(" ", "/", "-", "[", "]", "'", ")", "(", "/\O");
    $archivo = $_FILES[$input_file]['name'];
    $archivo = str_replace($vowels, '', $archivo);
    $n_foto1_tmp = $_FILES[$input_file]['tmp_name'];
    $tipo_archivo = $_FILES[$input_file]['type'];
    $tamano_archivo = $_FILES[$input_file]['size'];
    if ($archivo != "") {
        $documento = rand(0, 1000000) . '_' . $archivo;
        $destino = $upload_file_direc . $documento;
        if (!($tamano_archivo < $size_file)) {
            return 'FALSE';
            exit;
            die();
        } else {
            if (move_uploaded_file($_FILES[$input_file]['tmp_name'], $destino)) {
                $archivoSubido = $documento;
                return $archivoSubido;
            } else {
                return 'FALSE';
            }
        }
    } else {
        return 'FALSE';
    }
}

function type_archivo($input_file, $type_file) {
    $tipo_archivo = $_FILES[$input_file]['type'];
    switch ($tipo_archivo) {
        case 'application/pdf':
            if ($type_file == 'file') {
                return true;
            } else {
                return false;
            }
            break;
        case 'application/msword':
            if ($type_file == 'file') {
                return true;
            } else {
                return false;
            }
            break;
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            if ($type_file == 'file') {
                return true;
            } else {
                return false;
            }
            break;
        case 'image/jpeg':
            if ($type_file == 'image') {
                return true;
            } else {
                return false;
            }
            break;
        case 'image/pjpeg':
            if ($type_file == 'image') {
                return true;
            } else {
                return false;
            }
            break;
        case 'image/png':
            if ($type_file == 'image') {
                return true;
            } else {
                return false;
            }
            break;
        default:
            return false;
            break;
    }
}

?>