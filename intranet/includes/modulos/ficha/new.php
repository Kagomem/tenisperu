<?php
ob_start();
session_start();
$accion = $_REQUEST['acc'];
$stadoForm = $_REQUEST['st'];
($accion == '') ? $accion = 'I' : $accion = 'U';
if ($accion == 'U') {
    $id_sports_information = $_REQUEST['cd'];
    include_once('includes/commons/intranet.class.php');
    $objIntranet = new intranet;
    $dataInfrDeport = $objIntranet->getListSports_Information('where d.id_sports_information=' . $id_sports_information);
    while ($data = mysql_fetch_array($dataInfrDeport)) {
        $title_sports_information = $data['1'];
        $type_sports_information = $data['2'];
        $url_sports_information = $data['3'];
        $file_sports_information_temp = $data['4'];
        $date_sports_information = $data['5'];
        $publication_star = $data['6'];
        $publication_end = $data['7'];
        $state_sports_information = $data['8'];
        // id     titulo        tipo     url  3     archivo     fecha     publ_start     publ_end     estado    
    }
}
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#valid_file').click(function() {
            var _val = $('#urlSI').attr('checked');
            if (_val == 'checked') {
                $('#dUrl').fadeIn();
                $('#dFile').hide();
            } else {
                $('#dFile').fadeIn();
                $('#dUrl').hide();
            }
        });
    });
    function relationFile(object) {
        if (object.checked) {
            $('#dUrl').fadeIn();
            $('#dFile').hide();
        } else {
            $('#dFile').fadeIn();
            $('#dUrl').hide();
        }
    }
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
        ],
        toolbar1: "bold italic underline | alignleft aligncenter alignright alignjustify |formatselect fontselect fontsizeselect | styleselect",
        toolbar2: "undo redo | link unlink | forecolor backcolor | print preview code ",
        toolbar3: "subscript superscript | charmap | spellchecker",
        toolbar_items_size: 'small',
        menubar: false,
        image_advtab: true,
        external_filemanager_path: "recursos/filemanager/",
        filemanager_title: "Subir archivos",
        external_plugins: {"filemanager": "../filemanager/plugin.min.js"}
    });
</script>

<?php if ($stadoForm != NULL && $stadoForm == 1) { ?>
    <script>
        $(document).ready(function() {
            $('#message').fadeIn('normal', function() {
                $(this).delay(1500).fadeOut('normal');
            });
        });

    </script>
    <div class="form-state" id="message" style="display:none">
        <div class="message exito">Registro Guardado</div>
    </div>
<?php } ?>
    <?php if ($stadoForm != NULL && $stadoForm == 2) { ?>
    <script>
        $(document).ready(function() {
            $('#message').fadeIn('normal', function() {
                $(this).delay(1500).fadeOut('normal');
            });
        });

    </script>
    <div class="form-state" id="message" style="display:none">
        <div class="message exito">Error!!!. No se pudo guardar el nuevo registro.</div>
    </div>
<?php } ?>
<div id="record" class="formNew">
    <form action="includes/modulos/ficha/ficha_acciones.php" 
          method="post" enctype="multipart/form-data" 
          name="formInfrDeport" id="formInfrDeport">
        <input type="hidden" id="accion" name="accion" value="<?php echo $accion ?>" />
        <input type="hidden" id="idusuario" name="idusuario" value="<?php echo $_SESSION['idusuario']; ?>" />
        <input type="hidden" id="id_aviso_ficha" name="id_aviso_ficha" value="<?php echo $id_sports_information; ?>" />
        <input type="hidden" id="file_sports_information_temp" name="file_sports_information_temp" value="<?php echo $file_sports_information_temp; ?>" />
        <input type="hidden" id="tipo" name="tipo" value="2" />
        <table width="96%" border="0" align="center" cellpadding="2" cellspacing="2" class="optionForm">
            <tr>
                <td width="7%">&nbsp;</td>
                <td colspan="3"><div class="intr-label">Titulo</div></td>
                <td width="8%">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3"><div align="left">
                        <textarea name="titulo" cols="40" rows="3" class="text-input corner-input-intr small-input" style="width: 100%" id="titulo"><?php echo $title_sports_information; ?></textarea>
                    </div></td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td><div class="intr-label">Fecha</div></td>
                <td><div class="intr-label">Relacionado a un </div></td>
                <td><div class="intr-checkbox" id="valid_file">
                        <input name="urlSI" type="checkbox" id="urlSI" class="disabled" onclick="relationFile(this)" <?php echo (empty($url_sports_information)) ? '' : 'checked'; ?> />
                        <label for="urlSI">Mejor con Link</label>
                    </div></td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td><div align="left">
                        <input name="fecha" type="text" class="text-input corner-input-intr small-input" id="fecha" value="<?php echo $date_sports_information; ?>" readonly="readonly" />
                    </div></td>
                <td colspan="2"><div align="left" id="dFile" style="<?php echo (empty($url_sports_information)) ? '' : 'display:none;'; ?>" >
                        <input type="file" name="file_sports_information" id="file_sports_information"  class="text-input corner-input-intr small-input" style="height:16px;">
                    </div>
                    <div id="dUrl" style="<?php echo (empty($url_sports_information)) ? 'display:none;' : ''; ?>">
                        <input type="text" name="url" id="url" class="text-input corner-input-intr small-input" value="<?php echo $accion == 'U' ? $url_sports_information : 'http://'; ?>"/></div></td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td width="27%"><div class="intr-label">Inicio de Publicaci&oacute;n</div></td>
                <td width="27%"><div class="intr-label">Fin de Publicaci&oacute;n</div></td>
                <td width="31%"><div class="intr-label">Estado</div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><div align="left">
                        <input name="publicacion_ini" type="text" class="text-input corner-input-intr small-input" id="publicacion_ini" value="<?php echo $publication_star; ?>" readonly="readonly" />
                    </div></td>
                <td><div align="left">
                        <input name="publicacion_fin" type="text" class="text-input corner-input-intr small-input" id="publicacion_fin" value="<?php echo $publication_end; ?>" readonly="readonly" />
                    </div></td>
                <td width="31%"><div align="left">
                        <div class="intr-checkbox">
                            <input name="estado" type="radio" id="activo" value="1" <?php echo $state_sports_information == 1 ? 'checked="checked"' : ''; ?> />
                            <label for="activo">Publicar</label>
                        </div>
                        <div class="intr-checkbox">
                            <input name="estado" type="radio" id="inactivo" value="0" <?php echo $state_sports_information == 0 ? 'checked="checked"' : ''; ?> />
                            <label for="inactivo">No Publicar</label>
                        </div>
                    </div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="middle">&nbsp;</td>
                <td colspan="3" align="center" valign="middle">
                    <div class="buttons" style="text-align:center">
                        <input name="Restablecer" type="reset" class="bootom cancel" value=" Cancelar ">
                        <input class="bootom save" type="submit" value=" Guardar ">
                    </div></td>
                <td align="center" valign="middle">&nbsp;</td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        $(".save").click(function(e) {
            e.preventDefault();
            var titulo = tinyMCE.get('titulo').getContent();
            var inicioPublicacion = $("input[name=publicacion_ini]").val();
            var finPublicacion = $("input[name=publicacion_fin]").val();


            if (titulo.trim().length < 1)
            {
                alert("Porfavor complete todos los campos");
            }
            else if (titulo.trim().length < 5)
            {
                alert("Escriba un titulo con un minimo de 5 caracteres");
            }
            else if (isNaN(titulo.trim()) == false)
            {
                alert("El titulo no puede ser un valor numerico!!!");
            }
            else if (finPublicacion < inicioPublicacion)
            {
                alert("Erro:La fecha de fin de la publicacion no puede ser una fecha menor que la fecha de inicio");
            }
            else
            {
                document.formInfrDeport.submit();
            }
        });

        $("#fecha").glDatePicker({
            onChange: function(target, newDate)
            {
                target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
            }
        });
        $("#publicacion_ini").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 10,
                    startDate: new Date(),
                    allowOld: false
                });
        $("#publicacion_fin").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 20,
                    showPrevNext: true,
                    allowOld: false,
                    startDate: new Date()
                });
    });
</script> 