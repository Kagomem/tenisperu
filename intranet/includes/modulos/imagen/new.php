<?php
ob_start();
session_start();
$accion = $_REQUEST['acc'];
($accion == '') ? $accion = 'I' : $accion = 'U';
?>
<style type="text/css">

    fieldset.insert-img {
        width: 360px;
        border: 1px solid #CDCDCD; padding: 8px; padding-bottom:0px; margin: 8px 0;
    }
    fieldset.list-gal {
        float:right;
        width: 285px;
        border: 1px solid #CDCDCD; padding: 8px; padding-bottom:0px; margin: 8px 0;
    }
    #formulario {
        display:inline-block;
    }
    #formulario,#gal {
        overflow:hidden;
    }
    #sampleFile {

        margin-right: 15px;
    }
    #download {
        margin-top: 15px;
        display: table;
    }
    .dlImage {
        display: table-cell;
        float: left;
        margin-right: 10px;
    }
    .dlText {
        float: left;
        display: table-cell;
    }
    .fileDetails {
        color: red;
    }
    .releaseDate{
        margin-top: -3px;
        color: gray;
    }
</style>
<div id="record" class="formNew">
    <form action="includes/modulos/logro/logro_acciones.php" method="post" enctype="multipart/form-data" name="formDiary" id="formDiary">
        <div>
            <input type="hidden" id="accion" name="accion" value="<?php echo $accion ?>" />
            <input type="hidden" id="idusuario" name="idusuario" value="<?php echo $_SESSION['idusuario']; ?>" />
            <input type="hidden" id="id_image" name="id_image" value="<?php echo $id_image; ?>" />
        </div>
        <div id="sampleFile">
<?php include_once('uploadify-jgrowl.php');
?>

        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#fecha").glDatePicker({
            onChange: function(target, newDate)
            {
                target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
            }
        });
        $("#publicacion_ini").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 10,
                    startDate: new Date(),
                    allowOld: false
                });
        $("#publicacion_fin").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 20,
                    showPrevNext: true,
                    allowOld: false,
                    startDate: new Date()
                });
    });
</script> 