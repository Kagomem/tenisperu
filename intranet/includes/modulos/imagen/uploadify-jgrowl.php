<?php
include_once('includes/commons/intranet.class.php');
?>
<link rel="stylesheet" href="css/uploadify.jGrowl.css" type="text/css">
<link rel="stylesheet" href="uploadify/uploadify.css" type="text/css">
<script type="text/javascript" src="js/jquery.uploadify-3.1.min.js"></script>
<script type="text/javascript" src="js/jquery.jgrowl_minimized.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
        
      $('#fileUploadgrowl').uploadify({
            'fileSizeLimit': '2048KB',
            'buttonText': 'Subir Im&aacute;genes...',
            'auto': true,
            'fileTypeExts': '*.gif; *.jpg; *.png',
            'swf': 'uploadify/uploadify.swf',
            'uploader': 'uploadify/uploadify.php',
            'onSelect': function(file) {
                $('#contro_file').fadeIn();
                $('.uploadify-queue .cancel').click(function() {
                    var item = $('.uploadify-queue').children().length;
                    if (item == 1) {
                        $('#contro_file').fadeOut();
                    }
                });
            },
            'onQueueComplete': function(queueData) {
                $("#fecha1").val('');
                var galeria = $('#select-galeria').val()
              
                var pathname = window.location.pathname;
              // $(location).attr('href', pathname + '?md=8cf36abc27f01637a365c83df0cb5bea&fl=3&gl=' + galeria);

            },
            'queueSizeLimit': 9999,
            'onUploadSuccess': function(file, data, response) {
                if (data != "false") {
                    var date = $("#fecha1").val();
                    guardarDatos_imagen(data);
                }
                else {
                         console.info("no hay data");
                }
            }
            // Your options here
        });
        
//        $("#fileUploadgrowl").uploadify({
//               'swf': 'uploadify/uploadify.swf',
//            'uploader': 'uploadify/upload.php',
//             'buttonText': 'Subir Imagenes...',
////            'cancelImg': 'uploadify/cancel.png',
////            'script': 'uploadify/upload_name.php',
//            /*'folder': 'includes/modulos/imagenes/files/',*/
////            'folder': '../img/galeria/',
////            'fileDesc': 'Image Files',
//            'fileExt': '*.jpg;*.jpeg;*.png;*.gif',
//            'multi': true,
//              'auto': true,
//            'simUploadLimit': 3,
//            'sizeLimit': 1572864,
//            onError: function(event, queueID, fileObj, errorObj) {
//                var msg;
//                if (errorObj.status == 404) {
////                    alert('Could not find upload script. Use a path relative to: ' + '<?= getcwd() ?>');
//                    msg = 'Could not find upload script.';
//                } else if (errorObj.type === "HTTP")
//                    msg = errorObj.type + ": " + errorObj.status;
//                else if (errorObj.type === "File Size")
//                    msg = fileObj.name + '<br>' + errorObj.type + ' Limit: ' + Math.round(errorObj.sizeLimit / 1024) + 'KB';
//                else
//                    msg = errorObj.type + ": " + errorObj.text;
//                $.jGrowl('<p></p>' + msg, {
//                    theme: 'error',
//                    header: 'ERROR',
//                    sticky: true
//                });
//                $("#fileUploadgrowl" + queueID).fadeOut(250, function() {
//                    $("#fileUploadgrowl" + queueID).remove()
//                });
//                return false;
//            },
//            onCancel: function(a, b, c, d) {
//                var msg = "Subida Cancelada: " + c.name;
//                $.jGrowl('<p></p>' + msg, {
//                    theme: 'warning',
//                    header: 'Upload Cancelada',
//                    life: 4000,
//                    sticky: false
//                });
//            },
//            onClearQueue: function(a, b) {
//                var msg = "Restableciendo " + b.fileCount + " files from queue";
//                $.jGrowl('<p></p>' + msg, {
//                    theme: 'warning',
//                    header: 'Lista Limpiada',
//                    life: 4000,
//                    sticky: false
//                });
//            },
//            onComplete: function(a, b, c, d, e) {
//                var size = Math.round(c.size / 1024);
//                $.jGrowl('<p></p>' + c.name + ' - ' + size + 'KB', {
//                    theme: 'success',
//                    header: 'Subida Completa',
//                    life: 4000,
//                    sticky: false
//                });
//                var jac = sonString(c.name);
//                console.info(c.name);
//                console.info('-' + jac);
//                guardarDatos_imagen(jac);
//                //substr_replace(' ','_',c.name)
//                //var nm= c.name;
//
//                //for(){}
//                //console.info(nm);
//            }
//        });
    });
</script>
<div style="margin:auto;width:70%">
    <h2><strong>Subir de Imagenes</strong></h2><br />
    <p>El Tamaño de las imagenes  debe ser menor de <strong>1.5 mb</strong>...</p><br />
    <p>El tamaño recomendado debe ser mayor a las Dimensiones de : 190*190 px y </p>
    <p>Es recomendado ser una imagen cuadrada</p><br />
    <div id="combo-gal"><?php include("galeria-combo.php"); ?></div>
    <br /> <br />
    <div id="fileUploadgrowl">Tiene Problemas con el Javascript</div><br />
    <div align="center">
        <a href="javascript:$('#fileUploadgrowl').fileUploadStart()">
            Subir Imagenes
        </a> 
        |  
        <a href="javascript:$('#fileUploadgrowl').fileUploadClearQueue()">Borrar Lista
        </a>| 
        <a href="javascript:void(0)">Ver Im&aacute;genes</a>
    </div>
</div>
