<?php

include_once('../../commons/intranet.class.php');
// JQuery File Upload Plugin v1.4.1 by RonnieSan - (C)2009 Ronnie Garcia
$objIntranet = new intranet;
$id_gallery = $_POST['idgaleria'];
$image = $_POST['image'];
$title_image = '';
;
$id_user_creator = $_POST['idusuario'];

$state_image = '1';
$date_image = '';
$publication_star = '';
$publication_end = '';
$direccionGrande = '../../../../img/galeria/';

function resizeImage($direccion, $imagen_final, $alto_final = 0, $ancho_final = 0) {
    list($ancho_original, $alto_original, $tipo_original, $alternativo) = getimagesize($direccion);
    if ($tipo_original == IMAGETYPE_GIF) {
        $img = imagecreatefromgif($direccion);
    }
    if ($tipo_original == IMAGETYPE_JPEG) {
        $img = imagecreatefromjpeg($direccion);
    }
    if ($tipo_original == IMAGETYPE_PNG) {
        $img = imagecreatefrompng($direccion);
    }

    $thumb = imagecreatetruecolor($ancho_final, $alto_final);
    imagecopyresampled($thumb, $img, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho_original, $alto_original);

    if ($tipo_original == IMAGETYPE_GIF) {
        imagegif($thumb, $imagen_final);
    }
    if ($tipo_original == IMAGETYPE_JPEG) {
        imagejpeg($thumb, $imagen_final, 100);
    }
    if ($tipo_original == IMAGETYPE_PNG) {
        imagepng($thumb, $imagen_final);
    }
    return true;
}

resizeImage($direccionGrande . $image, $direccionGrande . 'mini/' . $image, 72, 72);
$prueba = $objIntranet->setInsertImage
        ($id_gallery, $image, $title_image, $date_image, $publication_star, $publication_end, $state_image, $id_user_creator);

if ($prueba == True) {
    $objIntranet->setUpdateGalleryPhotoPrincipal($id_gallery, $image);
    echo $idusuario . '*' . $idgaleria . '**';
} else {
    echo "errrorrrr";
}
?>