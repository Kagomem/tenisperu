<?php
$Img = '<img src="img/png/image.png" alt="" border="0" height="24" width="24" align="right">';
?>
<script>
    $(document).ready(function() {
        $('.content-box tbody tr:even').addClass('alt-row');
    });
    function EliminarDato(id, url) {
        var msg = confirm("Desea eliminar este Registro")
        if (msg) {
            $.ajax({
                url: url,
                type: "POST",
                data: 'accion=D&id_sports_information=' + id,
                success: function(datos) {
                    //alert('registro -'+id+'- eliminado');
                    //ConsultaDatos('includes/modulos/aviso_ficha/');
                    if (datos == '1') {
                        $("#fila-" + id).fadeOut('slow', function() {
                            $(this).remove();
                        });
                        $('#messageSuccess').fadeIn('normal', function() {
                            $(this).delay(1500).fadeOut('normal');
                        });
                    } else {
                        $('#messageError').fadeIn('normal', function() {
                            $(this).delay(1500).fadeOut('normal');
                        });
                    }
                }
            });
        }
        return false;
    }
</script>
<div class="form-state" id="messageSuccess" style="display:none;position:absolute;left:20%;z-index:10;">
    <div class="message exito">Registro Borrado</div>
</div>
<div class="form-state" id="messageError" style="display:none;position:absolute;left:20%;z-index:10;">
    <div class="message exito">Su Operaci&oacute;n Fall&oacute;</div>
</div>
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Lista de Calendarios</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab current">Listado General</a></li>
            <li><a href="intranet.php?md=<?php echo md5('new_calendar') ?>&fl=<?php echo $_REQUEST['fl']; ?>">Escribir Nuevo</a></li>
        </ul>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this div's tab -->
            <form action="" method="post">
                <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="3%" align="left"><input class="check-all" type="checkbox"></th>
                            <th width="33%" align="left">TITULO</th>
                            <th width="12%" align="center" valign="middle">TIENE UN</th>
                            <th width="10%" align="center" valign="middle">FECHA</th>
                            <th width="11%" align="center" valign="middle">ESTADO</th>
                            <th width="20%" align="center" valign="middle">ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include_once('includes/commons/intranet.class.php');
                        $objData = new intranet;
                        $result = $objData->getListSports_Information(' where type_sports_information=3 ORDER BY date_register DESC,id_sports_information desc ');
                        ?>
                        <?php
                        $imgLink = '<span><img src="img/png/link-image.png" alt="" border="0"  height="20" width="20" align="left"></span>';
                        $imgFile = '<img src="img/png/file-image.png" alt="" border="0"  height="20" width="20" align="right">';
                        while ($dataRow = mysql_fetch_row($result)) {
                            ?>
                            <tr id="fila-<?php echo $dataRow[0]; ?>">
                                <td align="left"><input type="checkbox" name="urls[]" value="0"></td>
                                <td align="left">
                                    <?php echo $dataRow['1']; ?>
                                </td>
                                <td align="center" valign="middle">
                                    <?php
                                    if ($dataRow['4'] == '') {
                                        if ($dataRow['3'] == '' || $dataRow['3'] == 'http://') {
                                            echo "<a href='#'>";
                                            echo '<span style="color:blue">Enlace Vacio</span></a>';
                                            echo "</a>";
                                        } else {
                                            echo '<a href="' . $dataRow['3'] . '" target="_blank">' .
                                            $imgLink . '<span style="color:blue">Enlace</span></a>';
                                        }
                                    } else {
                                        echo $imgFile . '<span style="color:green">Archivo</span>';
                                    }
//                                    echo ($dataRow['4'] == '') ?
//                                            '<a href="' . $dataRow['3'] . '" target="_blank">' .
//                                            $imgLink . '<span style="color:blue">Enlace</span></a>' : $imgFile . '<span style="color:green">Archivo</span>';
//                                    
                                    ?>
                                </td>
                                <td align="center" valign="middle"><?php echo $dataRow['5']; ?></td>
                                <td align="center" valign="middle"><?php echo ($dataRow['8'] == 1) ? '<span style="color:green">Activo</span>' : '<span style="color:red">Inactivo</span>'; ?></td>
                                <td align="center" valign="middle"><!-- Icons -->
                                    <a href="intranet.php?md=<?php echo md5('new_calendar') . '&fl=' . $_REQUEST['fl'] . '&acc=U&cd=' . $dataRow['0']; ?>" title="Edit"><img  src="img/png/pencil.png" alt="Edit" border="0" ></a>
                                    <a href="javascript:EliminarDato(<?php echo $dataRow['0']; ?>,'includes/modulos/calendar/calendar_acciones.php')" onclick="<?php echo $dataRow['0']; ?>" title="Delete"><img src="img/png/cross.png" alt="Delete" border="0"></a>
                                </td>

                            </tr>
<?php } ?>



                    </tbody>
                    <tfoot>
                        <tr align="left">
                            <td colspan="9">
                                <div class="bulk-actions align-left">
                                    <select name="option" class="first">
                                        <option value="">Select action...</option>
                                        <option value="delete">Delete</option>
                                        <option value="move" class="nextSelect">Move to URL</option>
                                        <option value="copy" class="nextSelect">Copy to URL</option>
                                    </select>
                                    <select name="page" class="second">
                                        <option value="0">Default</option>
                                        <option value="">home page</option><option value="">--Pagina nueva</option>
                                        <option value="^http://dev.ikfsystems.sk/ikfa/prueba$">Prueba</option>											
                                    </select>
                                    <input type="submit" class="button" name="apply" value="Apply on selected">
                                </div>
                                <div class="clear"></div>
                            </td>
                        </tr>
                    </tfoot>										
                </table>
            </form>
        </div> <!-- End #tab1 -->
    </div> <!-- End .content-box-content -->			
</div>