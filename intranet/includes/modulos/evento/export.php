<?php
	

	include("class.excel.writer.php");
	include_once('../../commons/intranet.class.php');
	$objData=new intranet;
	$xls = new ExcelWriter();
if(isset($_POST['cod_event_export_athlete'])){
	//datos para crear el execl
	$name_xls="Listado-atletas";
	$download=true;

	$event=$_POST['cod_event_export_athlete'];
	//exporta los participantes por evento
	//if($event==0){
	//	$result=$objData->getAthlete(' WHERE deleted !=1 ORDER BY dateregister DESC ');
	//}else{
		$result=$objData->getAthlete(' WHERE deleted !=1 AND id_event='.$event.' ORDER BY dateregister DESC ');
		$result_event=$objData->getListEvent(' WHERE deleted !=1 AND id_event='.$event.' ORDER BY dateregister DESC ');
	//}
	$name_eventt = array();
	$a_affiliate_code=array();
	$a_document_number=array();
	$a_name_person=array();
	$a_surname_paternal=array();
	$a_surname_maternal=array();
	$a_date_dirth=array();
	$a_email=array();
	$a_mobile=array();
	$a_name_father= array();
	$a_email_father= array();
	$a_phone_father= array();
	$a_number_operation= array();
	$a_gender= array();
	$a_state=array();
	while($rows_e = mysql_fetch_row($result_event)){
		array_push($name_eventt, utf8_encode($rows_e[1]));
	}
	while ($rows=mysql_fetch_row($result)) {
		array_push($a_affiliate_code,$rows[2]);
		array_push($a_document_number,$rows[8]);
		array_push($a_name_person,$rows[3]);
		array_push($a_surname_paternal,$rows[4]);
		array_push($a_surname_maternal,$rows[5]);
		array_push($a_date_dirth,$rows[7]);
		array_push($a_email,$rows[9]);
		array_push($a_mobile,$rows[16]);
		array_push($a_name_father, $rows[17]);
		array_push($a_email_father, $rows[18]);
		array_push($a_phone_father, $rows[19]);
		array_push($a_number_operation, $rows[10]);
		array_push($a_gender, $rows[6]);
		array_push($a_state,$rows[20]);
	}
	//evento
	$xls->OpenRow();
	$xls->NewCell("",true,array());
	$xls->CloseRow();
	$n = utf8_encode($name_eventt[0]);
	//Titulo de evento
	$xls->OpenRow();
	$xls->NewCell($n,false,array('color'=>'C00C2D','bold'=>true));
	$xls->CloseRow();

	$xls->OpenRow();
	$xls->NewCell("",true,array());
	$xls->CloseRow();
	//creamos la cabecera del archivo excel
	$cabecera = array('#','Cod Afilicacion','Documento','Nombre','Apellido Paterno','Apellido materno','Fech Nacimiento','Genero','Email','telefono','Nombre de Apoderado','Email de Apoderado','Telefono de Apoderado','Numero de Operacion','Estado');
	$xls->OpenRow();
	foreach($cabecera as $rows)	{
		$xls->NewCell($rows,false,array('align'=>'center','background'=>'666666','color'=>'FFFFFF','bold'=>true,'border'=>'000000'));
	}
	$xls->CloseRow();
	for ($i=0; $i <count($a_affiliate_code) ; $i++) { 
		$gender = ($a_gender[$i]=='M') ? 'Masculino' : 'Femenino' ;
		$ajax_date = explode("-", $a_date_dirth[$i]);
		$date = $ajax_date['2'].'/'.$ajax_date['1'].'/'.$ajax_date['0'];
		$xls->OpenRow();
			$xls->NewCell($i+1,false,array('align'=>'center','width'=>'30','type'=>'int','border'=>'000000'));
			$xls->NewCell($a_affiliate_code[$i],false,array('width'=>'60','border'=>'000000')); //Auto alineado
			$xls->NewCell($a_document_number[$i],false,array('align'=>'center','width'=>'70','border'=>'000000'));
			$xls->NewCell(utf8_encode($a_name_person[$i]),false,array('width'=>'80','border'=>'000000'));
			$xls->NewCell(utf8_encode($a_surname_paternal[$i]),false,array('width'=>'80','border'=>'000000'));
			$xls->NewCell(utf8_encode($a_surname_maternal[$i]),false,array('width'=>'80','border'=>'000000')); //Auto alineado
			$xls->NewCell($date,false,array('align'=>'center','width'=>'80','border'=>'000000'));
			$xls->NewCell($gender,false,array('align'=>'center','width'=>'80','border'=>'000000'));
			$xls->NewCell($a_email[$i],false,array('width'=>'250','border'=>'000000'));
			$xls->NewCell($a_mobile[$i],false,array('align'=>'center','width'=>'70','border'=>'000000'));
			$xls->NewCell(utf8_encode($a_name_father[$i]),false,array('align'=>'center','width'=>'120','border'=>'000000'));
			$xls->NewCell($a_email_father[$i],false,array('align'=>'center','width'=>'200','border'=>'000000'));
			$xls->NewCell($a_phone_father[$i],false,array('align'=>'center','width'=>'120','border'=>'000000'));
			$xls->NewCell($a_number_operation[$i],false,array('align'=>'center','width'=>'120','border'=>'000000'));
			$xls->NewCell($a_state[$i]==1?'Activo':'Inactivo',false,array('align'=>'center','width'=>'70','color'=>$a_state[$i]==1?'058E00':'E40000','border'=>'000000'));
		$xls->CloseRow();
	}

	$xls->GetXLS($download,$name_xls);
}else{
	//datos para crear el execl
	$name_xls="eventos";
	$download=true;
//exportar los eventos existentes
	$result=$objData->getListEvent(' WHERE deleted !=1  ORDER BY dateregister DESC ');
	$a_name_event=array();
	$a_start_date=array();
	$a_ending_date=array();
	$a_state=array();
	$count_event=0;
	while ($name_event=mysql_fetch_assoc($result)) {
		array_push($a_name_event,$name_event['name_event']);
		array_push($a_start_date,$name_event['date_start']);
		array_push($a_ending_date,$name_event['date_end']);
		array_push($a_state,$name_event['active']);
		$count_event++;
	}

	//creamos la cabecera del archivo excel
	$cabecera = array('#','Nombre Del Evento','Fecha Inicio','Fecha Fin','Estado');
	$xls->OpenRow();
	foreach($cabecera as $rows)	{
		$xls->NewCell($rows,false,array('align'=>'center','background'=>'666666','color'=>'FFFFFF','bold'=>true,'border'=>'000000'));
	}
	$xls->CloseRow();
	// creamos el cuerpo del archivo excel
	for($i=0;$i<$count_event;$i++){
		$xls->OpenRow();
			$xls->NewCell($i+1,false,array('align'=>'center','width'=>'30','type'=>'int','border'=>'000000'));
			$xls->NewCell(utf8_encode($a_name_event[$i]),false,array('width'=>'250','border'=>'000000')); //Auto alineado
			$xls->NewCell($a_start_date[$i],false,array('align'=>'center','width'=>'60','border'=>'000000'));
			$xls->NewCell($a_ending_date[$i],false,array('align'=>'center','width'=>'60','border'=>'000000'));
			$xls->NewCell($a_state[$i]==1?'Activo':'Inactivo',false,array('align'=>'center','width'=>'70','color'=>$a_state[$i]==1?'058E00':'E40000','border'=>'000000'));
		$xls->CloseRow();
	}
	
	$xls->GetXLS($download,$name_xls);
}
?>