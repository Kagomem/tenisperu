<?php 

$Img='<img src="img/png/image.png" alt="" border="0" height="24" width="24" align="right">';

?>
<style>
.dxe3sfaex{
	background-image: url(img/png/1325882758_star.png);
	background-repeat: no-repeat;
	background-position: center top;
display: block;
width: 16px;
height: 16px;
cursor:pointer;
}
.dxe3sfaexX{
	background-image: url(img/png/1325882758_star.png);
	background-repeat: no-repeat;
	background-position: center -16px;
display: block;
width: 16px;
height: 16px;
cursor:pointer;
}
.boton_excel_event_athlete{
  cursor: pointer;
  margin: 4px;
  position: relative;
  
  top: 9px;
}
.boton_excel_event_athlete:hover{
  cursor: pointer;
  text-decoration: underline;
}
#popup_voucher{
  display: none;
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(5,5,5,.5);
}
</style>
<script>
$(document).ready(function(){
	$('.content-box tbody tr:even').addClass('alt-row');
  $(document).on('click','img#img_voucher',function(){
    var src = $(this).attr('src');
    $('#popup_voucher').html('<img src="'+src+'"/>');
    $('#popup_voucher').css({'display':'block','z-index':'2'});
    $('#popup_voucher img').css({'width':'500px', 'margin-top': '200px','margin-left':'400px'});
  });
  $(document).on('click','#popup_voucher',function(){
    $(this).fadeOut();
  });
  //exportar a excel
  $(".boton_excel_event_athlete").click(function(event) {
      var cod=$("#form_export_event_athlete #cod_event_export_athlete").val();
      if(cod!="0"){
      $("#form_export_event_athlete").submit();
      }else{
        alert("Seleccione un evento");
      }
  });

  getAthlete_event();
 });
function EliminarDato(id,url,user){
	var msg = confirm("Desea eliminar este Registro"+user);
	if ( msg ) {
	$.ajax({
		url: url,
		type: "POST",
		data: 'accion=D&id_event='+id+'&idusuario='+user,
		success: function(datos){
			$("#fila-"+id).fadeOut('slow',function(){$(this).remove();});
			}
	});
	}
	return false;
}
function getAthlete_event(){
     $("#select_event").change(function(){
     	var event=$("#select_event").val();
      if(event!="0"){
       	$.ajax({
      		url:'includes/modulos/evento/event_ajax.php',
      		data:'pEvent='+event,
      		type:'post',
      		dataType:'json',
      		beforeSend: function(){
      		},
      		success:function(data){
  				var html="";
          var class_even="";

  				$.each(data,function(a,b){
              if (b['foto'] == ""||b['foto'] == null) {
                  b['foto'] = "no_photo.png";
              }
    					var date=b['date_birth'].split("-");
    					html=html+'<tr  id="fila-'+b['id_person']+'>';
                html=html+'<td  align="center" ></td>';
    						html=html+'<td align="center" >'+b['affiliate_code']+'</td>';
    						html=html+'<td  align="center" >'+b['document_number']+'</td>';
    						html=html+'<td  align="left" >'+b['name_person']+' '+b['surname_paternal']+' '+b['surname_maternal']+'</td>';
    						html=html+'<td>'+date[2]+'/'+date[1]+'/'+date[0]+'</td>';
    						html=html+'<td align="center" >'+b['email']+'</td>';
    						html=html+'<td align="center" ><img src="../img/inscription/person/'+b['foto']+'" width="50"/></td>';
                html=html+'<td align="center" ><img src="../img/inscription/voucher/'+b['voucher']+'" width="50" id="img_voucher"/></td>';
    						if(b['active']==1){
    							html=html+'<td  align="center" valign="middle" ><span style="color:green">Activo</span></td>';
    						}else{
    							if(b['active']==0){
    								html=html+'<td  align="center" valign="middle" ><span style="color:red">Inactivo</span></td>';	
    							}
    						}
    					html=html+"</tr>";
    				});
    				$("#tbl_event tbody").html(html);
            $("#form_export_event_athlete #cod_event_export_athlete").val(event);
            $('.content-box tbody tr:even').addClass('alt-row');
            
      		},
      		error:function(data){
  				console.log(":(");
  			  }	
      	});
      }else{
        var html="<tr class='alt-row'>";
              html=html+"<td colspan='7' align='center'>";
                html=html+"<span><b>Seleccione un evento</b></span>";
              html=html+"</td>";
            html=html+"</tr>";  
        $("#tbl_event tbody").html(html);
        $("#form_export_event_athlete #cod_event_export_athlete").val("0");
      }
  	});
}
</script>
<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
        <h3>Listado de Deportistas</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab current">Listado de Deportistas</a></li>
            <li>
            <form action="includes/modulos/evento/export.php" id="form_export_event_athlete" method="POST">
                <input type="hidden" name="cod_event_export_athlete" id="cod_event_export_athlete" value="0">
              <label style="cursor:pointer;" for="" class="boton_excel_event_athlete">Exportar</label>
              </form>
            </li>
        </ul>
        <div class="clear"></div>
  </div> <!-- End .content-box-header -->
        <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this div's tab -->
            <form action="" method="post">
            <table class="content-box" id="tbl_event" border="0" align="center" cellpadding="0" cellspacing="0">
            <thead>
            	<?php include_once('includes/commons/intranet.class.php');
				$objData=new intranet; 
				$result=$objData->getListEvent(' WHERE deleted !=1  ORDER BY dateregister DESC ');
				?>
            	<tr>
            		<td colspan="7" align="center" rowspan="" headers="">
                <label><b>Evento: </b>  </label>
                <select name="select_event" id="select_event">
        					<option value="0">-- Seleccione un evento --</option>}
        					<?php while($dataRow=mysql_fetch_row($result)){ ?>
        					<option value="<?php echo $dataRow['0'] ?>"><?php echo utf8_encode($dataRow['1']); ?></option>
        					<?php } ?>
        				</select>
            		</td>
            	</tr>
                <tr>
                    <th width="16%" align="center" valign="middle">COD. AFILIACI&Oacute;N</th>
                    <th width="10%" align="center" valign="middle">DNI</th>
                    <th width="20%" align="left">DEPORTISTA</th>
                    <th width="11%" align="left" valign="middle">FECHA NACIMIENTO</th>
                    <th width="15%" align="center" valign="middle">EMAIL</th>
                    <th width="11%" align="center" valign="middle">FOTO</th>
                    <th width="11%" align="center" valign="middle">VOUCHER</th>
                    <th width="18%" align="center" valign="middle">ESTADO</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="7" align="center">
                    <span><b>Seleccione un evento</b> </span>
                </td>
              </tr>
            </tbody>										
            </table>
            </form>
        </div> <!-- End #tab1 -->
	</div> <!-- End .content-box-content -->			
  
</div>