<?php
ob_start();
session_start();
$accion = $_REQUEST['acc'];
$stadoForm = $_REQUEST['st'];
($accion == '') ? $accion = 'I' : $accion = 'U';
if ($accion == 'U') {
    $id_event_information = $_REQUEST['cd'];
    include_once('includes/commons/intranet.class.php');
    $objIntranet = new intranet;
    $dataInfrDeport = $objIntranet->getListevent_Information('where d.id_event=' . $id_event_information);
    while ($data = mysql_fetch_array($dataInfrDeport)) {
        $id_event = $data['0'];
        $name_event = $data['1'];
        $date_start = $data['2'];
        $date_end = $data['3'];
        $active = $data['4'];
        $amount = $data['7'];
        $description = $data['8'];
        $place = $data['9'];
        $currency = $data['10'];
        $phone = $data['11'];
        $email = $data['12'];
        $type_pay = $data['13'];
        $name_pay = $data['14'];
        $type_acc = $data['15'];
    }
}
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#valid_file').click(function() {
            var _val = $('#urlSI').attr('checked');
            if (_val == 'checked') {
                $('#dUrl').fadeIn();
                $('#dFile').hide();
            } else {
                $('#dFile').fadeIn();
                $('#dUrl').hide();
            }
        });

        $("input[name='type_pay']").change(function() {
            if ($("input[name='type_pay']:checked").val() == 'P') {
                $(".txt_paying").css("display", 'block');
            }
            else if ($("input[name='type_pay']:checked").val() == 'B') {
                $(".txt_paying").css("display", 'none');
                $("input[name='name_paying']").val('');
            }
        });

        if ($("input[name='type_pay']:checked").val() == 'P') {
            $(".txt_paying").css("display", 'block');
        }
        else if ($("input[name='type_pay']:checked").val() == 'B') {
            $(".txt_paying").css("display", 'none');
        }

    });
    function relationFile(object) {
        if (object.checked) {
            $('#dUrl').fadeIn();
            $('#dFile').hide();
        } else {
            $('#dFile').fadeIn();
            $('#dUrl').hide();
        }
    }
</script>

<?php if ($stadoForm != NULL && $stadoForm == 1) { ?>
    <script>
        $(document).ready(function() {
            $('#message').fadeIn('normal', function() {
                $(this).delay(1500).fadeOut('normal');
            });
        });

    </script>
    <div class="form-state" id="message" style="display:none">
        <div class="message exito">Registro Guardado</div>
    </div>
<?php } ?>
<div id="record" class="formNew">
    <form action="includes/modulos/evento/evento_acciones.php" method="post" enctype="multipart/form-data" name="formInfrDeport" id="formEvent">
        <input type="hidden" id="accion" name="accion" value="<?php echo $accion ?>" />
        <input type="hidden" id="idusuario" name="idusuario" value="<?php echo $_SESSION['idusuario']; ?>" />
        <input type="hidden" id="id_event" name="id_event" value="<?php echo $id_event; ?>" />
        <table width="96%" border="0" align="center" cellpadding="2" cellspacing="2" class="optionForm">
            <tr >
                <td>&nbsp;</td>
                <td width="27%"><div class="intr-label">Tipo de Acceso</div></td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td width="27%">
                    <div align="left">
                        <div class="intr-checkbox">
                            <input name="type_acc" type="radio" id="type_acc_cod" value="C" <?php echo (isset($type_acc)) ? ($type_acc == 'C') ? 'checked' : ''  : ''; ?>/>
                            <label for="type_acc_cod">Codigo de Afiliacion</label>
                        </div>
                        <div class="intr-checkbox">
                            <input name="type_acc" type="radio" id="type_acc_dni" value="D" <?php echo (isset($type_acc)) ? ($type_acc == 'D') ? 'checked' : ''  : ''; ?> />
                            <label for="type_acc_dni">DNI</label>
                        </div>
                    </div>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td><div class="intr-label">Nombre del Evento</div></td>
                <!--td><div class="intr-label">Fecha</div></td-->
                <td width="27%"><div class="intr-label">Inicio de Inscripciones</div></td>
                <td width="27%"><div class="intr-label">Fin de Inscripcione</div></td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td>
                    <div align="left">
                        <input name="name_event" type="text" class="text-input corner-input-intr small-input" id="name_event" value="<?php echo utf8_encode($name_event); ?>" autocomplete="off" />
                    </div>
                </td>
                <td>
                    <div align="left">
                        <input name="date_start" type="text" class="text-input corner-input-intr small-input" id="date_start" value="<?php echo $date_start; ?>" readonly="readonly" />
                    </div>
                </td>
                <td>
                    <div align="left">
                        <input name="date_end" type="text" class="text-input corner-input-intr small-input" id="date_end" value="<?php echo $date_end; ?>" readonly="readonly" />
                    </div>
                </td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td><div class="intr-label">Tipo de Pago</div></td>
                <td><div class="intr-label txt_paying" >Lugar de Pago</div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td width="31%">
                    <div align="left">
                        <div class="intr-checkbox">
                            <input name="type_pay" type="radio" id="type_pay" value="P" <?php echo (isset($type_pay)) ? ($type_pay == 'P') ? 'checked' : ''  : ''; ?>/>
                            <label for="type_pay">Presencial</label>
                        </div>
                        <div class="intr-checkbox">
                            <input name="type_pay" type="radio" id="type_pay_ban" value="B" <?php echo (isset($type_pay)) ? ($type_pay == 'B') ? 'checked' : ''  : ''; ?> />
                            <label for="type_pay_ban">Pago a Banco</label>
                        </div>
                    </div>
                </td>
                <td>
                    <div align="left" class="txt_paying">
                        <input name="name_paying" type="text" class="text-input corner-input-intr small-input" id="name_pay" value="<?php echo utf8_encode($name_pay); ?>" autocomplete="off" />
                    </div>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td width="31%"><div class="intr-label">Lugar</div></td>
                <td><div class="intr-label" >Monto</div></td>
                <td><div class="intr-label" >Estado</div></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td width="31%">
                    <div align="left">
                        <input name="place" type="text" class="text-input corner-input-intr small-input" id="place" value="<?php echo utf8_encode($place); ?>" autocomplete="off" />
                    </div>
                </td>
                <td> 
                    <div align="left">
                        <input name="amount" type="text" class="text-input corner-input-intr small-input" id="amount" value="<?php echo $amount; ?>" autocomplete="off" />
                    </div>
                </td>
                <td>
                    <div align="left">
                        <select name="currency" id="currency">
                            <?php
                            if ($currency == 'S') {
                                ?>
                                <option selected="" value="S">Soles</option>
                                <option  value="D">D&oacute;lares</option>
                                <?php
                            } else {
                                ?>
                                <option selected="" value="D">D&oacute;lares</option>
                                <option  value="S">Soles</option>
                            <?php }
                            ?>
<!--                            <option <?php ($currency == 'S') ? "selected=selected" : '' ?> value="S">Soles</option>
         <option <?php ($currency == 'D') ? "selected=selected" : '' ?> value="D">D&oacute;lares</option>-->
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td width="31%"><div class="intr-label">Estado</div></td>
                <td><div class="intr-label" >Base</div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td width="31%">
                    <div align="left">
                        <div class="intr-checkbox">
                            <input name="estado" type="radio" id="activo" value="1" <?php echo (isset($active) == 1) ? 'checked="checked"' : 'checked="checked"'; ?> />
                            <label for="activo">Publicar</label>
                        </div>
                        <div class="intr-checkbox">
                            <input name="estado" type="radio" id="inactivo" value="0" <?php echo (isset($active) == 0) ? 'checked="checked"' : ''; ?> />
                            <label for="inactivo">No Publicar</label>
                        </div>
                    </div>
                </td>
                <td><input type="file" name="file_base" value="" placeholder="Archivo PDF" accept=".pdf" /> </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td width="31%"><div class="intr-label">Telefono</div></td>
                <td><div class="intr-label" >Correo Electronico</div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input name="phone" type="text" class="text-input corner-input-intr small-input" autocomplete="off" id="phone" value="<?php echo $phone; ?>"></td>
                <td><input name="email" type="text" class="text-input corner-input-intr small-input" autocomplete="off" id="email" value="<?php echo $email; ?>"></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td width="31%"><div class="intr-label">Descripción</div></td>
                <td><div class="intr-label" ></div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3">
                    <textarea cols="100" rows="5" style="height:50px" id="description" name="description"><?php echo $description ?></textarea>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td width="31%">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="middle">&nbsp;</td>
                <td colspan="3" align="center" valign="middle">
                    <div class="buttons" style="text-align:center">
                        <input name="Restablecer" type="reset" class="bootom cancel" value=" Cancelar ">
                        <input class="bootom save" type="submit" value=" Guardar " id="save">
                    </div></td>
                <td align="center" valign="middle">&nbsp;</td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#fecha").glDatePicker({
            onChange: function(target, newDate)
            {
                target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
            }
        });
        $("#date_start").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    /*endDate: 10,*/
                    startDate: new Date(),
                    allowOld: false
                });
        $("#date_end").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    /*endDate: 30,*/
                    showPrevNext: true,
                    allowOld: false,
                    startDate: new Date()
                });
    });
</script>
<script type="text/javascript">
    $('#formEvent #save').click(function(e) {
        e.preventDefault();
        var name_event = $("#name_event").val();
        var date_start = $("#date_start").val();
        var date_end = $("#date_end").val();
        var place = $("#place").val();
        var descripcion = $("#description").val();
        var upload = $("input[name='file_base']").val();
        var accion = $("input[name='accion']").val();
        if (name_event != " " && date_start != " " && date_end != " " && upload != " " && place != " " && descripcion != " ") {
            if (name_event != "" && date_start != "" && date_end != "" && place != "" && descripcion != "") {
                // Insertar
                if (accion == "I") {
                    if (date_end < date_start)
                    {
                        alert("La fecha del cierre de Inscripciones no puede ser menor al inicio de Inscripciones");
                    }
                
                    else if (upload != "") {
                        ext = upload.split(".");
                        if (ext[1] == "pdf") {
                            $("#formEvent").submit();
                        } else {
                            alert("Solo se pueden guardar archivos PDF");
                        }
                    } else {
                        alert("Todo los campos tienes que estar llenos, a excepcion de monto");
                    }
                }
                //Editar
                if (accion == "U") {
                    if (upload != "") {
                        ext = upload.split(".");
                        if (ext[1] == "pdf" || ext[1] == "PDF") {
                            $("#formEvent").submit();
                        } else {
                            alert("Solo se pueden guardar archivos PDF");
                        }
                    }
                    else {
                        $("#formEvent").submit();
                    }
                }
            }
            else {
                alert("Todo los campos tienes que estar llenos, a excepcion de monto");
            }
        }
        else {
            alert("Todo los campos tienes que estar llenos, a excepcion de monto");
        }
    });
</script> 

