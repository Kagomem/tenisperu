<?php
$Img = '<img src="img/png/image.png" alt="" border="0" height="24" width="24" align="right">';
$stadoForm = $_REQUEST['st'];
?>
<style>
    .dxe3sfaex{
        background-image: url(img/png/1325882758_star.png);
        background-repeat: no-repeat;
        background-position: center top;
        display: block;
        width: 16px;
        height: 16px;
        cursor:pointer;
    }
    .dxe3sfaexX{
        background-image: url(img/png/1325882758_star.png);
        background-repeat: no-repeat;
        background-position: center -16px;
        display: block;
        width: 16px;
        height: 16px;
        cursor:pointer;
    }
</style>
<script>
    $(document).ready(function() {
        $('.content-box tbody tr:even').addClass('alt-row');

        //exportar a excel
        $(".boton_excel").click(function(event) {
            $("#form_export_event").submit();
        });
    });
    function EliminarDato(id, url, user) {
        var msg = confirm("Desea eliminar este Registro");
        if (msg) {
            $.ajax({
                url: url,
                type: "POST",
                data: 'accion=D&id_event=' + id + '&idusuario=' + user,
                success: function(datos) {
                    //alert('registro -'+id+'- eliminado');
                    //ConsultaDatos('includes/modulos/avisos/');
                    $("#fila-" + id).fadeOut('slow', function() {
                        $(this).remove();
                    });
                }
            });
        }
        return false;
    }
</script>
<div class="content-box"><!-- Start Content Box -->
     <?php if ($stadoForm != NULL && $stadoForm == 1) { ?>
        <script>
            $(document).ready(function() {
                $('#message').fadeIn('normal', function() {
                    $(this).delay(1500).fadeOut('normal');
                });
            });
        </script>
        <div class="form-state" id="message" style="display:none">
            <div class="message exito">Registro Guardado</div>
        </div>
    <?php } ?>
    <?php if ($stadoForm != NULL && $stadoForm == 2) { ?>
        <script>
            $(document).ready(function() {
                $('#message').fadeIn('normal', function() {
                    $(this).delay(1500).fadeOut('normal');
                });
            });
        </script>
        <div class="form-state" id="message" style="display:none">
            <div class="message exito">Error!!!. No se pudo guardar el nuevo registro.</div>
        </div>
    <?php } ?>
          <?php if ($stadoForm != NULL && $stadoForm == 4) { ?>
        <script>
            $(document).ready(function() {
                $('#message').fadeIn('normal', function() {
                    $(this).delay(1500).fadeOut('normal');
                });
            });
        </script>
        <div class="form-state" id="message" style="display:none">
            <div class="message exito">Error!!!. No se pudo modificar el registro Evento duplicado.</div>
        </div>
    <?php } ?>
    <div class="content-box-header">
        <h3>Listado de Eventos</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab current">Listado de Eventos</a></li>
            <li><a href="intranet.php?md=<?php echo md5('new_event') ?>&fl=<?php echo $_REQUEST['fl']; ?>">Registrar Nuevo Evento</a></li>
            <li>
                <a href="#" title="">
                    <form action="includes/modulos/evento/export.php" id="form_export_event" method="POST">
                        <label for="" class="boton_excel">Exportar</label>
                    </form>
                </a>
            </li>
        </ul>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this divs tab -->
            <form action="" method="post">
                <table id="tbl_event" border="0" align="center" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="4%" align="left">
<!--                                <input class="check-all" type="checkbox">-->
                            </th>
                            <th width="46%" align="left">NOMBRE DEL EVENTO</th>
                            <th width="20%" align="center">BASES</th>
                            <th width="11%" align="center" valign="middle">INICIO DE INSCRIPCIONES</th>
                            <th width="11%" align="center" valign="middle">FIN DE INSCRIPCIONES</th>
                            <th width="10%" align="center" valign="middle">ESTADO</th>
                            <th width="18%" align="center" valign="middle">ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
include_once('includes/commons/intranet.class.php');
$objData = new intranet;
$result = $objData->getListEvent(' WHERE deleted !=1  ORDER BY dateregister DESC ');
?>
                        <?php while ($dataRow = mysql_fetch_row($result)) {
                            ?>
                            <tr id="fila-<?php echo $dataRow[0]; ?>">

                            <td align="left"><!--<input type="checkbox" name="urls[]" value="0">-->
                        &nbsp;<!--span class="dxe3sfaex<?php echo ($dataRow['14'] == 1) ? '' : 'X'; ?>" title="Noticia Destacada"></span-->                    </td>
                                <td align="left">
                                    <?php
                                    echo utf8_encode($dataRow['1']);
                                    ?>
                                </td>
                                <td align="center">
                                    <a href="../pdf/bases_torneo/<?php echo $dataRow['6']; ?>" target="_blank">Mostrar Bases</a>
                                </td>
                                <td align="center">
                                    <?php echo $dataRow['3']; ?>
                                </td>
                                <td>
                                    <?php $date_end = explode(" ", $dataRow['4']);
                                    echo $date_end[0];
                                    ?>
                                </td>
                                <td align="center" valign="middle"><?php echo ($dataRow['5'] == 1) ? '<span style="color:green">Activo</span>' : '<span style="color:red">Inactivo</span>'; ?></td>
                                <td align="center" valign="middle"><!-- Icons -->
                                    <a href="intranet.php?md=<?php echo md5('new_event') . '&fl=' . $_REQUEST['fl'] . '&acc=U&cd=' . $dataRow['0']; ?>" title="Edit"><img  src="img/png/pencil.png" alt="Edit" border="0" ></a>
                                    <a href="javascript:EliminarDato(<?php echo $dataRow['0']; ?>,'includes/modulos/evento/evento_acciones.php?accion=D',<?php echo $_SESSION['idusuario']; ?>)" onclick="<?php echo $dataRow['0']; ?>" title="Delete"><img src="img/png/cross.png" alt="Delete" border="0"></a>
                                </td>

                            </tr>
                        <?php } ?>



                    </tbody>										
                </table>
            </form>
        </div> <!-- End #tab1 -->
    </div> <!-- End .content-box-content -->			
</div>