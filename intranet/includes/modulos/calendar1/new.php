<?php ob_start(); session_start(); 
$accion=$_REQUEST['acc'];
($accion=='')?$accion='I':$accion='U';

if($accion=='U'){
	$id_link=$_REQUEST['cd'];
	include_once('includes/commons/intranet.class.php');
	$objIntranet=new intranet;
	$dataLink=$objIntranet->getListLink('where id_link='.$id_link);
	while($data=mysql_fetch_array($dataLink)){
	  $title_link=$data['1'];
	  $image_link=$data['2'];
	  $type_link=$data['3'];
	  $url_link=$data['4'];
	  $date_link=$data['5'];
	  $publication_star=$data['8'];
	  $publication_end=$data['9'];
	  $state_link=$data['10'];
	}
}?>
<div id="record" class="formNew">
  <form action="includes/modulos/enlace/enlace_acciones.php" method="post" enctype="multipart/form-data" name="formEnlace" id="formEnlace">
<input type="hidden" id="accion" name="accion" value="<?php echo $accion ?>" />
<input type="hidden" id="idusuario" name="idusuario" value="<?php echo $_SESSION['idusuario']; ?>" />
<input type="hidden" id="id_link" name="id_link" value="<?php echo $id_link; ?>" />
<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="optionForm">
  <tr>
    <td width="7%">&nbsp;</td>
    <td colspan="2"><div class="intr-label">Titulo</div></td>
    <td><div class="intr-label">Imagen</div></td>
    <td width="8%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><div align="left">
      <input type="text" name="titulo" id="titulo" class="text-input corner-input-intr small-input" value="<?php echo $title_link; ?>" />
    </div></td>
    <td width="31%" align="center"><div align="center">
    <?php if($image_link==''){?>
      <div class="intr-checkbox">
        <input name="image" type="radio" id="imageSI" value="1" <?php echo $image_link!=''?'checked="checked"':'';?> />
        <label for="imageSI">Con Imagen</label>
      </div>
      <div class="intr-checkbox">
        <input name="image" type="radio" id="imageNO" value="0" <?php echo $image_link==''?'checked="checked"':'';?> />
        <label for="imageNO">Sin Imagen</label>
      </div><?php }?>
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr >
    <td>&nbsp;</td>
    <td><div class="intr-label">Fecha</div></td>
    <td><div class="intr-label">Estado</div></td>
    <td width="31%" rowspan="7" bgcolor="#EFEFEF"><div><?php echo '<img src="../img/enlace/'.$image_link.'" border="0" />'; ?></div></td>
    <td>&nbsp;</td>
  </tr>
  <tr >
    <td>&nbsp;</td>
    <td><div align="left">
      <input name="fecha" type="text" class="text-input corner-input-intr small-input" id="fecha" value="<?php echo $date_link;?>" readonly="readonly" />
    </div></td>
    <td><div align="left">
      <div class="intr-checkbox">
        <input name="estado" type="radio" id="activo" value="1" <?php echo $state_link==1?'checked="checked"':'';?> />
        <label for="activo">Publicar</label>
        </div>
      <div class="intr-checkbox">
        <input name="estado" type="radio" id="inactivo" value="0" <?php echo $state_link==0?'checked="checked"':'';?> />
        <label for="inactivo">No Publicar</label>
        </div>
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr >
    <td>&nbsp;</td>
    <td width="27%"><div class="intr-label">Inicio de Publicaci&oacute;n</div></td>
    <td width="27%"><div class="intr-label">Fin de Publicaci&oacute;n</div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="left">
      <input name="publicacion_ini" type="text" class="text-input corner-input-intr small-input" id="publicacion_ini" value="<?php echo $publication_star; ?>" readonly="readonly" />
    </div></td>
    <td><div align="left">
      <input name="publicacion_fin" type="text" class="text-input corner-input-intr small-input" id="publicacion_fin" value="<?php echo $publication_end; ?>" readonly="readonly" />
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><div class="intr-label">Dominio</div></td>
    <td></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><div align="left">
      <input type="text" name="dominio" id="dominio" class="text-input corner-input-intr small-input" value="<?php echo $accion=='U'?$url_link:'http://'; ?>" />
    </div></td>
    <td></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
  </tr>
   <tr>
     <td align="center" valign="middle">&nbsp;</td>
     <td colspan="3" align="center" valign="middle"><div class="buttons" style="text-align:center">
       <input name="Restablecer" type="reset" class="bootom cancel" value=" Cancelar " />
       <input class="bootom save" type="submit" value=" Guardar " />
     </div></td>
     <td align="center" valign="middle">&nbsp;</td>
   </tr>
</table>
</form>
</div>
<script type="text/javascript"> 
$(document).ready(function()
{
	$("#fecha").glDatePicker({
		onChange: function(target, newDate)
    	{target.val(newDate.getFullYear()+ "/" + (newDate.getMonth() + 1)+ "/" +newDate.getDate());}
		});	
	$("#publicacion_ini").glDatePicker(
			{	
				onChange: function(target, newDate)
    				{target.val(newDate.getFullYear()+ "/" + (newDate.getMonth() + 1)+ "/" +newDate.getDate());},
				endDate: 10,
    			startDate: new Date(),
    			selectedDate: 3,
    			allowOld: false
			});	
	$("#publicacion_fin").glDatePicker(
			{
				onChange: function(target, newDate)
    				{target.val(newDate.getFullYear()+ "/" + (newDate.getMonth() + 1)+ "/" +newDate.getDate());},
				endDate: 20,
				showPrevNext: true,
				allowOld: false,
				startDate: new Date()
			});	
});
</script> 