<script>
    $(document).ready(function() {
        $('.content-box tbody tr:even').addClass('alt-row');
    });
    function EliminarDato(id, url) {
        var msg = confirm("Desea eliminar este Registro")
        if (msg) {
            $.ajax({
                url: url,
                type: "POST",
                data: 'accion=D&id_country=' + id,
                success: function(datos) {
                    //alert('registro -'+id+'- eliminado');
                    //ConsultaDatos('includes/modulos/avisos/');
                    $("#fila-" + id).fadeOut('slow', function() {
                        $(this).remove();
                    });
                }
            });
        }
        return false;
    }
</script>
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Lista De Paises</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab current">Lista De Paises</a></li>
            <li><a href="intranet.php?md=<?php echo md5('new_pais') ?>&fl=<?php echo $_REQUEST['fl']; ?>">Escribir Nuevo</a></li>
        </ul>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1" style="display: block; "> <!-- This is the target div. id must match the href of this div's tab -->
            <form action="" method="post">
                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="6%" align="left"><input class="check-all" type="checkbox"></th>
                            <th width="75%" align="left">TITULO</th>

                            <th width="19%" align="center" valign="middle">ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include_once('includes/commons/intranet.class.php');
                        $objData = new intranet;
                        $result = $objData->getListCountry(' ORDER BY date_register DESC ');
                        ?>
                                <?php while ($dataRow = mysql_fetch_row($result)) { ?>
                            <tr id="fila-<?php echo $dataRow[0]; ?>">
                                <td align="left"><input type="checkbox" name="urls[]" value="0"></td>
                                <td align="left">
    <?php echo $dataRow['1']; ?>
                                </td>

                                <td align="center" valign="middle"><!-- Icons -->
                                    <a href="intranet.php?md=<?php echo md5('new_pais') . '&fl=' . $_REQUEST['fl'] . '&acc=U&cd=' . $dataRow['0']; ?>" title="Edit"><img src="http://dev.ikfsystems.sk/ikfa/media/admin/images/icons/pencil.png" alt="Edit" border="0"></a>
                                    <a href="javascript:EliminarDato(<?php echo $dataRow['0']; ?>,'includes/modulos/pais/pais_acciones.php')" onclick="<?php echo $dataRow['0']; ?>" title="Delete"><img src="http://dev.ikfsystems.sk/ikfa/media/admin/images/icons/cross.png" alt="Delete" border="0"></a>
                                </td>

                            </tr>
<?php } ?>



                    </tbody>
                    <tfoot>
                        <tr align="left">
                            <td colspan="7">
<!--                                <div class="bulk-actions align-left">
                                    <select name="option" class="first">
                                        <option value="">Select action...</option>
                                        <option value="delete">Delete</option>
                                        <option value="move" class="nextSelect">Move to URL</option>
                                        <option value="copy" class="nextSelect">Copy to URL</option>
                                    </select>
                                    <select name="page" class="second">
                                        <option value="0">Default</option>
                                        <option value="">home page</option><option value="">--Pagina nueva</option>
                                        <option value="^http://dev.ikfsystems.sk/ikfa/prueba$">Prueba</option>											
                                    </select>
                                    <input type="submit" class="button" name="apply" value="Apply on selected">
                                </div>
                                <div class="clear"></div>-->
                            </td>
                        </tr>
                    </tfoot>										
                </table>
            </form>
        </div> <!-- End #tab1 -->
    </div> <!-- End .content-box-content -->			
</div>