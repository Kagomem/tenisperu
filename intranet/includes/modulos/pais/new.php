<?php
ob_start();
session_start();
$accion = $_REQUEST['acc'];
$stadoForm = $_REQUEST['st'];
($accion == '') ? $accion = 'I' : $accion = 'U';
if ($accion == 'U') {
    $id_country = $_REQUEST['cd'];
    include_once('includes/commons/intranet.class.php');
    $objIntranet = new intranet;
    $dataPais = $objIntranet->getListCountry('where id_country=' . $id_country);
    while ($data = mysql_fetch_array($dataPais)) {
        $title_country = $data['1'];
    }
}
?>
<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode: "textareas",
        theme: "advanced",
        skin: "o2k7",
        language: "en",
        skin_variant: "silver",
        forced_root_block: false,
        force_br_newlines: true,
        force_p_newlines: false,
        //plugins:"save,jfilebrowser",
        theme_advanced_buttons1: "justifyleft,justifycenter,justifyright,justifyfull,|,bold,italic,underline,|,fontselect,notice,unnotice,|,fontselect,fontsizeselect,forecolor,backcolor",
        theme_advanced_buttons2: "bullist,numlist,separator,outdent,indent,separator,undo,redo,link,unlink",
        theme_advanced_buttons3: "hr,removeformat,visualaid,separator,sub,sup,separator,charmap,upload",
    });
    /*tinymce.init({
     selector: "textarea",
     plugins: [
     "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
     "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
     "table contextmenu directionality emoticons template textcolor paste fullpage textcolor"
     ],
     
     toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify |formatselect fontselect fontsizeselect",
     toolbar2: "cut copy paste | bullist numlist | outdent indent | undo redo | link unlink image | forecolor backcolor",
     toolbar3: "table | hr | subscript superscript | charmap | spellchecker",
     
     menubar: false,
     toolbar_items_size: 'small',
     
     style_formats: [
     {title: 'Bold text', inline: 'b'},
     {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
     {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
     {title: 'Example 1', inline: 'span', classes: 'example1'},
     {title: 'Example 2', inline: 'span', classes: 'example2'},
     {title: 'Table styles'},
     {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
     ],
     
     templates: [
     {title: 'Test template 1', content: 'Test 1'},
     {title: 'Test template 2', content: 'Test 2'}
     ]
     });*/
</script>
<?php if ($stadoForm != NULL && $stadoForm == 1) { ?>
    <script>
        $(document).ready(function() {
            $('#message').fadeIn('normal', function() {
                $(this).delay(1500).fadeOut('normal');
            });
        });
    </script>
    <div class="form-state" id="message" style="display:none">
        <div class="message exito">Registro Guardado</div>
    </div>
<?php } ?>
<?php if ($stadoForm != NULL && $stadoForm == 2) { ?>
    <script>
        $(document).ready(function() {
            $('#message').fadeIn('normal', function() {
                $(this).delay(1500).fadeOut('normal');
            });
        });
    </script>
    <div class="form-state" id="message" style="display:none">
        <div class="message exito">Error!!!. No se pudo guardar el nuevo registro.</div>
    </div>
<?php } ?>
<div id="record" class="formNew">
    <form action="includes/modulos/pais/pais_acciones.php" method="post" 
          enctype="multipart/form-data" name="formPais" id="formPais">
        <input type="hidden" id="accion" name="accion" value="<?php echo $accion ?>" />
        <input type="hidden" id="idusuario" name="idusuario" value="<?php echo $_SESSION['idusuario']; ?>" />
        <input type="hidden" id="id_country" name="id_country" value="<?php echo $id_country; ?>" />
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="optionForm">
            <tr>
                <td width="28%">&nbsp;</td>
                <td width="40%"><div class="intr-label">Pa&iacute;s</div></td>
                <td width="32%">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><div align="left">
                        <input type="text" name="titulo" id="titulo" class="text-input corner-input-intr small-input" value="<?php echo $title_country; ?>" />
                    </div></td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle">
                    <div class="buttons" style="text-align:center">
                        <input class="bootom cancel" type="submit" value=" Cancelar ">
                        <input class="bootom save" type="submit" value=" Guardar ">
                    </div></td>
                <td align="center" valign="middle">&nbsp;</td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {

        $(".save").click(function(e) {
            e.preventDefault();
            var nombre = $("#titulo").val();

            if (nombre.trim().length < 1)
            {
                alert("Porfavor complete los campos");
            }
            else if (nombre.trim().length < 2)
            {
                alert("Ingrese el nombre del pais");
            }
            else if (isNaN(nombre.trim()) == false)
            {
                alert("El nombre del pais no puede ser numerico");
            }
            else if (nombre.trim().length > 200)
            {
                alert("EL campo del nombre del pais no puede exceder los 200 caracteres");
            }
            else
            {
                document.formPais.submit();
            }
        });


        $("#fecha").glDatePicker({
            onChange: function(target, newDate)
            {
                target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
            }
        });
        $("#publicacion_ini").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 10,
                    startDate: new Date(),
                    allowOld: false
                });
        $("#publicacion_fin").glDatePicker(
                {
                    onChange: function(target, newDate)
                    {
                        target.val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + newDate.getDate());
                    },
                    endDate: 20,
                    showPrevNext: true,
                    allowOld: false,
                    startDate: new Date()
                });
    });
</script> 