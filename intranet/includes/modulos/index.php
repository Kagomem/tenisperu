<?php ob_start();
session_start();
if (!$_SESSION['idusuario']) {
    header('Location:../');
} ?><div class="<?php echo ($color); ?> redondeo-div font-family-content intr-titulo-mod " id="titulo-interior"><div style="padding-left:8px;"><?php echo ($titulo) . ' - <span style="font-size:16px">' . $subtitulo . '</span>'; ?></div></div><script>$(document).ready(function() {
        var offs = $('.main-content-box').offset();
        $('.main-content-box').height($(document).height() - offs.top - 16);
    });</script><div style="" class="main-content-box" ><div class="font-family-body body-content" style="padding-bottom:20px;padding-top:20px"><?php if (!empty($link) && file_exists('includes/modulos/' . $link)) {
    include_once($link);
} else {
    ob_start();
    session_start();
    session_unset();
    session_destroy();
    header('Location:../../');
    exit;
} ?></div></div>