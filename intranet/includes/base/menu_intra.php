<link href="css/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('div.intr_li_background a').click(function() {
            $('li div').removeClass('active_menu');
            $(this).parent().addClass('active_menu');
            var elem = $(this).parent().next();
            if (elem.is('ul')) {
                $('#intr_menu ul:visible').not(elem).slideUp();
                elem.slideToggle();
            };
        });
    });
        </script>
<?php $men_active = $_REQUEST['fl']; ?>
<ul id="intr_menu">
    <li><div
            class="intr_li_background <?php echo ($men_active == 1) ? 'active_menu' : ''; ?>">
            <a href="#">Publicaci&oacute;n de Portada</a>
        </div>
        <ul style="display:<?php echo ($men_active == 1) ? 'block' : 'none;'; ?>">
            <li><a href="intranet.php?md=<?php echo md5('new_portada') ?>&fl=1">Nueva
                    Portada</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_portada') ?>&fl=1">Administrar
                    Portada</a>
            </li>
        </ul>
    </li>
    <li><div
            class="intr_li_background <?php echo ($men_active == 2) ? 'active_menu' : ''; ?>">
            <a href="#">Noticias</a>
        </div>
        <ul style="display:<?php echo ($men_active == 2) ? 'block' : 'none;'; ?>">
            <li><a href="intranet.php?md=<?php echo md5('new_noticia') ?>&fl=2">Nueva
                    Noticia</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_noticia') ?>&fl=2">Administrar
                    Noticias</a>
            </li>
        </ul>
    </li>
    <li><div
            class="intr_li_background <?php echo ($men_active == 3) ? 'active_menu' : ''; ?>">
            <a href="#">Torneos Deportivos</a>
        </div>
        <ul style="display:<?php echo ($men_active == 3) ? 'block' : 'none;'; ?>">
            <li><a href="intranet.php?md=<?php echo md5('new_torneoweb') ?>&fl=3">Nuevo
                    Torneo</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_torneoweb') ?>&fl=3">Administrar
                    Torneos</a>
            </li>
        </ul>
    </li>
    <li><div
            class="intr_li_background <?php echo ($men_active == 4) ? 'active_menu' : ''; ?>">
            <a href="#">Informaci&oacute;n Deportiva</a>
        </div>
        <ul style="display: <?php echo ($men_active == 4) ? 'block' : 'none;'; ?> ">
            <li><a href="intranet.php?md=<?php echo md5('new_aviso') ?>&fl=4">Nuevo
                    Aviso</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_aviso') ?>&fl=4">Administrar
                    Avisos</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('new_ficha') ?>&fl=4">Nueva
                    Ficha Inscripci&oacute;n</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_ficha') ?>&fl=4">Administrar
                    Ficha Inscripcion</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('new_calendar') ?>&fl=4">Nuevo
                    Calendario</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_calendar') ?>&fl=4">Administrar
                    Calendario</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('new_pais') ?>&fl=4">Nuevo
                    Pa&iacute;s</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_pais') ?>&fl=4">Administrar
                    Pa&iacute;s</a>
            </li>
        </ul>
    </li>
    <li><div
            class="intr_li_background <?php echo ($men_active == 6) ? 'active_menu' : ''; ?>">
            <a href="#">Galeria Im&aacute;genes / Videos</a>
        </div>
        <ul style="display:<?php echo ($men_active == 6) ? 'block' : 'none;'; ?>">
            <li><a href="intranet.php?md=<?php echo md5('new_imagen') ?>&fl=6">Agregar
                    Im&aacute;genes</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_imagen') ?>&fl=6">Administrar
                    Im&aacute;genes</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('new_video') ?>&fl=6">Agregar
                    Videos</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_video') ?>&fl=6">Administrar
                    Videos</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('new_galeria') ?>&fl=6">Agregar
                    Galer&iacute;a</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_galeria') ?>&fl=6">Administrar
                    Galer&iacute;as</a>
            </li>
        </ul>
    </li>
    <li><div
            class="intr_li_background <?php echo ($men_active == 7) ? 'active_menu' : ''; ?>">
            <a href="#">Auspiciadores</a>
        </div>
        <ul style="display:<?php echo ($men_active == 7) ? 'block' : 'none;'; ?>">
            <li><a
                    href="intranet.php?md=<?php echo md5('new_auspiciador') ?>&fl=7">Nuevo
                    Auspiciador</a>
            </li>
            <li><a
                    href="intranet.php?md=<?php echo md5('admi_auspiciador') ?>&fl=7">Administrar
                    Auspiciadores</a>
            </li>
        </ul>
    </li>
    <li><div
            class="intr_li_background <?php echo ($men_active == 8) ? 'active_menu' : ''; ?>">
            <a href="#">Enlaces Relacionados</a>
        </div>
        <ul style="display:<?php echo ($men_active == 8) ? 'block' : 'none;'; ?>">
            <li><a href="intranet.php?md=<?php echo md5('new_enlace') ?>&fl=8">Nuevo
                    Enlace</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_enlace') ?>&fl=8">Administrar
                    Enlaces</a>
            </li>
        </ul>
    </li>
    <li><div
            class="intr_li_background <?php echo ($men_active == 9) ? 'active_menu' : ''; ?>">
            <a href="#">Ranking Deportivo</a>
        </div>
        <ul style="display:<?php echo ($men_active == 9) ? 'block' : 'none;'; ?>">
            <li><a href="intranet.php?md=<?php echo md5('new_jugador') ?>&fl=9">Nuevo
                    Jugador</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_jugador') ?>&fl=9">Administrar
                    Jugadores</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('new_torneo') ?>&fl=9">Nuevo
                    Torneo</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_torneo') ?>&fl=9">Administrar
                    Torneos</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('new_categoria') ?>&fl=9">Nueva
                    Categor&iacute;a</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_categoria') ?>&fl=9">Administrar
                    Categor&iacute;as</a>
            </li>
            <li><a
                    href="intranet.php?md=<?php echo md5('admi_participante') ?>&fl=9">Participantes
                    por Torneo</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_puntaje') ?>&fl=9">Puntajes
                    por Torneo</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_reporte') ?>&fl=9">Reportes</a>
            </li>
        </ul>
    </li>
    <li><div
            class="intr_li_background <?php echo ($men_active == 10) ? 'active_menu' : ''; ?>">
            <a href="#">P&aacute;ginas</a>
        </div>
        <ul style="display:<?php echo ($men_active == 10) ? 'block' : 'none;'; ?>">
            <li><a href="intranet.php?md=<?php echo md5('new_page') ?>&fl=10">Nueva
                    P&aacute;gina</a>
            </li>
            <li><a href="intranet.php?md=<?php echo md5('admi_page') ?>&fl=10">Administrar
                    P&aacute;ginas</a>
            </li>
        </ul>
    </li>
    <li>
        <div
            class="intr_li_background <?php echo ($men_active == 11) ? 'active_menu' : ''; ?>">
            <a href="#">Inscripciones</a>
        </div>
        <ul style="display:<?php echo ($men_active == 11) ? 'block' : 'none;'; ?>">
            <li>
                <a href="intranet.php?md=<?php echo md5('admi_event') ?>&fl=11">Administrar Evento</a>
            </li>
            <li>
                <a href="intranet.php?md=<?php echo md5('athlete_list') ?>&fl=12" title="">Listado de Deportistas</a>
            </li>
        </ul>
    </li>
</ul>
