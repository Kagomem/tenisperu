<?php
ob_start();
session_start();
if (!$_SESSION['idusuario']) {
    header('Location:../');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Intranet FDP. Tennis</title>
        <script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="recursos/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="js/glDatePicker.min.js"></script>
        <script type="text/javascript" src="js/uploadFunction.js"></script>
        <script type="text/javascript" src="js/colorpicker.js"></script>
        <script type="text/javascript" src="js/eye.js"></script>
        <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>
        <script type="text/javascript">function salir() {
                location.href = 'includes/commons/salir.php';
            }</script>
        <link href="css/style-page.css" rel="stylesheet" type="text/css" />
        <link href="css/glDatePicker.default.css" rel="stylesheet"type="text/css"/>
        <link href="css/colorpicker.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="popup_voucher">

        </div>
        <div class="intr-body">
            <div class="intr_panel_left">
                <div class="intr_secction">
                    <div class="intr_logo">
                        <img src="img/png/intra_logo.png" width="166" height="124"
                             border="0" />
                    </div>
                </div>
                <div class="intr_secction">
                    <div class="intr_usser">
                        Usuario : <span><?php echo $_SESSION['usuario']; ?> </span>
                    </div>
                </div>
                <div class="intr_secction">
                    <div class="intr_option">
                        <a href="intranet.php"><span>Inicio</span> </a>|<a
                            href="javascript:salir();"><span>Cerrar Sesi&oacute;n</span> </a>
                    </div>
                </div>
                <div class="intr_secction">
                    <?php include("includes/base/menu_intra.php"); ?>
                </div>
            </div>
            <div class="intr_panel_rigth">
                <?php include("intr_interior.php"); ?>
                <div class="intr_footer">&copy; Copyright 2011 SoftmarkPer&uacute;</div>
            </div>
        </div>
        <link rel="stylesheet" href="css/validationEngine.jquery.css"
              type="text/css">
            <script type="text/javascript" src="js/jquery.validationEngine-es.js"></script>
            <script type="text/javascript" src="js/jquery.validationEngine.js"></script>
            <script>$(document).ready(function() {
                    $('#titulo').addClass('validate[required,maxSize[150]]');
                    var f = new Date();
                        var $d=f.getDate();
                        var $m=f.getMonth();
                        var Year=f.getFullYear();
                        var fecha =Year+'/'+$m+'/'+$d;
                    $('#fecha').addClass('validate[required,custom[date]]');
                    $('#dominio').addClass('validate[custom[url]]');
                    $('#tipo').addClass('validate[required]');
                    $('#publicacion_ini').addClass('validate[required]');
                    $('#publicacion_fin').addClass('validate[required]');
                });
                $(document).ready(function() {
                    $("#formFront,#formNotice,#formTorneo,#formInfrDeport,#formCalendar,#formPais,#formAuspiciador").validationEngine();
                    $("#formFront").bind("jqv.form.validating", function(event) {
                        $("#hookError").html("")
                    });
                    $("#formFront").bind("jqv.form.result", function(event, errorFound) {
                        if (errorFound)
                            $("#hookError").append("There is some problems with your form");
                    });
                })
            </script>

    </body>
</html>
