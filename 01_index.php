<?php include_once("includes/query/config.ini.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.::Federacion Deportiva Peruana de Tenis::.</title>
<meta name="keywords" content="tenis">
	<meta name="description" content="tenis">
		<meta name="robots" content="index, follow">
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<link rel="shortcut icon" href="favicon.ico">
					<meta name="author" content="root" />
					<link href='http://fonts.googleapis.com/css?family=Terminal+Dosis:500' rel='stylesheet' type='text/css'>
						<link href="css/deportes_styles.css" rel="stylesheet"
							type="text/css" />
						<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet"
							type="text/css" />
						<script type="text/javascript"
							src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
						<script
							src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
						<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
						<script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
						<script src="js/jquery.mCustomScrollbar.js"></script>
						<link href="css/slides.css" rel="stylesheet" type="text/css" />
						<script type="text/javascript" src="js/slides.min.jquery.js"></script>

</head>
<body>
	<div class="dpt_body fnd_deportes">
		<div class="dpt_container_uno" style="margin-bottom: 10px">
		<?php include_once("includes/base/header.php");?>
		</div>
		<div class="dpt_container_uno">
			<div class="dpt_section panel_item_uno"
				style="background: none; border: none">
				<div class="dpt_panel">
					<div id="_deportes_portada"
						class="corner_div_6 tn-background-portada">
						<?php include_once("includes/modulo/modulo.portada.php");?>
					</div>
					<div id="_deportes_ranking" class="corner_div_6">
						<div class="tn-background-ranking corner_div_6"
							style="padding-bottom: 3px">
							<?php include_once("includes/modulo/modulo.ranking.php");?>
						</div>
					</div>
					<div class="dpt_item corner_div_6" id="_deportes_noticia">
					<?php include_once("includes/modulo/modulo.notice.php");?>
					</div>
					<div class="dpt_item corner_div_6" id="_deportes_torneo">
					<?php include_once("includes/modulo/modulo.torneo.php");?>
					</div>
					<div class="dpt_item corner_div_6" id="_deportes_galeria">
					<?php include_once("includes/modulo/modulo.gallery.php");?>
					</div>
					<div class="dpt_item corner_div_6" id="_deportes_publi">
						<a
							href="interior.popup.php?pg=48b755f61888b92698ef6b72044fe6bb&id=Tenis-en-las-Escuelas"
							class="uopa1ufg5p" target="_blank"><img
							src="img/jpg/tenis_scuela.jpg" alt="Tenis en las Escuelas"
							width="300" height="146" border="0" /> </a>
					</div>
					<div class="dpt_item corner_div_6" id="_deportes_auspiciadores">
					<?php include_once("includes/modulo/modulo.auspiciador.php");?>
					</div>
					<div class="dpt_item corner_div_6" id="_deportes_inforDepor">
					<?php include_once("includes/modulo/modulo.informacion_deportiva.php");?>
					</div>
				</div>
			</div>
			<div class="dpt_section panel_item_dos corner_div_4">
				<div class="dpt_panel">
					<div class="dpt_item corner_div_6" id="_deportes_enlaces_rela">
					<?php include_once("includes/modulo/modulo.enlaces.php");?>
					</div>
				</div>
			</div>
			<script type="text/javascript">var _gaq = _gaq || [];_gaq.push(['_setAccount', 'UA-21199236-4']);_gaq.push(['_trackPageview']);(function() {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);})();</script>
			<script language="javascript" type="text/javascript"
				src="js/funciones.tenis.js"></script>
		</div>
	</div>
	<?php include_once("includes/base/pie_pag.php");  ?>
	<?php include_once("includes/modulo/modulo.popup.php"); ?>
	<link href="css/deportes.interior.css" rel="stylesheet" type="text/css" />
</body>
</html>
